/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <unistd.h>
#include <stdlib.h>
#include <math.h>

#include "libavutil/motion_vector.h"
#include "libavformat/avformat.h"

#include "internal/argparse.h"
#include "internal/util.h"

#include "error.h"
#include "frame.h"
#include "tiff.h"
#include "video.h"


typedef struct {
    int help;
    char* vis_prefix;
} Opts;


static Opts opts = {
    .help = 0,
    .vis_prefix = NULL
};


static void print_usage (ArgParse* P)
{
    printf ("Usage: vftk perror [OPTIONS...] INPUT\n"
            "\n"
            "Options:\n");

    argparse_print (P);
}


static ArgParse* _parse (int argc, char* argv[])
{
    int ret;

    ArgParse* P = argparse_new (argc, argv);

    argparse_add (P, ARG_STR, "-V", "--vis-prefix=PREFIX",
            "write prediction error visualization frames to disk "
            "using file names starting with PREFIX",
            &opts.vis_prefix);

    argparse_add (P, ARG_FLAG, "-h", "--help",
            "display this help text and exit", &opts.help);

    if (!argparse (P)) {
        ret = EXIT_FAILURE;
        goto out;
    }

    if (opts.help) {
        print_usage (P);
        ret = EXIT_SUCCESS;
        goto out;
    }

    if (P->argc < 1) {
        fprintf (stderr, "Must specify INPUT\n");
        ret = EXIT_FAILURE;
        goto out;
    }

    return P;

out:
    argparse_destroy (&P);
    exit (ret);
}


static void _dump_perror (Video* V)
{
    Frame* perror = NULL;
    char fn[512] = {0};


    if (opts.vis_prefix)
        perror = frame_new ();

    do {
        if (opts.vis_prefix)
            video_get_frame_perror (V, perror);

        printf ("%"PRId64", %f\n", video_peek_framenumber(V),
                video_peek_frame_perror (V));

        if (opts.vis_prefix) {
            snprintf (fn, 512, "%s%0*"PRId64".tiff", opts.vis_prefix,
                    _num_digits(video_peek_nframes (V)),
                    video_peek_framenumber(V));

            tiff_writer (fn, perror);
        }
    } while (video_seek_next_frame (V));

    frame_destroy (&perror);
}


int mode_perror (int argc, char* argv[])
{
    ErrorCode err;
    ArgParse* P;
    Video* V;

    int ret = EXIT_SUCCESS;

    P = _parse (argc, argv);

    err = video_open (&V, P->argv[0]);
    if (err) {
        fprintf (stderr, "%s ['%s']\n", err2str (err), P->argv[0]);
        ret = EXIT_FAILURE;
        goto out;
    }

    _dump_perror (V);

    video_destroy (&V);
out:
    argparse_destroy (&P);
    return ret;
}
