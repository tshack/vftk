/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <string.h>

#include "internal/preproc.h"
#include "internal/video.h"

#include "error.h"
#include "list.h"
#include "video.h"

#define to_preproc(ptr) container_of (ptr, PreProc, node)
#define to_ctx(ptr) container_of (ptr, PreProcContext, node)

static List modules;


static void _blind_rewind (Video* V)
{
    av_seek_frame (V->container, V->stream_idx, 0, AVSEEK_FLAG_BACKWARD);
    avcodec_flush_buffers (V->decoder);
}


static void _detect_interlaced (Video* V)
{
    _blind_rewind (V);
    video_seek_next_frame (V);

    if (V->avframe->interlaced_frame)
        V->is_interlaced = 1;
    else
        V->is_interlaced = 0;

    return;
}


static void _preprocessing_load (void)
{
    static int loaded = 0;

    if (loaded)
        return;

    list_init(&modules);

    /* NOTE:
     * All preprocessing modules must:
     *     1. have an 'extern NEW_PREPROC(name)' entry in preproc.h
     *     2. be added to the modules list as below
     */
    list_add_before(&modules, &PREPROC(gopnm).node);
    list_add_before(&modules, &PREPROC(ts_luts).node);
    list_add_before(&modules, &PREPROC(ftypes).node);

    loaded = 1;
}


static ErrorCode _preprocessing_init (Video* V)
{
    PreProc* P;
    PreProcContext* ctx;
    List* cur;

    ErrorCode err = SUCCESS;

    _preprocessing_load ();

    list_foreach (&modules, cur) {
        P = to_preproc (cur);

        if ( !(ctx = malloc (sizeof(*ctx))) ) {
            err = EPREPROC_ALLOC;
            goto out;
        }

        ctx->P = P;
        if ( !(ctx->D = malloc (P->size)) ) {
            err = EPREPROC_ALLOC;
            goto out2;
        }

        if ( (err = ctx->P->init (V, ctx->D)) != SUCCESS )
            goto out3;

        ctx->done = 0;
        list_add_before (&V->preprocs, &ctx->node);
    }

    goto out;

out3:
    free (ctx->D);
out2:
    free (ctx);
out:
    return err;
}


static int _next_packet (Video* V)
{
    int is_eof;

    if (V->pkt.buf)
        av_packet_unref (&V->pkt);

    while (!(is_eof = av_read_frame (V->container, &V->pkt))) {
        if (V->pkt.stream_index == V->stream_idx)
            break;
        else
            av_packet_unref (&V->pkt);
    }

    if (is_eof)
        return 0;

    return 1;
}


static int64_t _count_packets (Video* V)
{
    int64_t npkts = 0;

    _blind_rewind (V);

    while (_next_packet (V))
        npkts++;

    return npkts;
}


void preprocess_fini (Video* V)
{
    List* cur;
    List* bak;
    PreProcContext* ctx;

    list_foreach_safe (&V->preprocs, cur, bak) {
        ctx = to_ctx (cur);

        ctx->P->fini (V, ctx->D);

        list_remove (cur);
        free (ctx->D);
        free (ctx);
    }
}


PreProcReturn preproc_export (Video* V, char* data_name)
{
    int i;
    List* cur;
    PreProcContext* ctx;
    PreProcReturn data;

    list_foreach (&V->preprocs, cur) {
        ctx = to_ctx (cur);

        if (!ctx->P->exports)
            continue;

        for (i=0; ctx->P->exports[i].name; i++) {
            if (!strcmp(ctx->P->exports[i].name, data_name)) {
                if (!ctx->done && ctx->P->perframe)
                    preprocess_frames (V);

                switch (ctx->P->exports[i].type) {
                case INT:
                    data.int_ = *(int*)((char*)ctx->D + ctx->P->exports[i].data);
                    break;
                case INT64:
                    data.int64_ = *(int64_t*)((char*)ctx->D + ctx->P->exports[i].data);
                    break;
                case PTR:
                    data.ptr_ = *(void**)((char*)ctx->D + ctx->P->exports[i].data);
                    break;
                }
                return data;
            }
        }
    }

    data.ptr_ = NULL;
    return data;
}


void preprocess_frames (Video* V)
{
    List* cur;
    PreProcContext* ctx;
    int64_t save;
    int64_t framenumber;

    save = video_peek_framenumber (V);
    video_seek_frame (V, 0);
    framenumber = video_peek_framenumber (V);

    do {
        list_foreach (&V->preprocs, cur) {
            ctx = to_ctx (cur);
            if (ctx->P->perframe && !ctx->done)
                ctx->P->perframe (V, ctx->D, framenumber);
        }

        framenumber++;
    } while (video_seek_next_frame (V));

    list_foreach (&V->preprocs, cur) {
        ctx = to_ctx (cur);
        if (ctx->P->perframe)
            ctx->done = 1;
    }

    video_seek_frame (V, save);
}


ErrorCode preprocess_packets (Video* V)
{
    List* cur;
    PreProcContext* ctx;
    ErrorCode err;

    int64_t packet_number = 0;

    V->npackets = _count_packets (V);

    _detect_interlaced (V);

    if ( (err = _preprocessing_init (V)) != SUCCESS )
        return err;

    _blind_rewind (V);
    if (!_next_packet (V))
        return EPREPROC_PKT;

    do {
        list_foreach (&V->preprocs, cur) {
            ctx = to_ctx (cur);
            if (ctx->P->perpacket && !ctx->done)
                ctx->P->perpacket (V, ctx->D, packet_number);
        }

        packet_number++;
    } while (_next_packet (V));

    list_foreach (&V->preprocs, cur) {
        ctx = to_ctx (cur);
        if (ctx->P->perpacket)
            ctx->done = 1;
    }

    _blind_rewind (V);
    video_seek_next_frame(V);

    return SUCCESS;
}
