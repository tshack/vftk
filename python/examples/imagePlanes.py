#!/usr/bin/env python3
#####################################################################
# Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
#####################################################################
import argparse
import numpy as np
import vftk

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input',
                    type=str,
                    dest='infile',
                    default='',
                    required=True,
                    help="Input video filename")

# Parse command line arguments
args = parser.parse_args()

# Open the file specified in the command line arguments
video = vftk.Video(args.infile)
print("Opened '%s'" % args.infile)

# Advance 100 frames.
for _ in range(100):
    video.seekNextFrame()

# Get the current frame
frame = video.getFrame()

# Get the frame's color planes.
# Each color plane is a numpy.ndarray (dtype=uint8)
R, G, B = frame.imagePlanes()

# Print a few ndarray attributes for the red image plane
print("R.%r" % R.dtype)
print("R.shape: (%i, %i)" % R.shape)
print("R.ndim: %i" % R.ndim)
print("R.size: %i elements" % R.size)
# print("R.itemsize: $r bytes" % R.itemsize)
print("R.nbytes: %i bytes" % R.nbytes)
print("R.base is frame: %r" % (R.base is frame))

######################################################################
# NOTE: ndarrays are column major. The underlying pixel buffer is
# row major.  This results in each matrix column mapping to a row
# of the image.
####################

# Slice the red image plane to get a view of the first
# column of red pixels
R_column0 = R[:, 0]

# Get the blue channel pixel value from the
# 10th column, 20th row of the image
blue_pixel = B[9, 19]
print(blue_pixel)


######################################################################
# Important Notes on Memory Management
######################################################################
# Keep in mind that each ndarray color plane holds a reference to its
# underlying PykerFrame object, and each PykerFrame object holds a
# reference to its underlying PykerVideo object.  This is necessary,
# normal, sane Python behavior; otherwise the data underlying a color
# plane would vanish while you were still holding a reference to it if
# you were to drop all of your direct references to either the
# underlying PykerFrame or PykerVideo objects.
#
# This means that as long as you are holding a reference to a color
# plane in Python, the underlying PykerVideo and PykerFrame data
# structures will continue to persist in memory until you release all
# your references to them and their dependents -- either by assigning
# the references to other objects (most usual) or explicitly deleting
# the references using `del`
#
# (If this doesn't make sense to you, brush up on your Python and basic
# computer science)
#
# If you do not desire this behavior, make copies of the image plane
# numpy arrays (e.g. B_frame10 = np.copy(B)) instead of creating simple
# references to them (e.g. B_frame10 = B).
####################

# Make a copy of the blue color plane
B_copy = np.copy(B)

# Delete all direct references to heavy weight Pyker objects
del frame, video

# Delete all indirect references to Pyker objects
del R, G, B, R_column0

# Note: blue_pixel is a reference to a standard Integer object.
# (If your Python fundamentals are strong, this isn't a surprise)

###################################################################
# At this point, all heavy wight Pyker objects have been freed from
# memory since there are no outstanding references to them
####################

# Naturally, we can still use our copy of the blue color plane
blue_pixel = B_copy[9, 19]
print(blue_pixel)

# Note: It is a bad idea to always make copies of color planes.  Think
# about what you are doing and make the better decision based on your
# situation and what you are trying to accomplish, keeping in mind that
# RAM is both an expensive and a finite resource.
