#!/usr/bin/env python3
#####################################################################
# Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
#####################################################################
import argparse
import vftk

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input',
                    type=str,
                    dest='infile',
                    default='',
                    required=True,
                    help="Input video filename")

# Parse command line arguments
args = parser.parse_args()

# Open the file specified in the command line arguments
video = vftk.Video(args.infile)
print("Opened '%s'" % args.infile)

# Advance 100 frames.
for _ in range(100):
    video.seekNextFrame()

# Get the current frame
frame = video.getFrame()

# Motion vectors are accessible via an MotionVectorIterator
# object, which is obtain by calling frame.motionVectors().
# This iterator returns a dictionary for each macroblock that
# has an associated motion vector.
for mv in frame.motionVectors():
    print("Associated Frame: %i" % mv['source'])
    print("Macroblock Size: %i x %i" % mv['size'])
    print("Motion Vector Source [in current frame]: (%i, %i)" % mv['src'])
    print("Motion Vector Dest [in assocated frame]: (%i, %i)" % mv['dst'])
    print("=======================================================")
