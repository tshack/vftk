#!/usr/bin/env python3
#####################################################################
# Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
#####################################################################
import argparse
import vftk

profile2str = {
    44:  "(44) CAVLC 4:4:4 Intra Profile",
    66:  "(BP) Baseline Profile",
    77:  "(MP) Main Profile",
    88:  "(XP) Extended Profile",
    100: "(HiP) High Profile",
    110: "(Hi10P) High 10 Profile",
    118: "(HiMulti) Multiview High Profile",
    122: "(Hi422P) High 4:2:2 Profile",
    128: "(HiStereo) Stereo High Profile",
    144: "(Hi444) High 4:4:4 Profile",
    244: "(Hi444PP) High 4:4:4 Predictive Profile",
    578: "(CBP) Constrained Baseline Profile",
    2158: "(Hi10PIntra) High 10 Intra Profile",
    2292: "(Hi444Intra) High 4:4:4 Intra Profile"
}

primaries2str = {
    0: 'Reserved',
    1: 'BT709 / ITU-R BT1361 / IEC 61966-2-4 / SMPTE',
    2: 'Unspecified',
    3: 'Reserved',
    4: 'BT470M / FCC Title 47 Code of Federal Regulations 73.682 (a)(20)',
    5: 'BT470BG / ITU-R BT601-6 625 / ITU-R BT1358 625 / ITU-R BT1700 625 PAL & SECAM',
    6: 'SMPTE170M / ITU-R BT601-6 525 / ITU-R BT1358 525 / ITU-R BT1700 NTSC',
    7: 'SMPTE240M',
    8: 'Film / Illuminant C',
    9: 'BT2020 / ITU-R BT2020',
    10: 'SMPTE428 / SMPTE ST 428-1 (CIE 1931 XYZ)',
    11: 'SMPTE431 / SMPTE ST 431-2 (2011) / DCI P3',
    12: 'SMPTE432 / SMPTE ST 432-1 (2010) / P3 D65 / Display P3',
    22: 'JEDEC P22 Phosphors'
}

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input',
                    type=str,
                    dest='infile',
                    default='',
                    required=True,
                    help="Input video filename")

args = parser.parse_args()

video = vftk.Video(args.infile)

print("Opened '%s'" % args.infile)
print("Video Properties:")
print("  - Width:   %i" % video.width())
print("  - Height:  %i" % video.height())
print("  - Frames:  %i" % video.nframes())
print('  - Profile: %i  "%s"' % (video.profile(), profile2str[video.profile()]))
print("  - Level:   %s" % video.level())
print("  - GOP:     N=%i, M=%i" % video.gop())
print("  - VFR:     %r" % video.isVFR())
print("  - Bitrate: %i" % video.bitrate())

video.seekNextFrame()
frame = video.getFrame()
props = frame.properties()

print("Frame Properties:")
print("  - Chroma Location: %s" % props['chroma_loc'])
print('  - Color Primaries: %i  "%s"' % (props['primaries'], primaries2str[props['primaries']]))
print("  - Color Range: %s" % props['range'])
print("  - Color Transfer Characteristic: %i" % props['xfer_char'])
print("  - Interlaced: %r" % props['interlaced'])
print("  - Entropy Coding: %s" % props['entropy_coding'])
