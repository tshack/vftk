#!/usr/bin/env python3
#####################################################################
# Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
#####################################################################
"""
Example: Seeking to specific frames

This example demonstrates the h264pyker seekFrame() method by:
    1. Creating a list of frame number we want to seek to

    2. Seeking to each frame number and writing the corresponding
       frame to disk in PNG format

    3. Showing how to use try-except-else to catch exceptions
       raised if the specified frame number is past the end
       of the video

    *Note: Specifying a negative frame number does not raise an
           exception.  Instead, the video is simply seeked to
           frame 0
"""
import sys
import argparse
import numpy as np
from PIL import Image
import vftk


def write_png(filename, video, frame):
    """
    Writes a given frame to the disk in PNG format.
    """
    RGB = np.zeros((video.height(), video.width(), 3), 'uint8')
    red, green, blue = frame.imagePlanes()
    RGB[..., 0] = red
    RGB[..., 1] = green
    RGB[..., 2] = blue

    Image.fromarray(RGB).save(filename)


def seek_to(video, frame):
    """
    Prints the destiation frame to the screen and then
    seeks the video to it.

    Returns True on success; otherwise, False
    """
    print("Seeking to Frame %i..." % frame)
    return video.seekFrame(frame)


def framenumber_zero_padded (video):
    """
    Returns a string containing the current frame number that has been
    zero padded to match the number of digits in the last frame number.
    """
    return str(video.frameNumber()).zfill(len(str(video.nframes())))


def write_current_frame(video):
    """
    Writes the current frame to disk and prints a status message
    containing the file name to the screen.
    """
    outfile = 'frame' + framenumber_zero_padded(video) + '.png'
    print("  Writing '%s'" % outfile)
    write_png(outfile, video, video.getFrame())


def parse_args():
    """
    Defines and parses command line arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input',
                        type=str,
                        dest='infile',
                        default='',
                        required=True,
                        help="Input video filename")

    return parser.parse_args()


def main():
    args = parse_args()
    video = vftk.Video(args.infile)
    print("Opened '%s'" % args.infile)

    # Seek to these frames in this order
    frames = [3345,   8, 1023, 1200, 0,
               100, -23,  400,  250, 100000,
               900, 492, 9001,    5, 327]

    for frame in frames:
        try:
            seek_to(video, frame)
        except:
            print("  error -- seek failed to frame %i" % frame)
            print("  @ Frame %i [%s]" % (video.frameNumber(), video.frameType()))
        else:
            print("  @ Frame %i [%s]" % (video.frameNumber(), video.frameType()))
            write_current_frame(video)


if __name__ == '__main__':
    main()
