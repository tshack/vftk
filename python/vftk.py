#!/usr/bin/env python3
"""
vftk.py

James A. Shackleford <shack@linux.com>, <shack@drexel.edu>

A reimplementation of vftk in Python3 for the purpose of
demonstrating the vftk Cython wrapper module.
"""
import sys
import contextlib
import argparse
import numpy as np
from PIL import Image
import vftk


@contextlib.contextmanager
def sane_open(filename=None):
    """
    Attempt to regain some well designed file output interface where I
    can open either stdout or a 'real' file.  Users specifying an output
    file of '-' for stdout will actually get expected behavior.
    """
    if filename and filename != '-':
        file_handle = open(filename, 'w')
    else:
        file_handle = sys.stdout
    try:
        yield file_handle
    finally:
        if file_handle is not sys.stdout:
            file_handle.close()


def write_png(filename, video, frame):
    """
    Writes a given frame to the disk in PNG format.
    """
    RGB = np.zeros((video.height(), video.width(), 3), 'uint8')
    red, green, blue = frame.imagePlanes()
    RGB[..., 0] = red
    RGB[..., 1] = green
    RGB[..., 2] = blue

    Image.fromarray(RGB).save(filename)


def mode_todo(args):
    """
    If an unimplemented mode is specifed on the command line, you end up
    here.  Let's not end up here so often... submit a merge request!
    """
    print(""" Mode '%s' not yet implemented!

          Please submited your Merge Request on GitLab (tshack/vftk).
          """ % args.mode)


def mode_ftypes(args):
    """
    MODE: 'ftypes'

    Writes out type for each frame in sequential order.
    """
    video = vftk.Video(args.infile)

    with sane_open(args.outfile) as outfile_handle:
        while True:
            print("%s, %i, %f" %
                  (video.frameType(), video.frameNumber(), video.time()),
                  file=outfile_handle)

            if not video.seekNextFrame():
                break


def mode_frames(args):
    """
    MODE: 'frames'

    Dumps all frames to disk.  The 'output file' specified by the -o
    command line argument is used as the filename prefix for all frames
    """
    if args.outfile == '-' or args.outfile == '':
        print('File prefix required for dumping frames.')
        print('Please specify prefix with -o')
        sys.exit(0)

    video = vftk.Video(args.infile)

    while True:
        outfile = args.outfile + \
             str(video.frameNumber()).zfill(len(str(video.nframes()))) + \
             '.png'

        write_png(outfile, video, video.getFrame())
        if not video.seekNextFrame():
            break


def mode_mv(args):
    """
    Writes out motion vectors for each frame in sequential order.
    """
    video = vftk.Video(args.infile)

    with sane_open(args.outfile) as outfile_handle:
        while True:
            frame = video.getFrame()

            for mv in frame.motionVectors():
                print('%i [%2i]: %2ix%2i: (%4i, %4i) => (%4i, %4i)' %
                      (video.frameNumber(), mv['source'],
                       mv['size'][0], mv['size'][1],
                       mv['src'][0], mv['src'][1],
                       mv['dst'][0], mv['dst'][1]), file=outfile_handle)

            if not video.seekNextFrame():
                break


def get_arguments():
    """
    Parses the command line and returns the options
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('-m', '--mode',
                        type=str,
                        dest='mode',
                        default='',
                        required=True,
                        help="""
                             'mv' for motion vector extraction,
                             'ftypes' to list frame types,
                             'frames' to dump frames to disk,
                             'perror' to dump frame prediction errors
                             """
                        )

    parser.add_argument('-i', '--input',
                        type=str,
                        dest='infile',
                        default='',
                        required=True,
                        help="Input video filename")

    parser.add_argument('-o', '--output',
                        type=str,
                        dest='outfile',
                        default='',
                        help="Output video filename (default: stdout)")

    return parser.parse_args()


def main():
    """
    Program entry point.
    """
    modes = {
        'mv': mode_mv,
        'ftypes': mode_ftypes,
        'frames': mode_frames
    }

    args = get_arguments()
    modes.get(args.mode, mode_todo)(args)


if __name__ == '__main__':
    main()
