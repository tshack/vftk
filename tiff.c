/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <tiffio.h>
#include "frame.h"
#include "video.h"

int tiff_writer (const char* filename, Frame* F)
{
    unsigned int i, j;

    int ret = EXIT_SUCCESS;;
    unsigned char* buf = NULL;
    TIFF *out = TIFFOpen (filename, "w");

    if (!out) {
        ret = EXIT_FAILURE;
        goto cleanup;
    }

    unsigned int width = F->w;
    unsigned int height = F->h;
    unsigned int channels = 3;

    TIFFSetField (out, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField (out, TIFFTAG_IMAGELENGTH, height);
    TIFFSetField (out, TIFFTAG_SAMPLESPERPIXEL, channels);
    TIFFSetField (out, TIFFTAG_BITSPERSAMPLE, 8);
    TIFFSetField (out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField (out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField (out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);

    tsize_t linebytes = channels*width;

    if (TIFFScanlineSize(out))
        buf = (unsigned char*)_TIFFmalloc (linebytes);
    else
        buf = (unsigned char*)_TIFFmalloc (TIFFScanlineSize(out));

    if (!buf){
        ret = EXIT_FAILURE;
        goto cleanup;
    }

    TIFFSetField (out, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(out, width*channels));

    for (j=0; j<height; j++) {
        for (i=0; i<width; i++) {
            buf[i*channels + 0] = F->R[width*j + i];
            buf[i*channels + 1] = F->G[width*j + i];
            buf[i*channels + 2] = F->B[width*j + i];
        }

        if (TIFFWriteScanline(out, buf, j, 0)<0)
            break;
    }

cleanup:
    if (out)
        TIFFClose(out);
    if (buf)
        _TIFFfree(buf);
    return ret;
}
