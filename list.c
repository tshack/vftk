/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include "list.h"

void list_init(struct list* head)
{
    head->next = head;
    head->prev = head;
}


struct list* list_first_item(struct list* head)
{
    return head->next;
}


struct list* list_last_item(struct list* head)
{
    return head->prev;
}


int list_is_empty(struct list* head)
{
    return head->next == head;
}


void list_add_after(struct list* cur, struct list* new)
{
    new->next = cur->next;
    new->prev = cur;
    cur->next->prev = new;
    cur->next = new;
}


void list_add_before(struct list* cur, struct list* new)
{
    new->next = cur;
    new->prev = cur->prev;
    cur->prev->next = new;
    cur->prev = new;
}


void list_remove(struct list* node)
{
    node->prev->next = node->next;
    node->next->prev = node->prev;
    node->prev = NULL;
    node->next = NULL;
}


int list_count(struct list* head)
{
    struct list* cur;
    int i = 0;

    for (cur = head->next; cur != head; cur = cur->next)
        i++;

    return i;
}
