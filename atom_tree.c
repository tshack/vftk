/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "internal/util.h"

#include "atom.h"
#include "list.h"

#define min(a,b) a < b ? a : b

typedef struct {
    char* data;
    List node;
} Glyph;


static void _to_system_endian (void* val_ptr, size_t nbytes)
{
    if (util_is_little_endian())
        util_swap_endian (val_ptr, nbytes);
}


static int _is_atom (char* type)
{
    int i;

    for (i=0; i<4; i++)
        if (!isalnum(type[i]))
            return 0;

    return 1;
}


static Atom* _read_atom (FILE* fp, uint8_t* header_size)
{
    Atom* atom = malloc (sizeof(*atom));

    list_init (&atom->head_children);
    atom->offset = ftell (fp);
    atom->size = 0;

    *header_size = 8;

    if (1 != fread (&atom->size, 4, 1, fp))
        goto out;

    if (4 != fread (atom->type, 1, 4, fp))
        goto out;

    atom->type[4] = '\0';

    if (!_is_atom (atom->type))
        goto out;

    _to_system_endian (&atom->size, 4);

    /* Extended Size Atom/Box (>2^32 bytes) */
    if (atom->size == 1) {
        if (1 != fread (&atom->size, 8, 1, fp))
            goto out;

        _to_system_endian (&atom->size, 8);

        *header_size += 8;
    }
    /* Atom Extends to EOF */
    else if (atom->size == 0) {
        long save = ftell (fp);

        fseek (fp, 0L, SEEK_END);
        atom->size = ftell (fp) + *header_size;
        fseek (fp, save, SEEK_SET);
    }

    return atom;

out:
    free (atom);
    return NULL;
}


static inline void _link_in (AtomTree* tree, Atom* parent, Atom* child)
{
    List* node_parent = !parent ? &tree->head_tree : &parent->head_children;

    child->parent = parent;
    list_add_before (node_parent, &child->tree);
    list_add_before (&tree->head_all, &child->all);
}


static void _get_children (AtomTree* tree, Atom* parent, uint64_t bytes_left)
{
    Atom* atom;
    long save;
    uint8_t header_size;

    while (bytes_left > 7) {
        save = ftell (tree->fp);

        if ( !(atom = _read_atom (tree->fp, &header_size)) )
            return;

        /* JAS 2019.02.27 -- child should never extend
         * beyond parent unless the container is corrupt! */
        if (bytes_left < atom->size)
            return;
        else
            bytes_left -= atom->size;

        _link_in (tree, parent, atom);
        _get_children (tree, atom, atom->size - header_size);

        fseek (tree->fp, save, SEEK_SET);
        fseek (tree->fp, atom->size, SEEK_CUR);
    }
}


static int _tree_fprint (FILE* fp, List* atoms, List* glyphs)
{
    Glyph level, *glyph;
    List *cur, *gcur;
    Atom* atom;

    int n = 0;

    level.data = "|   ";

    list_foreach (atoms, cur) {
        atom = container_of (cur, Atom, tree);

        list_foreach (glyphs, gcur) {
            glyph = container_of (gcur, Glyph, node);
            n += fprintf (fp, "%s", glyph->data);
        }

        n += fprintf (fp, "+-- ");

        n += fprintf (fp, "%s [%"PRIu64" bytes] @ 0x%lx\n",
                atom->type, atom->size, atom->offset);

        if (cur->next == atoms)
            level.data = "    ";

        if (!list_is_empty (&atom->head_children)) {
            list_add_before (glyphs, &level.node);
            n += _tree_fprint (fp, &atom->head_children, glyphs);
            list_remove (&level.node);
        }
    }

    return n;
}


int atom_tree_fprint (FILE* fp, AtomTree* tree)
{
    List glyphs;

    list_init (&glyphs);
    return _tree_fprint (fp, &tree->head_tree, &glyphs);
}


static int _hexdump_fprint (FILE* out, FILE* in, long offset, size_t size)
{
    unsigned char bytes[16];
    int i;

    long n = 0;
    int  p = 0;
    long read = 0;
    long save = ftell (in);

    long prefix = offset % 16;
    long start = offset - prefix;

    fseek (in, start, SEEK_SET);

    while (read < size + prefix) {
        p += fprintf (out, "%08lx:", start + read);

        n = fread (bytes, 1, min(16, size + prefix - read), in);

        for (i=0; i<16; i++) {
            if (i % 2 == 0)
                p += fprintf (out, " ");

            if ( (start + read + i < offset) || i >= n)
                p += fprintf (out, "  ");
            else
                p += fprintf (out, "%02x", bytes[i]);
        }

        p += fprintf (out, "  ");

        for (i=0; i<n; i++) {
            if (start + read + i < offset)
                p += fprintf (out, " ");
            else {
                if (!isprint(bytes[i]))
                    bytes[i] = '.';
                p += fprintf (out, "%c", bytes[i]);
            }
        }

        p += fprintf (out, "\n");

        read += n;
    }

    fseek (in, save, SEEK_SET);

    return p;
}


int atom_hexdump_fprint (FILE* fp, AtomTree* at, Atom* atom)
{
    return _hexdump_fprint (fp, at->fp, atom->offset, atom->size);
}


Atom* atom_tree_find (AtomTree* tree, const char* id)
{
    List* cur;
    char needle[4];

    int i, n, N;
    Atom* atom = NULL;
    List* head = &tree->head_tree;

    i = n = N = 0;

    while (*id) {
        while (i < 4 && (*id != '.' || *id != '\0'))
            needle[i++] = *id++;

        if (*id == '[' && id++)
            while (*id++ != ']')
                N = 10 * N + *(id - 1) - '0';

        list_foreach (head, cur) {
            atom = container_of (cur, Atom, tree);

            if (!strncmp (atom->type, needle, 4)) {
                if (n != N)
                    n++;
                else goto descend;
            }
        }

        return NULL;

    descend:
        i = n = N = 0;
        if (*id) id++;
        head = &atom->head_children;
    }

    return atom;
}


AtomTree* atom_tree_create (const char * const filename)
{
    AtomTree* tree;
    Atom* atom;
    uint8_t header_size;
    long save;

    tree = malloc (sizeof(*tree));
    list_init (&tree->head_tree);
    list_init (&tree->head_all);

    if ( !(tree->fp = fopen (filename, "rb")) )
        goto out;

    save = ftell (tree->fp);

    for (; (atom = _read_atom (tree->fp, &header_size)); save = ftell (tree->fp)) {
        _link_in (tree, NULL, atom);
        _get_children (tree, atom, atom->size - header_size);

        fseek (tree->fp, save, SEEK_SET);
        fseek (tree->fp, atom->size, SEEK_CUR);
    }

    return tree;

out:
    free (tree);
    return NULL;
}


void atom_tree_destroy (AtomTree** tree)
{
    Atom* atom;
    List* cur;
    List* sav;

    list_foreach_safe (&(*tree)->head_all, cur, sav) {
        atom = container_of (cur, Atom, all);
        list_remove (cur);
        free (atom);
    }

    fclose ((*tree)->fp);
    free (*tree);
    *tree = NULL;
}
