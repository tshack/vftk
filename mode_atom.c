/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "internal/argparse.h"
#include "internal/util.h"

#include "atom.h"
#include "list.h"

#define to_atomdata(ptr) container_of(ptr, AtomData, node)

typedef struct {
    int help;
    int tree;
    char* dump;
    char* parse;
} Opts;


static Opts opts = {
    .help = 0,
    .tree = 0,
    .dump = NULL,
    .parse = NULL
};


static void print_usage (ArgParse* P)
{
    printf ("Usage: vftk atom [OPTIONS...] INPUT\n"
            "\n"
            "Options:\n");

    argparse_print (P);

    printf ("\n");

    printf ("A Fully Qualified Atom Location (FQAL) is a unique identifer used to\n"
            "specify a particular atom within the atom tree (e.g. ftype, moov.mvhd,\n"
            "moov.trak[0].mdia.minf.dinf.dref, moov.trak[1].mdia.minf.stbl.stts)\n");

    printf ("\n");
}


static ArgParse* _parse (int argc, char* argv[])
{
    int ret;

    ArgParse* P = argparse_new (argc, argv);

    argparse_add (P, ARG_FLAG, "-t", "--tree",
            "print MP4 container's atom tree to standard output",
            &opts.tree);

    argparse_add (P, ARG_STR, "-d", "--dump=FQAL",
            "print hex dump of atom content for atom at FQAL",
            &opts.dump);

    argparse_add (P, ARG_STR, "-p", "--parse=FQAL",
            "print parsed contents of atom at FQAL",
            &opts.parse);

    argparse_add (P, ARG_FLAG, "-h", "--help",
            "display this help text and exit", &opts.help);

    if (!argparse (P)) {
        ret = EXIT_FAILURE;
        goto out;
    }

    if (opts.help) {
        print_usage (P);
        ret = EXIT_SUCCESS;
        goto out;
    }

    if (P->argc < 1) {
        fprintf (stderr, "Must specify INPUT\n");
        ret = EXIT_FAILURE;
        goto out;
    }

    return P;

out:
    argparse_destroy (&P);
    exit (ret);
}


static void _atom_dump (AtomTree* at)
{
    Atom* atom;

    atom = atom_tree_find (at, opts.dump);
    if (!atom) {
        fprintf (stderr, "error -- atom '%s' not found.\n", opts.dump);
        return;
    }

    printf ("%"PRIu64" byte hex dump of atom '%s':\n",
            atom->size, opts.dump);

    atom_hexdump_fprint (stdout, at, atom);
}


static void _print_parse (List* parse)
{
    List* cur;
    AtomData* ad;
    size_t i;

    list_foreach (parse, cur) {
        ad = to_atomdata (cur);

        printf ("  %s: ", ad->name);

        switch (ad->type) {
        case ATOM_DATA_INT8:
            for (i=0; i<ad->nelements; i++)
                printf ("0x%02"PRIx8 " ", ((int8_t*)(ad->data))[i]);
            break;
        case ATOM_DATA_INT16:
            for (i=0; i<ad->nelements; i++)
                printf ("0x%04"PRIx16 " ", ((int16_t*)(ad->data))[i]);
            break;
        case ATOM_DATA_INT32:
            for (i=0; i<ad->nelements; i++)
                printf ("0x%08"PRIx32 " ", ((int32_t*)(ad->data))[i]);
            break;
        case ATOM_DATA_INT64:
            for (i=0; i<ad->nelements; i++)
                printf ("0x%016"PRIx64 " ", ((int64_t*)(ad->data))[i]);
            break;
        case ATOM_DATA_UINT8:
            for (i=0; i<ad->nelements; i++)
                printf ("0x%02"PRIx8 " ", ((int8_t*)(ad->data))[i]);
            break;
        case ATOM_DATA_UINT16:
            for (i=0; i<ad->nelements; i++)
                printf ("0x%04"PRIx16 " ", ((int16_t*)(ad->data))[i]);
            break;
        case ATOM_DATA_UINT32:
            for (i=0; i<ad->nelements; i++)
                printf ("0x%08"PRIx32 " ", ((int32_t*)(ad->data))[i]);
            break;
        case ATOM_DATA_UINT64:
            for (i=0; i<ad->nelements; i++)
                printf ("0x%016"PRIx64 " ", ((int64_t*)(ad->data))[i]);
            break;
        case ATOM_DATA_STRING:
            for (i=0; i<ad->nelements; i++)
                printf ("'%s' ", ((char**)(ad->data))[i]);
            break;
        default:
            break;
        }

        printf ("\n");
    }
}


static void _atom_parse (AtomTree* at)
{
    Atom* atom;
    List* parse;

    atom = atom_tree_find (at, opts.parse);
    if (!atom) {
        fprintf (stderr, "error -- atom '%s' not found.\n", opts.parse);
        return;
    }

    parse = atom_parse (at, atom);
    if (!parse) {
        fprintf (stderr, "error -- no parser for atom type '%s'\n",
                atom->type);
        return;
    }

    printf ("Parsed contents of atom '%s':\n", opts.parse);
    _print_parse (parse);
    atom_parse_destroy (&parse);
}


int mode_atom (int argc, char* argv[])
{
    ArgParse* P;
    AtomTree* at;

    int ret = EXIT_SUCCESS;

    P = _parse (argc, argv);

    at = atom_tree_create (P->argv[0]);
    if (!at) {
        fprintf (stderr, "error -- failed to generate MP4 atom tree for '%s'\n", P->argv[0]);
        ret = EXIT_FAILURE;
        goto out;
    }

    if (opts.tree) {
        atom_tree_fprint (stdout, at);
    } else if (opts.dump) {
        _atom_dump (at);
    } else if (opts.parse) {
        _atom_parse (at);
    } else
        fprintf (stderr, "No operation specified\n");

    atom_tree_destroy (&at);
out:
    argparse_destroy (&P);
    return ret;
}
