# Building the VFTK Cython Module

The `vftk` Python module wraps core `libvftk` functionality, exposing
VFTK functionality for rapid and accessible Python development.  To
build this module, you will need to satisfy the following additional
build dependencies:

1. `Cython`
2. `Numpy >= 1.7`

## Procedure

Clone the VFTK repo, enter it, and create a build directory:
```
$ git clone git@gitlab.com:tshack/vftk.git
$ cd vftk
$ mkdir build/
```

Generate the build system within the `build/` directory
```
$ cd build/
$ cmake -DBUILD_CYTHON=ON ..
```

Perform the build:
```
$ make
```

Install:
```
$ make install
```

Upon success, you should now be able to `import vftk` in Python.

*Note: If you built the Python module against Python3, it will only be
importable to Python3.  Likewise, if built against Python2, it will only
be importable to Python2.*


## Trying It Out

Let's give the `vftk` Python module a test drive.

In the `python/` directory, you will find a Python implementation of the
`vftk` command line application named `vftk.py` along with its
`requirements.txt` dependencies list.

Now, since we are good kids, we don't install packages from `pip`
globally with `sudo`.  So, let's create a virtual environment and
activate it:

```
$ cd python/
$ python -m venv my_venv
$ source my_venv/bin/activate
```

Now let's install the module dependencies needed by `vftk.py`
into our virtual environment using `pip`:
```
$ pip install -r requirements.txt
```

Run the program and have fun.  It's probably a good idea to first look
at the help screen to see what the options are:
```
$ python vftk.py -h
```

When you are done, you are going to want to exit the virtual
environment:
```
$ deactivate
```

## Examples

In addition to `python/vftk.py`, there are several bite-sized examples
to help you get started!  They are located in `python/examples`

I highly recommend you take a look at these examples!


## Getting Help
The VFTK module has full docstring documentation.  To access this,
spin up the interactive Python REPL, import the module, and get help on
it:

```
$ python
>>> import vftk
>>> help(vftk)
```

## What's Next?
Think of functionality you want and send me patches or submit feature
requests in the [Issue Tracker](https://www.gitlab.com/tshack/vftk/issues)!

Cheers.
