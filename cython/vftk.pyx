# cython: embedsignature=True
#####################################################################
# Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
#####################################################################
"""
Python bindings for the Video Forensics Toolkit (vftk)
"""
from libc.stdlib cimport malloc, free
from cpython cimport PyObject, Py_INCREF, Py_DECREF
import numpy as np
cimport numpy as np


np.import_array()


cdef class MotionVectorIterator:
    """
    Iterates over the motion vectors of the producing frame.  Each
    iteration returns a dictionary with the following key:value pairs:

    'source': int        -- previous frame (-1) or next frame (1)
      'size': (int, int) -- size of macroblock
       'src': (int, int) -- coordinate of macroblock in current frame
       'dst': (int, int) -- coordinate of macroblock in destination frame
    """
    def __cinit__ (self, Frame F):
        self.F = F
        self.idx = 0

    def __iter__ (self):
        return self

    def __next__ (self):
        cdef AVMotionVector* mv
        out = dict()

        if self.idx < self.F.ctx.num_motion_vecs:
            mv = &(self.F.ctx.motion_vecs[self.idx])
            out['source'] = mv.source
            out['size'] = (mv.w, mv.h)
            out['src'] = (mv.src_x, mv.src_y)
            out['dst'] = (mv.dst_x, mv.dst_y)
            self.idx += 1
            return out
        else:
            raise StopIteration


cdef class Frame:
    """
    Represents a video frame, providing access to the pixel buffer and
    motion vectors.
    """
    def __cinit__ (self, Video vid):
        self.vid = vid
        self.ctx = frame_new()
        video_get_frame (self.vid.ctx, self.ctx)

    def properties (self):
        """
        Returns a dictionary of frame color properties.

        Dictionary Keys:
          'chroma_loc' - location of chroma samples
          'primaries' - chromaticity coordinates (ISO/IEC 23001-8_2013 § 7.1)
          'range' - color range (MPEG YUV or JPEG YUV)
          'xfer_char' - color transfer characteristic code (ISO/IEC 23001-8_2013 § 7.2)
          'interlaced' - the frame is interlaced (Boolean)
          'entropy_coding' - 'CABAC', 'CAVLC', or 'Unknown'
        """
        AVChromaLocation = {
            0: 'Unknown',
            1: 'Left',
            2: 'Center',
            3: 'TopLeft',
            4: 'Top',
            5: 'BottomLeft',
            6: 'Bottom'
        }

        AVColorRange = {
            0: 'Unspecified',
            1: 'MPEG YUV',
            2: 'JPEG YUV'
        }

        if self.ctx.is_cabac == 1:
            entropy_coding = 'CABAC'
        elif self.ctx.is_cabac == 0:
            entropy_coding = 'CAVLC'
        else:
            entropy_coding = 'Unknown'

        props = {
            'chroma_loc': AVChromaLocation[self.ctx.chroma_location],
            'primaries': self.ctx.color_primaries,
            'range': AVColorRange[self.ctx.color_range],
            'xfer_char': self.ctx.color_xfer_characteristic,
            'interlaced': True if self.ctx.is_interlaced else False,
            'entropy_coding': entropy_coding
        }

        return props

    def imagePlanes (self):
        """
        Returns a tuple of 3 numpy arrays containing the red, blue, and
        green color planes in RGB order.
        """
        cdef np.ndarray R
        cdef np.ndarray G
        cdef np.ndarray B
        cdef np.npy_intp shape[2]

        shape[1] = <np.npy_intp>video_peek_width (self.vid.ctx)
        shape[0] = <np.npy_intp>video_peek_height (self.vid.ctx)

        R = np.PyArray_SimpleNewFromData (2, shape, np.NPY_UINT8, self.ctx.R)
        G = np.PyArray_SimpleNewFromData (2, shape, np.NPY_UINT8, self.ctx.G)
        B = np.PyArray_SimpleNewFromData (2, shape, np.NPY_UINT8, self.ctx.B)
        np.PyArray_SetBaseObject(R, self)
        Py_INCREF(self)
        np.PyArray_SetBaseObject(G, self)
        Py_INCREF(self)
        np.PyArray_SetBaseObject(B, self)
        Py_INCREF(self)

        return (R, G, B)

    def motionVectors (self):
        """
        Returns a MotionVectorIterator object, which can be used to
        iterate through the motion vectors of the frame.
        """
        return MotionVectorIterator (self)

    def __dealloc__ (self):
        frame_destroy (&self.ctx)


cdef class Video:
    """
    Represents an open video stream on the disk.  Instantiation requires
    that a filename to a valid video file be passed.

    Example:
        video = Video('paris.mp4')
    """
    def __cinit__ (self, filename):
        cdef ErrorCode err;

        if isinstance (filename, str):
            fn = filename.encode()
        elif isinstance (filename, bytes):
            fn = filename
        else:
            raise Exception ("bad argument type (filename)")

        err = video_open (&self.ctx, fn)

        if err != SUCCESS:
            raise Exception (err2str(err))

    def __dealloc__ (self):
        video_destroy (&self.ctx)

    def time (self):
        """
        Returns the current presentation time (s)
        """
        return video_peek_time (self.ctx)

    def width (self):
        """
        Returns the presentation frame width
        """
        return video_peek_width (self.ctx)

    def height (self):
        """
        Returns the presentation frame height
        """
        return video_peek_height (self.ctx)

    def nframes (self):
        """
        Returns the total number of frames
        """
        return video_peek_nframes (self.ctx)

    def frameNumber (self):
        """
        Returns the current frame number.
        """
        return video_peek_framenumber (self.ctx)

    def frameType (self):
        """
        Returns the current frame's type as a string.

        Possible values include 'I', 'P', 'B', and 'UNKNOWN_FRAME'.
        """
        ftypes  = {UNKNOWN_FRAME: 'UNKNOWN_FRAME',
                   I_FRAME: 'I',
                   P_FRAME: 'P',
                   B_FRAME: 'B'}

        return ftypes[video_peek_frametype (self.ctx)]

    def profile (self):
        """
        Returns the h264 profile code for the video.
        """
        return video_peek_profile (self.ctx)

    def level (self):
        """
        Returns the h264 level for the video, dictating decoder
        performance.
        """
        code = video_peek_level (self.ctx)
        return str(code//10) + "." + str(code%10)

    def gop (self):
        """
        Returns a tuple of the GOP N, M values (N, M)
        """
        cdef int N
        cdef int M

        video_peek_gop (self.ctx, &M, &N)

        return (N, M)

    def isVFR (self):
        """
        Returns True if video is variable frame rate; otherwise False
        """
        cdef int ret = video_is_vfr (self.ctx)
        if ret == 1:
            return True
        else:
            return False

    def bitrate (self):
        """
        Returns the video bitrate (in bits per second)
        """
        return video_peek_bitrate (self.ctx)

    def allFrameTypes (self):
        """
        Returns a string of 'nframes()' characters, where each character
        is either 'I', 'P, 'B', or '?' -- corresponding to each frame's
        type (in presentation order).
        """
        return video_peek_all_frame_types (self.ctx).decode()

    def seekNextFrame (self):
        """
        Seeks the video to the next frame.

        Returns False if there are no more frames; otherwise True.
        """
        cdef int ret = video_seek_next_frame (self.ctx)
        if ret == 1:
            return True
        else:
            return False

    def seekFrame (self, frame_number):
        """
        Seeks the video to the specified frame number.

        Raises an Exception on failure
        """
        cdef int ret = video_seek_frame (self.ctx, frame_number)
        if ret != 1:
            raise Exception ('Seek Failed')

    def getFrame (self):
        """
        Returns a Frame object containing the current frame's pixel
        buffer and motion vectors.
        """
        return Frame(self)
