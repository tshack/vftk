#####################################################################
# Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
#####################################################################

from libc.stdint cimport uint8_t, int16_t, uint16_t, int32_t, int64_t, uint64_t
cimport numpy as np

cdef extern from "libavutil/motion_vector.h":
    ctypedef struct AVMotionVector:
        int32_t source
        uint8_t w
        uint8_t h
        int16_t src_x
        int16_t src_y
        int16_t dst_x
        int16_t dst_y
        uint64_t flags
        int32_t motion_x
        int32_t motion_y
        uint16_t motion_scale


cdef extern from "../include/error.h":
    ctypedef enum ErrorCode:
        SUCCESS = 0,
        EFILE_IO,
        ESTREAM,
        ESTREAM_INFO,
        ECODEC_ALLOC,
        ECODEC_PARAMS,
        ECODEC_IO,
        EFRAME_ALLOC,
        EFRAME_MV,
        EFRAME_RGB24,

    const char* const err2str (ErrorCode e)


cdef extern from "../include/video.h":
    ctypedef struct CVideo "Video"
    ErrorCode video_open (CVideo** V, char* infile)
    void video_destroy (CVideo** V)
    double video_peek_time (CVideo* V)
    int video_peek_width (CVideo* V)
    int video_peek_height (CVideo* V)
    int64_t video_peek_nframes (CVideo* V)
    int64_t video_peek_framenumber (CVideo* V)
    FrameType video_peek_frametype (CVideo* V)
    int video_seek_next_frame (CVideo* V)
    int video_seek_frame (CVideo* V, int64_t frame_number)
    void video_get_frame (CVideo* V, CFrame* F)
    int video_peek_profile (CVideo* V)
    int video_peek_level (CVideo* V)
    int video_peek_gop (CVideo* V, int* m, int* n)
    int video_is_vfr (CVideo* V)
    int64_t video_peek_bitrate (CVideo* V)
    const char* video_peek_all_frame_types (CVideo* V)


cdef extern from "../include/frame.h":
    ctypedef enum FrameType:
        UNKNOWN_FRAME,
        I_FRAME,
        P_FRAME,
        B_FRAME

    ctypedef struct CFrame "Frame":
        size_t number
        FrameType type
        double time_s

        uint8_t* R
        uint8_t* G
        uint8_t* B
        size_t w
        size_t h

        AVMotionVector* motion_vecs
        size_t num_motion_vecs

        uint8_t chroma_location
        uint8_t color_primaries
        uint8_t color_range
        uint8_t color_xfer_characteristic
        uint8_t is_interlaced
        uint8_t is_cabac

    CFrame* frame_new ()
    void frame_destroy (CFrame** F)


cdef class MotionVectorIterator:
    cdef Frame F
    cdef idx


cdef class Video:
    cdef CVideo* ctx


cdef class Frame:
    cdef CFrame* ctx
    cdef Video vid
