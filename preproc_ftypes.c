/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <string.h>

#include "internal/preproc.h"

#include "error.h"
#include "frame.h"
#include "video.h"

typedef struct PreProcData {
    char* ftypes;
} PreProcData;

#define OFFSET(attr) offsetof(PreProcData, attr)

static const PreProcExport exports[] = {
    {.name = "all_frame_types", .type = PTR, .data = OFFSET(ftypes)},
    { NULL }
};


static ErrorCode init (Video* V, PreProcData* D)
{
    size_t size = (video_peek_nframes (V) + 1) * sizeof(*D->ftypes);

    D->ftypes = malloc (size);

    if (!D->ftypes)
        return EPREPROC_INIT;

    memset (D->ftypes, 0, size);

    return SUCCESS;
}


static void perframe (Video* V, PreProcData* D, int64_t framenumber)
{
        switch (video_peek_frametype(V)) {
        case I_FRAME:
            D->ftypes[framenumber] = 'I';
            break;
        case P_FRAME:
            D->ftypes[framenumber] = 'P';
            break;
        case B_FRAME:
            D->ftypes[framenumber] = 'B';
            break;
        default:
            D->ftypes[framenumber] = '?';
            break;
        }
}


static void fini (Video* V, PreProcData* D)
{
    free (D->ftypes);
}


NEW_PREPROC(ftypes) = {
    .name = "All Frame Types",
    .init = init,
    .perframe = perframe,
    .fini = fini,
    .exports = exports,
    .size = sizeof(PreProcData)
};
