/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include "libavutil/motion_vector.h"
#include "libavutil/timestamp.h"
#include "libavformat/avformat.h"

#include "internal/argparse.h"
#include "internal/util.h"

#include "error.h"
#include "frame.h"
#include "video.h"


typedef struct {
    int help;
} Opts;


static Opts opts = {
    .help = 0
};


static void print_usage (ArgParse* P)
{
    printf ("Usage: vftk ftypes [OPTIONS...] INPUT\n"
            "\n"
            "Options:\n");

    argparse_print (P);
}


static ArgParse* _parse (int argc, char* argv[])
{
    int ret;

    ArgParse* P = argparse_new (argc, argv);

    argparse_add (P, ARG_FLAG, "-h", "--help",
            "display this help text and exit", &opts.help);

    if (!argparse (P)) {
        ret = EXIT_FAILURE;
        goto out;
    }

    if (opts.help) {
        print_usage (P);
        ret = EXIT_SUCCESS;
        goto out;
    }

    if (P->argc < 1) {
        fprintf (stderr, "Must specify INPUT\n");
        ret = EXIT_FAILURE;
        goto out;
    }

    return P;

out:
    argparse_destroy (&P);
    exit (ret);
}


static void _dump_ftypes (Video* V)
{
    printf ("Frame_Type, Frame_Number, Presentation_Time_(s)\n");

    do {
        printf ("%s, %"PRId64", %.3f\n",
                _frametype2str (video_peek_frametype (V)),
                video_peek_framenumber (V),
                video_peek_time (V));
    } while (video_seek_next_frame (V));

    printf ("done.\n\n");
}


int mode_ftypes (int argc, char* argv[])
{
    ErrorCode err;
    ArgParse* P;
    Video* V;

    int ret = EXIT_SUCCESS;;


    P = _parse (argc, argv);

    err = video_open (&V, P->argv[0]);
    if (err) {
        fprintf (stderr, "%s ['%s']\n", err2str (err), P->argv[0]);
        ret = EXIT_FAILURE;
        goto out;
    }

    _dump_ftypes (V);

    video_destroy (&V);
out:
    argparse_destroy (&P);
    return ret;
}
