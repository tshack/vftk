macro(create_output_dir dir)
    file (MAKE_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${dir})
endmacro()


macro (set_executable_subdir target subdir)
    set_target_properties ("${target}" PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${subdir}")
endmacro()


macro(new_test src_name test_name)
    add_executable ("${src_name}_test" ${CMAKE_SOURCE_DIR}/test/${src_name}.c)
    set_executable_subdir ("${src_name}_test" "test")
    target_link_libraries ("${src_name}_test" libvftk)
    add_test (NAME ${test_name}
        COMMAND "${src_name}_test"
        WORKING_DIRECTORY "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/test/")
endmacro()


macro(copy_test_videos loc items)
    create_output_dir (${loc})
    foreach(item IN LISTS ${items})
        set(TEST_VIDEO_DST "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${loc}/${item}")
        set(TEST_VIDEO_SRC "${CMAKE_SOURCE_DIR}/test/videos/${item}")
        set(TEST_VIDEOS_DEP ${TEST_VIDEOS_DEP} ${TEST_VIDEO_DST})
        add_custom_command(
            OUTPUT "${TEST_VIDEO_DST}"
            COMMAND ${CMAKE_COMMAND} -E copy "${TEST_VIDEO_SRC}" "${TEST_VIDEO_DST}"
            DEPENDS "${TEST_VIDEO_SRC}"
        )
    endforeach()
    add_custom_target(test-videos ALL DEPENDS ${TEST_VIDEOS_DEP})
endmacro()
