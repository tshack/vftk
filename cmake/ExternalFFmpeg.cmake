macro(ExternalFFMpeg target tag src_dir install_dir)
    find_package (Git REQUIRED)
    include(ProcessorCount)
    ProcessorCount(NUM_CORES)

    set(FFMPEG_LIBRARIES
        "${install_dir}/lib/libavformat.a"
        "${install_dir}/lib/libavcodec.a"
        "${install_dir}/lib/libavfilter.a"
        "${install_dir}/lib/libavutil.a"
        "${install_dir}/lib/libavdevice.a"
        "${install_dir}/lib/libswscale.a"
        "${install_dir}/lib/libswresample.a"
    )

    include(ExternalProject)
    ExternalProject_Add(
        ${target}
        GIT_REPOSITORY https://git.ffmpeg.org/ffmpeg.git
        GIT_TAG "n4.0"
        PATCH_COMMAND ${GIT_EXECUTABLE} apply ${CMAKE_CURRENT_SOURCE_DIR}/patch/0001-libavcodec-h264-save-frame-prediction-error.patch ${CMAKE_CURRENT_SOURCE_DIR}/patch/0002-mathops-clip-constants-used-with-shift.patch

        SOURCE_DIR ${src_dir}
        BUILD_IN_SOURCE 1

        # JAS 2019-02-18: MMX optimizations must be disabled for the
        # time being since they seem to interfere with my prediction
        # error extraction patch to FFMpeg for a small subset of test
        # videos, resulting in a double free driven crash.
        CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=${install_dir}
                                                 --disable-shared
                                                 --enable-static
                                                 --enable-pic
                                                 --extra-libs=-static
                                                 --disable-doc
                                                 --disable-mmx
                                                 #--disable-optimizations
                                                 #--disable-stripping

        BUILD_COMMAND ${CMAKE_MAKE_PROGRAM} -j${NUM_CORES}

        STEP_TARGETS ${FFMPEG_LIBRARIES}
    )

    set(FFMPEG_INCLUDE_DIRS ${src_dir} CACHE STRING "The FFmpeg include directories." FORCE)
    set(FFMPEG_LIBRARIES    ${FFMPEG_LIBRARIES} CACHE STRING "The FFmpeg libraries." FORCE)

    mark_as_advanced(FFMPEG_INCLUDE_DIRS FFMPEG_LIBRARIES)
endmacro()
