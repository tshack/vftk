/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#ifndef _VFTK_FRAME_H_
#define _VFTK_FRAME_H_

#include <stdint.h>
#include "error.h"

typedef struct AVMotionVector AVMotionVector;
typedef struct AVFrame AVFrame;

typedef enum FrameType {
    UNKNOWN_FRAME,
    I_FRAME,
    P_FRAME,
    B_FRAME
} FrameType;

typedef struct Frame {
    int64_t number;
    FrameType type;
    double time_s;

    uint8_t* R;
    uint8_t* G;
    uint8_t* B;
    size_t w;
    size_t h;

    AVMotionVector* motion_vecs;
    size_t num_motion_vecs;

    uint8_t chroma_location;
    uint8_t color_primaries;
    uint8_t color_range;
    uint8_t color_xfer_characteristic;
    uint8_t is_interlaced;
    uint8_t is_cabac;
} Frame;

Frame* frame_new ();
Frame* frame_new_blank (int width, int height, uint8_t* rgb);
Frame* frame_clone_empty (Frame* src);
void frame_fill (Frame* F, uint8_t* rgb);
void frame_destroy (Frame** F);
ErrorCode frame_set_picture (Frame* F, AVFrame* avframe);
ErrorCode frame_set_motion_vecs (Frame* F, AVFrame* avframe);

#endif /* _VFTK_FRAME_H_ */
