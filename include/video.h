/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#ifndef _VFTK_VIDEO_H_
#define _VFTK_VIDEO_H_

#include "error.h"
#include "frame.h"

typedef struct Options Options;
typedef struct Video Video;

/* Create / Destroy */
ErrorCode video_open (Video** V, char* infile);
void video_destroy (Video** V);

/* Info about Video / Current Frame */
double video_peek_time (Video* V);
int video_peek_width (Video* V);
int video_peek_height (Video* V);
int64_t video_peek_nframes (Video* V);
int64_t video_peek_framenumber (Video* V);
FrameType video_peek_frametype (Video* V);
double video_peek_frame_perror (Video* V);
int video_peek_profile (Video* V);
int video_peek_level (Video* V);
int video_is_avc (Video* V);
int video_is_vfr (Video* V);
int video_peek_frame_is_cabac (Video* V);
int video_peek_gop (Video* V, int* m, int* n);
int64_t video_peek_bitrate (Video* V);
const char* video_peek_all_frame_types (Video* V);

/* Video seek functions */
int video_seek_next_frame (Video* V);
int video_seek_frame (Video* V, int64_t framenumber);

/* Get the current frame */
void video_get_frame (Video* V, Frame* F);
ErrorCode video_get_frame_perror (Video* V, Frame* F);

/* Debug */
void video_print_format (Video* V);

#endif /* _VFTK_VIDEO_H_ */
