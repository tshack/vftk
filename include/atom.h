/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#ifndef _VFTK_ATOM_H_
#define _VFTK_ATOM_H_

#include <stdint.h>
#include <stdio.h>

#include "list.h"


typedef enum AtomDataType {
    ATOM_DATA_INT8,
    ATOM_DATA_INT16,
    ATOM_DATA_INT32,
    ATOM_DATA_INT64,
    ATOM_DATA_UINT8,
    ATOM_DATA_UINT16,
    ATOM_DATA_UINT32,
    ATOM_DATA_UINT64,
    ATOM_DATA_STRING
} AtomDataType;


typedef struct AtomData {
    char* name;
    AtomDataType type;
    void* data;
    size_t nelements;
    List node;
} AtomData;


typedef struct {
    FILE* fp;
    List head_tree;
    List head_all;
} AtomTree;


typedef struct Atom Atom;
struct Atom {
    uint64_t size;
    char type[5];
    long offset;

    Atom* parent;
    List head_children;

    List tree;
    List all;
};


AtomTree* atom_tree_create (const char * const filename);
void atom_tree_destroy (AtomTree** atoms);
int atom_tree_fprint (FILE* fp, AtomTree* tree);
Atom* atom_tree_find (AtomTree* tree, const char* id);
int atom_hexdump_fprint (FILE* fp, AtomTree* at, Atom* atom);

List* atom_parse (AtomTree* tree, Atom* atom);
void atom_parse_destroy (List** parse);

#endif /* _VFTK_ATOM_H_ */
