/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#ifndef _VFTK_ERROR_H_
#define _VFTK_ERROR_H_

typedef enum ErrorCode {
    SUCCESS = 0,
    EFILE_IO,          /* Failed to open file */
    ESTREAM,           /* Failed to find stream */
    ESTREAM_INFO,      /* Failed to find stream information */
    ESTREAM_NOFRAMES,  /* Video contains no frames! */
    ECODEC_ALLOC,      /* Failed to allocate codec */
    ECODEC_PARAMS,     /* Failed to copy codec params to context */
    ECODEC_IO,         /* Failed to open codec */
    EFRAME_ALLOC,      /* Failed to allocate frame */
    EFRAME_MV,         /* Failed to get motion vectors */
    EFRAME_RGB24,      /* Failed to convert frame to RGB24 */
    EPREPROC_ALLOC,    /* Failed to allocate preproc context */
    EPREPROC_INIT,     /* Failed to initialize preproc */
    EPREPROC_PKT,      /* Failed to receive packet */
    EATTR,             /* Failed to determine requested attribute */
} ErrorCode;

const char* const err2str (ErrorCode e);

#endif /* _VFTK_ERROR_H_ */
