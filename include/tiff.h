/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#ifndef _VFTK_TIFF_H_
#define _VFTK_TIFF_H_

typedef struct Frame Frame;

int tiff_writer (const char* filename, Frame* F);

#endif /* _VFTK_TIFF_H_ */
