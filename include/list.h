/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#ifndef _VFTK_LIST_H_
#define _VFTK_LIST_H_

#include <stddef.h>

#ifndef container_of
#define container_of(_mptr, type, member) \
    (type*)((char*)_mptr - offsetof(type, member))
#endif /* container_of */

typedef struct list List;
struct list {
    struct list* prev;
    struct list* next;
};

#define list_foreach(head, iter) \
    for (iter = (head)->next; iter != head; iter = iter->next)

#define list_foreach_safe(head, iter, bak) \
    for (iter = (head)->next, bak = iter->next; iter != head; \
            iter = bak, bak = bak->next)

void list_init(struct list* head);
struct list* list_first_item(struct list* head);
struct list* list_last_item(struct list* head);
int list_is_empty(struct list* head);
void list_add_after(struct list* cur, struct list* new);
void list_add_before(struct list* cur, struct list* new);
void list_remove(struct list* node);
int list_count(struct list* head);

#endif /* _VFTK_LIST_H_ */
