/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#ifndef _VFTK_PREPROC_H_
#define _VFTK_PREPROC_H_

#include <stddef.h>
#include <stdint.h>
#include "error.h"
#include "list.h"

#define PREPROC(name) _vftk_preproc_##name

#define NEW_PREPROC(name) PreProc PREPROC(name)

typedef struct Video Video;
typedef struct PreProcData PreProcData;

typedef enum {
    INT,
    INT64,
    PTR
} PreProcExportType;

typedef struct {
    char* name;
    PreProcExportType type;
    size_t data;
} PreProcExport;

typedef struct {
    const char* name;
    ErrorCode (*init)(Video* V, PreProcData* D);
    void (*perframe)(Video* V, PreProcData* D, int64_t framenumber);
    void (*perpacket)(Video* V, PreProcData* D, int64_t packet_number);
    void (*fini)(Video* V, PreProcData* D);
    const PreProcExport* exports;
    const size_t size;
    List node;
} PreProc;

typedef union {
    int int_;
    int64_t int64_;
    void* ptr_;
} PreProcReturn;

typedef struct {
    PreProc* P;
    PreProcData* D;
    int done;
    List node;
} PreProcContext;

ErrorCode preprocess_packets (Video* V);
void preprocess_frames (Video* V);
void preprocess_fini (Video* V);
PreProcReturn preproc_export (Video* V, char* data_name);

extern NEW_PREPROC(gopnm);
extern NEW_PREPROC(ts_luts);
extern NEW_PREPROC(ftypes);

#endif  /* _VFTK_PREPROC_H_ */
