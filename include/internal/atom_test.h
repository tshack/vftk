#ifndef _VFTK_INTERNAL_TEST_ATOM_
#define _VFTK_INTERNAL_TEST_ATOM_

#include "atom.h"
#include "list.h"

#define to_atomdata(ptr) container_of(ptr, AtomData, node)


static inline AtomData* _get_atomdata (List* parse, char* name)
{
    List* cur;
    AtomData* ad;

    list_foreach (parse, cur) {
        ad = to_atomdata (cur);
        if (!strcmp (ad->name, name))
            return ad;
    }

    return NULL;
}


static inline void _check_val (List* parse, char* name, const void* ans)
{
    size_t i;

    if (!name)
        return;

    AtomData* ad = _get_atomdata (parse, name);

    fprintf (stderr, "Checking '%s'\n", name);

    if (!ad) {
        fprintf (stderr, "Failed to get AtomData\n");
        exit (EXIT_FAILURE);
    }

    switch (ad->type) {
    case ATOM_DATA_INT8:
        for (i=0; i<ad->nelements; i++) {
            fprintf (stderr, "  Result: %"PRIx8"\n", ((int8_t*)(ad->data))[i]);
            fprintf (stderr, "Expected: %"PRIx8"\n", ((int8_t*)(ans))[i]);

            if (((int8_t*)(ans))[i] != ((int8_t*)(ad->data))[i] )
                exit (EXIT_FAILURE);
        }
        break;
    case ATOM_DATA_INT16:
        for (i=0; i<ad->nelements; i++) {
            fprintf (stderr, "  Result: %"PRIx16"\n", ((int16_t*)(ad->data))[i]);
            fprintf (stderr, "Expected: %"PRIx16"\n", ((int16_t*)(ans))[i]);

            if (((int16_t*)(ans))[i] != ((int16_t*)(ad->data))[i] )
                exit (EXIT_FAILURE);
        }
        break;
    case ATOM_DATA_INT32:
        for (i=0; i<ad->nelements; i++) {
            fprintf (stderr, "  Result: %"PRIx32"\n", ((int32_t*)(ad->data))[i]);
            fprintf (stderr, "Expected: %"PRIx32"\n", ((int32_t*)(ans))[i]);

            if (((int32_t*)(ans))[i] != ((int32_t*)(ad->data))[i] )
                exit (EXIT_FAILURE);
        }
        break;
    case ATOM_DATA_INT64:
        for (i=0; i<ad->nelements; i++) {
            fprintf (stderr, "  Result: %"PRIx64"\n", ((int64_t*)(ad->data))[i]);
            fprintf (stderr, "Expected: %"PRIx64"\n", ((int64_t*)(ans))[i]);

            if (((int64_t*)(ans))[i] != ((int64_t*)(ad->data))[i] )
                exit (EXIT_FAILURE);
        }
        break;
    case ATOM_DATA_UINT8:
        for (i=0; i<ad->nelements; i++) {
            fprintf (stderr, "  Result: %"PRIx8"\n", ((uint8_t*)(ad->data))[i]);
            fprintf (stderr, "Expected: %"PRIx8"\n", ((uint8_t*)(ans))[i]);

            if (((uint8_t*)(ans))[i] != ((uint8_t*)(ad->data))[i] )
                exit (EXIT_FAILURE);
        }
        break;
    case ATOM_DATA_UINT16:
        for (i=0; i<ad->nelements; i++) {
            fprintf (stderr, "  Result: %"PRIx16"\n", ((uint16_t*)(ad->data))[i]);
            fprintf (stderr, "Expected: %"PRIx16"\n", ((uint16_t*)(ans))[i]);

            if (((uint16_t*)(ans))[i] != ((uint16_t*)(ad->data))[i] )
                exit (EXIT_FAILURE);
        }
        break;
    case ATOM_DATA_UINT32:
        for (i=0; i<ad->nelements; i++) {
            fprintf (stderr, "  Result: %"PRIx32"\n", ((uint32_t*)(ad->data))[i]);
            fprintf (stderr, "Expected: %"PRIx32"\n", ((uint32_t*)(ans))[i]);

            if (((uint32_t*)(ans))[i] != ((uint32_t*)(ad->data))[i] )
                exit (EXIT_FAILURE);
        }
        break;
    case ATOM_DATA_UINT64:
        for (i=0; i<ad->nelements; i++) {
            fprintf (stderr, "  Result: %"PRIx64"\n", ((uint64_t*)(ad->data))[i]);
            fprintf (stderr, "Expected: %"PRIx64"\n", ((uint64_t*)(ans))[i]);

            if (((uint64_t*)(ans))[i] != ((uint64_t*)(ad->data))[i] )
                exit (EXIT_FAILURE);
        }
        break;
    case ATOM_DATA_STRING:
        for (i=0; i<ad->nelements; i++) {
            fprintf (stderr, "  Result: '%s'\n", ((char**)(ad->data))[i]);
            fprintf (stderr, "Expected: '%s'\n", ((char**)(ans))[i]);

            if (strcmp (((char**)(ans))[i], ((char**)(ad->data))[i]) )
                exit (EXIT_FAILURE);
        }
        break;
    default:
        exit (EXIT_FAILURE);
    }
}

static inline Atom* _find_atom (AtomTree* at, char* name)
{
    Atom* atom;

    fprintf (stderr, "Searching for atom '%s': ", name);
    if ( !(atom = atom_tree_find (at, name)) ) {
        fprintf (stderr, "NOT FOUND!\n");
        exit (EXIT_FAILURE);
    } else
        fprintf (stderr, "OK!\n");

    return atom;
}


static inline void _check_size (Atom* atom, size_t size)
{
    fprintf (stderr, "Checking size: ");
    if (atom->size != size) {
        fprintf (stderr, "BAD!\n");
        exit (EXIT_FAILURE);
    } else
        fprintf (stderr, "OK!\n");
}


static inline void _check_offset (Atom* atom, long offset)
{
    fprintf (stderr, "Checking offset: ");
    if (atom->offset != offset) {
        fprintf (stderr, "BAD!\n");
        exit (EXIT_FAILURE);
    } else
        fprintf (stderr, "OK!\n");
}


static inline void _check_fields (Atom* atom, List* parse, char* const * fields)
{
    int j;

    fprintf (stderr, "Parse: checking for fields\n");

    for (j=0; fields[j]; j++) {
        fprintf (stderr, "  Looking for '%s': ", fields[j]);

        if (!_get_atomdata (parse, fields[j])) {
            fprintf (stderr, "MISSING!\n");
            exit (EXIT_FAILURE);
        } else {
            fprintf (stderr, "FOUND\n");
        }
    }
}

#endif /* _VFTK_INTERNAL_TEST_ATOM_ */
