/*
MIT License

Copyright (c) 2019 James A. Shackleford

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software. */

#ifndef _VFTK_ARGPARSE_H_
#define _VFTK_ARGPARSE_H_

#include <stdint.h>
#include "list.h"

#define ARG_FLAG     0x00
#define ARG_INT      0x01
#define ARG_FLOAT    0x02
#define ARG_STR      0x03
#define ARG_REQUIRED 0x10


typedef struct {
    int argc;
    char** argv;
    List head;
} ArgParse;


ArgParse* argparse_new (int argc, char* argv[]);
void argparse_destroy (ArgParse** ap);
void argparse_print (ArgParse* ap);
int argparse (ArgParse* ap);
int argparse_check_required (ArgParse* ap);

void argparse_add (
    ArgParse* ap,
    uint8_t flags,
    const char* argshort,
    const char* arglong,
    const char* help,
    void* storage);

#endif  /* _VFTK_ARGPARSE_H_ */
