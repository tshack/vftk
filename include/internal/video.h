/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#ifndef _VIDEO_PRIV_H_
#define _VIDEO_PRIV_H_

#include "libavformat/avformat.h"
#include "libavcodec/h264dec.h"
#include "libavcodec/h264_ps.h"
#include "list.h"

typedef struct Video Video;
struct Video {
    char* filename;

    AVFormatContext* container;
    AVCodecContext* decoder;
    AVStream* stream;
    int stream_idx;
    int64_t npackets;
    int is_interlaced;

    AVPacket pkt;
    AVFrame* avframe;

    H264Context* h;

    List preprocs;
};


#endif  /* _VIDEO_PRIV_H_ */
