/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#ifndef _UTIL_H_
#define _UTIL_H_

#include <math.h>

#include "frame.h"

static inline unsigned int _num_digits (unsigned int n)
{
    return n > 0 ? (unsigned int) log10 ((double)n) + 1 : 1;
}


static inline int util_is_little_endian (void)
{
    int val = 1;
    char* test = (char*)&val;

    return *test;
}


static inline void util_swap_endian (void* val_ptr, size_t nbytes)
{
    char* p;
    char tmp;
    size_t lb;
    size_t hb;

    if (nbytes < 2)
        return;

    p = val_ptr;

    for (lb=0, hb=nbytes-1; lb<hb; lb++, hb--) {
        tmp = p[lb];
        p[lb] = p[hb];
        p[hb] = tmp;
    }
}


static inline const char* const _frametype2str (FrameType ftype)
{
    if (I_FRAME == ftype)
        return "I";
    else if (P_FRAME == ftype)
        return "P";
    else if (B_FRAME == ftype)
        return "B";
    else
        return "?";
}


static inline const char* const _ecoding2str (int is_cabac)
{
    switch (is_cabac) {
    case 1:
        return "CABAC";
    case 0:
        return "CAVLC";
    default:
        return "Not AVC";
    }
}


static inline const char* const util_profile2str (int profile)
{
    switch (profile) {
    case 44:
        return "(44) CAVLC 4:4:4 Intra Profile";
    case 66:
        return "(BP) Baseline Profile";
    case 77:
        return "(MP) Main Profile";
    case 88:
        return "(XP) Extended Profile";
    case 100:
        return "(HiP) High Profile";
    case 110:
        return "(Hi10P) High 10 Profile";
    case 118:
        return "(HiMulti) Multiview High Profile";
    case 122:
        return "(Hi422P) High 4:2:2 Profile";
    case 128:
        return "(HiStereo) Stereo High Profile";
    case 144:
        return "(Hi444) High 4:4:4 Profile";
    case 244:
        return "(Hi444PP) High 4:4:4 Predictive Profile";
    case 578:
        return "(CBP) Constrained Baseline Profile";
    case 2158:
        return "(Hi10PIntra) High 10 Intra Profile";
    case 2292:
        return "(Hi444Intra) High 4:4:4 Intra Profile";
    default:
        return "Unknown";
    }
}


#ifndef NAN
#include <stdint.h>

static union {
    uint32_t _i;
    float _d;
} _vftk_nan_union = {0x7fc00000};

#define NAN (_vftk_nan_union._d)

#endif


#endif /* _UTIL_H_ */
