/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include "internal/preproc.h"

#include "error.h"
#include "frame.h"
#include "video.h"

typedef struct PreProcData {
    int gop_n;
    int gop_m;

    int gop_num;
    int m;
    int n;
    int n1;
    int has_failed;
} PreProcData;

#define OFFSET(attr) offsetof(PreProcData, attr)

static const PreProcExport exports[] = {
    {.name = "gop_n", .type = INT, .data = OFFSET(gop_n)},
    {.name = "gop_m", .type = INT, .data = OFFSET(gop_m)},
    { NULL }
};


static ErrorCode init (Video* V, PreProcData* D)
{
    D->gop_num = D->m = D->n = D->n1 = 1;
    D->has_failed = 0;

    D->gop_n = 0;
    D->gop_m = 0;

    return SUCCESS;
}


static void perframe (Video* V, PreProcData* D, int64_t framenumber)
{
    if (D->has_failed)
        return;

    if (framenumber == 0) {
        if (video_peek_frametype(V) != I_FRAME)
            goto failure;

        return;
    }

    if (D->gop_num >= video_peek_nframes(V) / D->n1)
        return;


    switch (video_peek_frametype(V)) {
    case I_FRAME:
        if (D->gop_n == 0)
            D->gop_n = D->n1 = D->n;
        else if (D->gop_n != D->n)
            goto failure;

        D->n = 0;
        D->gop_num++;

        /* fallthrough */
    case P_FRAME:
        if (D->gop_m < D->m)
            D->gop_m = D->m;

        D->m = 0;

    default:
        break;
    }

    D->n++;
    D->m++;

    return;

failure:
    D->has_failed = 1;
    D->gop_m = 0;
    D->gop_n = 0;
}


static void fini (Video* V, PreProcData* D)
{
    return;
}


NEW_PREPROC(gopnm) = {
    .name = "GOP N,M",
    .init = init,
    .perframe = perframe,
    .fini = fini,
    .exports = exports,
    .size = sizeof(PreProcData)
};
