#include <stdlib.h>
#include <inttypes.h>
#include <string.h>

#include "internal/util.h"

#include "atom.h"
#include "list.h"

#define to_atomdata(ptr) container_of(ptr, AtomData, node)

typedef void (*parser_t)(List* parse, AtomTree* tree, Atom* atom);

typedef struct {
    const char* type;
    parser_t parser;
} AtomParser;


static void _to_system_endian (void* val_ptr, size_t nbytes)
{
    if (util_is_little_endian())
        util_swap_endian (val_ptr, nbytes);
}


static size_t _size_from_type (AtomDataType type)
{
    switch (type) {
    case ATOM_DATA_INT8:
    case ATOM_DATA_UINT8:
        return 1;
    case ATOM_DATA_INT16:
    case ATOM_DATA_UINT16:
        return 2;
    case ATOM_DATA_INT32:
    case ATOM_DATA_UINT32:
        return 4;
    case ATOM_DATA_INT64:
    case ATOM_DATA_UINT64:
        return 8;
    case ATOM_DATA_STRING:
        return sizeof(char*);
    default:
        return 0;
    }
}


static char** _string_array_new (void)
{
    char** array = malloc(sizeof(*array));
    *array = NULL;

    return array;
}


static void _string_array_destroy (char*** array_ptr)
{
    size_t i = 0;

    while ((*array_ptr)[i])
        free ((*array_ptr)[i++]);

    free (*array_ptr);
    *array_ptr = NULL;
}


static void _string_array_append (char*** array_ptr, char* string)
{
    size_t i;

    for (i=0; (*array_ptr)[i]; i++);

    *array_ptr = realloc (*array_ptr, sizeof(**array_ptr)*(i+2));
    (*array_ptr)[i] = string;
    (*array_ptr)[i+1] = NULL;
}


static size_t _string_array_nelements (char** array)
{
    size_t i;

    for (i=0; array[i]; i++);

    return i;
}


static void _atom_data_add (
    List* parse,
    char* name,
    AtomDataType type,
    size_t nelements,
    void* data)
{
    if (!data)
        return;

    AtomData* ad = malloc (sizeof(*ad));

    ad->name = name;
    ad->type = type;
    ad->nelements = nelements;
    ad->data = data;

    list_add_before (parse, &ad->node);
}


static void* _parse_strings (
    List* parse,
    AtomTree* tree,
    Atom* atom,
    char* name,
    size_t string_len,
    size_t nstrings)
{
    int i;
    char* str;
    char byte;
    long save;

    if (string_len < 0)
        return NULL;

    char** string_array = _string_array_new ();

    if (string_len > 0) {
        for (i=0; i<nstrings; i++) {
            str = calloc (string_len + 1, 1);
            if (string_len != fread (str, 1, string_len, tree->fp))
                goto out2;

            _string_array_append (&string_array, str);
        }
    } else {
        for (i=0; i<nstrings; i++) {
            save = ftell (tree->fp);
            do {
                if (!fread (&byte, 1, 1, tree->fp))
                    goto out;

                string_len++;
            } while (byte != '\0');

            fseek (tree->fp, save, SEEK_SET);

            str = malloc (string_len);

            if (string_len != fread (str, 1, string_len, tree->fp))
                goto out2;

            _string_array_append (&string_array, str);
        }
    }


    _atom_data_add (parse, name, ATOM_DATA_STRING,
            _string_array_nelements(string_array), string_array);

    return string_array;

out2:
    free (str);
out:
    _string_array_destroy (&string_array);
    return NULL;
}


static void* _parse_integers (
    List* parse,
    AtomTree* tree,
    Atom* atom,
    char* name,
    AtomDataType type,
    size_t nelements)
{
    void* data;
    size_t element;
    size_t element_size = _size_from_type (type);

    data = malloc (element_size * nelements);

    if (nelements != fread (data, element_size, nelements, tree->fp)) {
        free (data);
        return NULL;
    }

    for (element=0; element < nelements; element++)
        _to_system_endian (data + element * element_size, element_size);

    _atom_data_add (parse, name, type, nelements, data);

    return data;
}



static void _ftyp (List* parse, AtomTree* tree, Atom* atom)
{
    long save = ftell (tree->fp);
    size_t end = atom->offset + atom->size;

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_strings (parse, tree, atom, "major_brand", 4, 1);

    _parse_integers (parse, tree, atom, "minor_version", ATOM_DATA_UINT32, 1);

    _parse_strings (parse, tree, atom, "compatible_brands[]", 4,
            (end - ftell(tree->fp))/4);

    fseek (tree->fp, save, SEEK_SET);
}


static void _mvhd (List* parse, AtomTree* tree, Atom* atom)
{
    long save = ftell (tree->fp);
    uint32_t* version_flags;
    uint8_t version;

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    version_flags = _parse_integers (parse, tree, atom, "version+flags",
            ATOM_DATA_UINT32, 1);

    if (!version_flags)
        goto out;
    else
        version = (*version_flags >> 24) & 0x000000ff;

    if (version == 1) {
        _parse_integers (parse, tree, atom, "creation_time",
                ATOM_DATA_UINT64, 1);

        _parse_integers (parse, tree, atom, "modification_time",
                ATOM_DATA_UINT64, 1);

        _parse_integers (parse, tree, atom, "timescale",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "duration",
                ATOM_DATA_UINT64, 1);
    } else {
        _parse_integers (parse, tree, atom, "creation_time",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "modification_time",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "timescale",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "duration",
                ATOM_DATA_UINT32, 1);
    }

    /* v fixed point 16.16 */
    _parse_integers (parse, tree, atom, "rate", ATOM_DATA_UINT32, 1);
    /* v fixed point 8.8 */
    _parse_integers (parse, tree, atom, "volume", ATOM_DATA_UINT16, 1);
    _parse_integers (parse, tree, atom, "reserved", ATOM_DATA_UINT16, 1);
    _parse_integers (parse, tree, atom, "reserved[]", ATOM_DATA_UINT32, 2);
    _parse_integers (parse, tree, atom, "matrix[]", ATOM_DATA_INT32, 9);
    _parse_integers (parse, tree, atom, "pre_defined[]", ATOM_DATA_UINT32, 6);
    _parse_integers (parse, tree, atom, "next_track_ID", ATOM_DATA_UINT32, 1);

out:
    fseek (tree->fp, save, SEEK_SET);
}


static void _tkhd (List* parse, AtomTree* tree, Atom* atom)
{
    long save = ftell (tree->fp);
    uint32_t* version_flags;
    uint8_t version;

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    version_flags = _parse_integers (parse, tree, atom, "version+flags",
            ATOM_DATA_UINT32, 1);

    if (!version_flags)
        goto out;
    else
        version = (*version_flags >> 24) & 0x000000ff;

    if (version == 1) {
        _parse_integers (parse, tree, atom, "creation_time",
                ATOM_DATA_UINT64, 1);

        _parse_integers (parse, tree, atom, "modification_time",
                ATOM_DATA_UINT64, 1);

        _parse_integers (parse, tree, atom, "track_ID",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "reserved",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "duration",
                ATOM_DATA_UINT64, 1);
    } else {
        _parse_integers (parse, tree, atom, "creation_time",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "modification_time",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "track_ID",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "reserved",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "duration",
                ATOM_DATA_UINT32, 1);
    }

    _parse_integers (parse, tree, atom, "reserved[]", ATOM_DATA_UINT32, 2);
    _parse_integers (parse, tree, atom, "layer", ATOM_DATA_INT16, 1);
    _parse_integers (parse, tree, atom, "alternate_group", ATOM_DATA_INT16, 1);
    /* v fixed point 8.8 */
    _parse_integers (parse, tree, atom, "volume", ATOM_DATA_INT16, 1);
    _parse_integers (parse, tree, atom, "reserved", ATOM_DATA_UINT16, 1);
    _parse_integers (parse, tree, atom, "matrix[]", ATOM_DATA_INT32, 9);
    /* v fixed point 16.16 */
    _parse_integers (parse, tree, atom, "width", ATOM_DATA_UINT32, 1);
    /* v fixed point 16.16 */
    _parse_integers (parse, tree, atom, "height", ATOM_DATA_UINT32, 1);

out:
    fseek (tree->fp, save, SEEK_SET);
}


static void _mdhd (List* parse, AtomTree* tree, Atom* atom)
{
    long save = ftell (tree->fp);
    uint32_t* version_flags;
    uint8_t version;

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    version_flags = _parse_integers (parse, tree, atom, "version+flags",
            ATOM_DATA_UINT32, 1);

    if (!version_flags)
        goto out;
    else
        version = (*version_flags >> 24) & 0x000000ff;

    if (version == 1) {
        _parse_integers (parse, tree, atom, "creation_time",
                ATOM_DATA_UINT64, 1);

        _parse_integers (parse, tree, atom, "modification_time",
                ATOM_DATA_UINT64, 1);

        _parse_integers (parse, tree, atom, "timescale",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "duration",
                ATOM_DATA_UINT64, 1);
    } else {
        _parse_integers (parse, tree, atom, "creation_time",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "modification_time",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "timescale",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "duration",
                ATOM_DATA_UINT32, 1);
    }

    /* (ISO-639-2/T) actually 1 pad bit + three 5-bit uints... */
    _parse_integers (parse, tree, atom, "language", ATOM_DATA_UINT16, 1);
    _parse_integers (parse, tree, atom, "pre_defined", ATOM_DATA_UINT16, 1);

out:
    fseek (tree->fp, save, SEEK_SET);
}


static void _vmhd (List* parse, AtomTree* tree, Atom* atom)
{
    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);
    _parse_integers (parse, tree, atom, "graphicsmode", ATOM_DATA_UINT16, 1);
    _parse_integers (parse, tree, atom, "opcolor", ATOM_DATA_UINT16, 3);

    fseek (tree->fp, save, SEEK_SET);
}


static void _elst (List* parse, AtomTree* tree, Atom* atom)
{
    long save = ftell (tree->fp);
    uint32_t* version_flags;
    uint8_t version;
    uint32_t* entry_count;
    uint32_t i;

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    version_flags = _parse_integers (parse, tree, atom, "version+flags",
            ATOM_DATA_UINT32, 1);

    if (!version_flags)
        goto out;
    else
        version = (*version_flags >> 24) & 0x000000ff;

    entry_count = _parse_integers (parse, tree, atom, "entry_count",
            ATOM_DATA_UINT32, 1);

    for (i=0; i<*entry_count; i++) {
        if (version == 1) {
            _parse_integers (parse, tree, atom, "segment_duration",
                    ATOM_DATA_UINT64, 1);

            _parse_integers (parse, tree, atom, "media_time",
                    ATOM_DATA_INT64, 1);
        } else {
            _parse_integers (parse, tree, atom, "segment_duration",
                    ATOM_DATA_UINT32, 1);

            _parse_integers (parse, tree, atom, "media_time",
                    ATOM_DATA_INT32, 1);
        }
    }

    _parse_integers (parse, tree, atom, "media_rate_integer",
            ATOM_DATA_INT16, 1);

    _parse_integers (parse, tree, atom, "media_rate_fraction",
            ATOM_DATA_INT16, 1);

out:
    fseek (tree->fp, save, SEEK_SET);
}


static void _hdlr (List* parse, AtomTree* tree, Atom* atom)
{
    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);
    _parse_integers (parse, tree, atom, "pre_defined", ATOM_DATA_UINT32, 1);
    _parse_integers (parse, tree, atom, "handler_type", ATOM_DATA_UINT32, 1);
    _parse_integers (parse, tree, atom, "reserved[]", ATOM_DATA_UINT32, 3);
    _parse_strings (parse, tree, atom, "name", 0, 1);

    fseek (tree->fp, save, SEEK_SET);
}


static void _stts (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t i;
    uint32_t* entry_count;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);

    entry_count = _parse_integers (parse, tree, atom, "entry_count",
            ATOM_DATA_UINT32, 1);

    for (i=0; i<*entry_count; i++) {
        _parse_integers (parse, tree, atom, "sample_count", ATOM_DATA_UINT32, 1);
        _parse_integers (parse, tree, atom, "sample_delta", ATOM_DATA_UINT32, 1);
    }

    fseek (tree->fp, save, SEEK_SET);
}


static void _ctts (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t i;
    uint32_t* entry_count;
    uint32_t* version_flags;
    uint8_t version;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    version_flags = _parse_integers (parse, tree, atom, "version+flags",
            ATOM_DATA_UINT32, 1);

    if (!version_flags)
        goto out;
    else
        version = (*version_flags >> 24) & 0x000000ff;

    entry_count = _parse_integers (parse, tree, atom, "entry_count",
            ATOM_DATA_UINT32, 1);

    if (version == 0) {
        for (i=0; i<*entry_count; i++) {
            _parse_integers (parse, tree, atom, "sample_count",
                    ATOM_DATA_UINT32, 1);

            _parse_integers (parse, tree, atom, "sample_offset",
                    ATOM_DATA_UINT32, 1);
        }
    } else if (version == 1) {
        for (i=0; i<*entry_count; i++) {
            _parse_integers (parse, tree, atom, "sample_count",
                    ATOM_DATA_UINT32, 1);

            _parse_integers (parse, tree, atom, "sample_offset",
                    ATOM_DATA_INT32, 1);
        }
    }

out:
    fseek (tree->fp, save, SEEK_SET);
}


static void _cslg (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t* version_flags;
    uint8_t version;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    version_flags = _parse_integers (parse, tree, atom, "version+flags",
            ATOM_DATA_UINT32, 1);

    if (!version_flags)
        goto out;
    else
        version = (*version_flags >> 24) & 0x000000ff;

    if (version == 0) {
        _parse_integers (parse, tree, atom, "compositionToDTShift",
                ATOM_DATA_INT32, 1);

        _parse_integers (parse, tree, atom, "leastDecodeToDisplayDelta",
                ATOM_DATA_INT32, 1);

        _parse_integers (parse, tree, atom, "greatestDecodeToDisplayDelta",
                ATOM_DATA_INT32, 1);

        _parse_integers (parse, tree, atom, "compositionStartTime",
                ATOM_DATA_INT32, 1);

        _parse_integers (parse, tree, atom, "compositionEndTime",
                ATOM_DATA_INT32, 1);
    } else {
        _parse_integers (parse, tree, atom, "compositionToDTShift",
                ATOM_DATA_INT64, 1);

        _parse_integers (parse, tree, atom, "leastDecodeToDisplayDelta",
                ATOM_DATA_INT64, 1);

        _parse_integers (parse, tree, atom, "greatestDecodeToDisplayDelta",
                ATOM_DATA_INT64, 1);

        _parse_integers (parse, tree, atom, "compositionStartTime",
                ATOM_DATA_INT64, 1);

        _parse_integers (parse, tree, atom, "compositionEndTime",
                ATOM_DATA_INT64, 1);
    }

out:
    fseek (tree->fp, save, SEEK_SET);
}


static void _stss (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t i;
    uint32_t* entry_count;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);

    entry_count = _parse_integers (parse, tree, atom, "entry_count",
            ATOM_DATA_UINT32, 1);

    for (i=0; i<*entry_count; i++)
        _parse_integers (parse, tree, atom, "sample_number",
                ATOM_DATA_UINT32, 1);

    fseek (tree->fp, save, SEEK_SET);
}


static void _stsh (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t i;
    uint32_t* entry_count;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);

    entry_count = _parse_integers (parse, tree, atom, "entry_count",
            ATOM_DATA_UINT32, 1);

    for (i=0; i<*entry_count; i++) {
        _parse_integers (parse, tree, atom, "shadowed_sample_number",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "sync_sample_number",
                ATOM_DATA_UINT32, 1);
    }

    fseek (tree->fp, save, SEEK_SET);
}


static void _stsz (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t i;
    uint32_t* sample_size;
    uint32_t* sample_count;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);

    sample_size = _parse_integers (parse, tree, atom, "sample_size",
            ATOM_DATA_UINT32, 1);

    sample_count = _parse_integers (parse, tree, atom, "sample_count",
            ATOM_DATA_UINT32, 1);

    if (*sample_size != 0)
        goto out;

    for (i=0; i<*sample_count; i++)
        _parse_integers (parse, tree, atom, "entry_size",
                ATOM_DATA_UINT32, 1);

out:
    fseek (tree->fp, save, SEEK_SET);
}


static void _stsc (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t i;
    uint32_t* entry_count;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);

    entry_count = _parse_integers (parse, tree, atom, "entry_count",
            ATOM_DATA_UINT32, 1);

    for (i=0; i<*entry_count; i++) {
        _parse_integers (parse, tree, atom, "first_chunk",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "samples_per_chunk",
                ATOM_DATA_UINT32, 1);

        _parse_integers (parse, tree, atom, "sample_description_index",
                ATOM_DATA_UINT32, 1);
    }

    fseek (tree->fp, save, SEEK_SET);
}


static void _stco (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t i;
    uint32_t* entry_count;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);

    entry_count = _parse_integers (parse, tree, atom, "entry_count",
            ATOM_DATA_UINT32, 1);

    for (i=0; i<*entry_count; i++)
        _parse_integers (parse, tree, atom, "chunk_offset",
                ATOM_DATA_UINT32, 1);

    fseek (tree->fp, save, SEEK_SET);
}


static void _co64 (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t i;
    uint32_t* entry_count;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);

    entry_count = _parse_integers (parse, tree, atom, "entry_count",
            ATOM_DATA_UINT32, 1);

    for (i=0; i<*entry_count; i++)
        _parse_integers (parse, tree, atom, "chunk_offset",
                ATOM_DATA_UINT64, 1);

    fseek (tree->fp, save, SEEK_SET);
}


static void _smhd (List* parse, AtomTree* tree, Atom* atom)
{
    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);
    _parse_integers (parse, tree, atom, "balance", ATOM_DATA_INT16, 1);
    _parse_integers (parse, tree, atom, "reserved", ATOM_DATA_UINT16, 1);


    fseek (tree->fp, save, SEEK_SET);
}


static void _dref (List* parse, AtomTree* tree, Atom* atom)
{
    uint32_t i;
    uint32_t* entry_count;
    char** type;

    long save = ftell (tree->fp);

    fseek (tree->fp, atom->offset + 8, SEEK_SET);

    _parse_integers (parse, tree, atom, "version+flags", ATOM_DATA_UINT32, 1);

    entry_count = _parse_integers (parse, tree, atom, "entry_count",
            ATOM_DATA_UINT32, 1);

    /* entries are FullBoxes */
    for (i=0; i<*entry_count; i++) {
        _parse_integers (parse, tree, atom, "size", ATOM_DATA_UINT32, 1);
        type = _parse_strings (parse, tree, atom, "type", 4, 1);
        _parse_integers (parse, tree, atom, "version+flags",
                ATOM_DATA_UINT32, 1);

        if (!strncmp (type[0], "urn ", 4))
            _parse_strings (parse, tree, atom, "name", 0, 1);

        _parse_strings (parse, tree, atom, "location", 0, 1);
    }

    fseek (tree->fp, save, SEEK_SET);
}



#define PARSER(name) { #name, _##name }

static AtomParser parsers[] = {
    PARSER (ftyp),
    PARSER (mvhd),
    PARSER (tkhd),
    PARSER (mdhd),
    PARSER (vmhd),
    PARSER (elst),
    PARSER (hdlr),
    PARSER (stts),
    PARSER (ctts),  /* needs test */
    PARSER (cslg),  /* needs test */
    PARSER (stss),
    PARSER (stsh),  /* needs test */
    PARSER (stsz),  /* needs test */
    PARSER (stsc),
    PARSER (stco),
    PARSER (co64),  /* needs test */
    PARSER (smhd),  /* needs test */
    PARSER (dref),
    { NULL }
};


List* atom_parse (AtomTree* tree, Atom* atom)
{
    int i;
    List* parse;

    for (i=0; parsers[i].type; i++)
        if (!strcmp (parsers[i].type, atom->type)) {
            parse = malloc (sizeof(*parse));
            list_init (parse);
            parsers[i].parser (parse, tree, atom);
            return parse;
        }

    return NULL;
}


void atom_parse_destroy (List** parse)
{
    List *cur, *sav;
    AtomData* ad;
    int i;

    list_foreach_safe (*parse, cur, sav) {
        ad = to_atomdata (cur);
        list_remove (cur);

        if (ad->type == ATOM_DATA_STRING)
            for (i=0; i<ad->nelements; i++)
                free(((void**)(ad->data))[i]);

        free(ad->data);
        free(ad);
    }

    free (*parse);
    *parse = NULL;
}
