# VFTK -- The Video Forensics Toolkit

VFTK is a toolkit designed to extract useful, yet generally hard to
obtain, information from digitally encoded video that is useful in
performing low-level forensic analysis and general digital video
research.  For example, you can use VFTK to obtain frame types (I, P,
B), frame decode and presentation times, frame dumps, video container
atom tree topology, raw atom dumps, parsed atom contents, macroblock
sizes/locations, motion vectors, and H.264 prediction errors.

The toolkit consists of three interfaces:

- `vftk` -- A pipelinable command line utility for extracting video
  metrics and information.

- `libvftk` -- A C library (with accompanying headers) for developing
  custom video forensic algorithms and applications in C.

- The `vftk` Python module -- A module written in Cython, which provides
  native, high performance extension types that wrap and expose the
  functionality of `libvftk` to Python.  Provides the means to rapidly
  develop custom video forensic algorithms and applications in Python.


## Build Dependencies

Primary VFTK development is conducted on Arch Linux, meaning that
development builds are conducted with tools and libraries that are
generally on the latest upstream stable release.  If you encounter build
issues using (reasonably) outdated tools/libraries, please submit a bug
report.

- **CMake (>=2.8.5)** -- VFTK uses Kitware's cross-platform build system
  generator, CMake.  For the uninitiated, CMake will generate Makefiles,
  Visual Studio Solutions, Xcode projects, etc depending on the user's
  platform and preference.  You'll like it.

- **libtiff (>=4.0)** -- Frame dumps, prediction error visualizations,
  etc are dumped to disk in Tagged Image File Format (TIFF) via
  `libtiff`.

In order to provide functionality otherwise unavailable, VFTK will
clone, patch, and link against its own FFmpeg 4.0 build automatically
when built.  This requires that you have the required FFmpeg library
dependencies as well.  The following should be sufficient:

- **libx11** -- *Arch:* `libx11`, *Ubuntu:* `libx11-dev`
- **libvdpau** -- *Arch:* `libvdpau`, *Ubuntu:* `libvdpau-dev`
- **libva** -- *Arch:* `libva`, *Ubuntu:* `libva-dev`


## Building the Software

Once you have prepared your system by installing all of the requisite
build dependencies, building is simple and straightforward.

1. Simply checkout the software:
```
$ git clone git@gitlab.com:tshack/vftk.git
```

2. Create a directory that will hold the build system.
```
$ cd vftk
$ mkdir build/
```

3. Use CMake to generate the build system. You have two options here:

    - If you only want the `vftk` binary and the C library/headers:
    ```
    $ cd build/
    $ cmake ..
    ```

    - If you also want the `vftk` Python module, instead run:
    ```
    $ cd build/
    $ cmake -DBUILD_CYTHON ..
    ```

    - *Note: Building the VFTK Python module requires that you have
      `Cython` and `Numpy >= 1.7` installed on your system*

    - For more information on the Python module, please see the
      [Cython README](cython).

4. Use the generated build system to build VFTK:
```
$ make
```

5. *(Optional)* Run the self-tests to ensure the build is properly
   functioning:
```
$ make test
```

6. Once built, you may install VFTK system wide via:
```
# make install
```

7. Later, if you want to uninstall VFTK, you may run the following from
   within the VFTK build directory:
```
# make uninstall
```


## Usage

The `vftk` command line application provides several modes of operation.
Passing no arguments to `vftk` will result in all available modes being
displayed:

```
$ vftk
Usage: vftk MODE [--help]

  Modes:
    info ....... General Video Information
    ftypes ..... List Frame Types
    frames ..... Frame Extraction
    tsdump ..... Frame Decode/Presentation Timstamps
    atom ....... Explore container atoms
    mv ......... Motion Vector Extraction
    perror ..... Frame Prediction Errors
```

Each mode has its own set of options that will further specify behavior.
Let's look at a few examples involving each mode!


### Mode `info`: General Video Information

```
$ vftk info --help
Usage: vftk info [OPTIONS...] INPUT

Options:
  -h, --help                display this help text and exit
  -f, --fast                don't display info that requires scanning the
                            entire video stream
```

This mode displays general information for the supplied `INPUT` video.

For example:
```
$ vftk info foo.mp4
File: foo.mp4
========================================================
Display Dimensions: 1920 x 1080
Profile           : (HiP) High Profile
Level             : 4.1
Bitrate           : 21394665 b/s
Total Frames      : 169
Frame Rate        : Variable
GOP Structure     : Fixed Length (N=29, M=2)
                    IBPBPBPBPBPBPBPBPBPBPBPBPBPBPIBPBPBPBPBPBPBPBPBPBPBPBP...

```

As noted in this mode's help, supplying the option `--fast` will produce
the output more quickly but will not display information that requires a
full scan of the video stream (e.g. GOP structure).


### Mode `ftypes`: List Frame Type

```
$ vftk ftypes --help
Usage: vftk ftypes [OPTIONS...] INPUT

Options:
  -h, --help                display this help text and exit
```

This mode provides the frame type, frame number, and presentation time
(in seconds) for each and every frame in the input video specified by
`INPUT`.  This mode is useful for inspecting video GOP structure,
finding the presentation time of a specific P-frame, etc.

```
$ vftk ftypes foo.mp4
Frame_Type, Frame_Number, Presentation_Time_(s)
I, 0, 0.000
P, 1, 0.033
B, 2, 0.067
B, 3, 0.100
B, 4, 0.133
P, 5, 0.167
P, 6, 0.200
P, 7, 0.234
B, 8, 0.267
B, 9, 0.300
I, 10, 0.334
P, 11, 0.367
B, 12, 0.400
...
```


### Mode `frames`: Extract Frames and Write to Disk

```
$ vftk frames --help
Usage: vftk frames [OPTIONS...] INPUT

Options:
  -h, --help                display this help text and exit
  -i, --iframes             write inter frames (I-frames) to disk
  -p, --pframes             write predicted frames (P-frames) to disk
  -b, --bframes             write bidirectionally predicted frames (B-frames)
                            to disk
  -P, --prefix=PREFIX       write frames to disk using file names starting
                            with PREFIX

Selected frames will be written to disk in Tagged Image File Format (TIFF).
```

This mode allows for frames of the specified types to be dumped to disk.
Frames are individually written to disk in TIFF format with filenames
taking the format of a prefix followed by the frame number of the dumped
frame.  The required argument `--prefix` specifies the filename prefix.
The flags `--iframes`, `--pframes`, and `--bframes` determine what type
of frames will be written to disk.  For example, to dump all I-frames
and P-frames:

```
$ vftk frames -ip --prefix=frames- input.mp4
Writing frames to disk in TIFF format with prefix 'frame-'... done.
```


### Mode `tsdump`: Frame Decode/Presentation Schedule

```
$ vftk tsdump --help
Usage: vftk tsdump [OPTIONS...] INPUT

Options:
  -h, --help                display this help text and exit
```

This mode will generate a CSV formatted schedule of frame decode and
presentation times, ordered by time stamp.

```
$ vftk tsdump opengop-video.mp4
Time Stamp, Decode Frame, Present Frame
     -2002,            0,
     -1001,            1,
         0,            5,             0
      1001,            3,             1
      2002,            2,             2
      3003,            4,             3
      4004,            6,             4
      5005,            7,             5
      6006,           10,             6
      7007,            8,             7
      8008,            9,             8
      9009,           11,             9
     10010,           14,            10
     11011,           12,            11
...
```


### Mode `atom`: Explore Container Atoms

```
$ vftk atom --help
Usage: vftk atom [OPTIONS...] INPUT

Options:
  -t, --tree                print MP4 container's atom tree to standard output
  -d, --dump=FQAL           print hex dump of atom content for atom at FQAL
  -p, --parse=FQAL          print parsed contents of atom at FQAL
  -h, --help                display this help text and exit

A Fully Qualified Atom Location (FQAL) is a unique identifer used to
specify a particular atom within the atom tree (e.g. ftype, moov.mvhd,
moov.trak[0].mdia.minf.dinf.dref, moov.trak[1].mdia.minf.stbl.stts)
```

This powerful mode allows you to perform low level analysis of a
container file.  Currently, only ISO base media file format (ISO/EIC
14496-12 -- MPEG-4 Part 12) style containers are supported.  This
includes containers such as `MP4`, `3GP`, `3G2`, `.mov`, `.mj2`, `.dvb`,
`.dcf`, `.m21`, and `.f4v`.

ISO base media style containers represent a tree of "atoms" (or "boxs").
Actual data is generally contained in leaf node atoms.  Non-leaf node
atoms are intended to be used purely for the purpose of organizing leaf
atoms.  To view the atom tree for a given MPEG-4 part 12 style file, use
the `atom` mode's `--tree` option:

```
$ vftk atom --tree foo.mp4
+-- ftyp [28 bytes] @ 0x0
+-- moov [179627 bytes] @ 0x1c
|   +-- mvhd [108 bytes] @ 0x24
|   +-- iods [21 bytes] @ 0x90
|   +-- trak [107889 bytes] @ 0xa5
|   |   +-- tkhd [92 bytes] @ 0xad
|   |   +-- mdia [107789 bytes] @ 0x109
|   |       +-- mdhd [32 bytes] @ 0x111
|   |       +-- hdlr [66 bytes] @ 0x131
|   |       +-- minf [107683 bytes] @ 0x173
|   |           +-- smhd [16 bytes] @ 0x17b
|   |           +-- dinf [36 bytes] @ 0x18b
|   |           |   +-- dref [28 bytes] @ 0x193
|   |           +-- stbl [107623 bytes] @ 0x1af
|   |               +-- stsd [91 bytes] @ 0x1b7
|   |               +-- stts [24 bytes] @ 0x212
|   |               +-- stsc [40 bytes] @ 0x22a
|   |               +-- stsz [102772 bytes] @ 0x252
|   |               +-- stco [4688 bytes] @ 0x193c6
|   +-- trak [70845 bytes] @ 0x1a616
|   |   +-- tkhd [92 bytes] @ 0x1a61e
|   |   +-- mdia [70745 bytes] @ 0x1a67a
|   |       +-- mdhd [32 bytes] @ 0x1a682
|   |       +-- hdlr [66 bytes] @ 0x1a6a2
|   |       +-- minf [70639 bytes] @ 0x1a6e4
|   |           +-- vmhd [20 bytes] @ 0x1a6ec
|   |           +-- dinf [36 bytes] @ 0x1a700
|   |           |   +-- dref [28 bytes] @ 0x1a708
|   |           +-- stbl [70575 bytes] @ 0x1a724
|   |               +-- stsd [171 bytes] @ 0x1a72c
|   |               +-- stts [24 bytes] @ 0x1a7d7
|   |               +-- stss [1080 bytes] @ 0x1a7ef
|   |               +-- stsc [7324 bytes] @ 0x1ac27
|   |               +-- stsz [57280 bytes] @ 0x1c8c3
|   |               +-- stco [4688 bytes] @ 0x2a883
|   +-- udta [756 bytes] @ 0x2bad3
|       +-- meta [748 bytes] @ 0x2badb
+-- mdat [157828719 bytes] @ 0x2bdc7
```

The four letter labels define a given atom's type, the values inside the
square brackets indicate the atom's size, and the displayed hexadecimal
values correspond to the atom's offset within the specified `INPUT`
file.

Having the tree, the contents of individual atoms can now be easily
viewed.  Please note, however, that since non-leaf atoms are used to
organize (i.e. contain) the atoms beneath them, viewing a non-leaf atom
will result in viewing the non-leaf atom and all of its child atoms
(i.e. the non-leaf atom's contents).

Given the above tree, we can view the contents of a given atom by
passing the desired atom's Fully Qualified Atom Location (FQAL) using
the `atom` mode's `--dump` option.  For example:

```
$ vftk atom --dump=moov.trak.mdia.minf.dinf.dref foo.mp4
28 byte hex dump of atom 'moov.trak.mdia.minf.dinf.dref':
00000190:        00 0000 1c64 7265 6600 0000 0000     ....dref.....
000001a0: 0000 0100 0000 0c75 726c 2000 0000 01    .......url ....
```

Notice, however, that there are two parallel `trak` atoms in this tree.
You may manually specify which `trak` atom to follow by using zero
indexed square bracket notation.  If no index is supplied, as in the
previous example, then index 0 is assumed (e.g. we could have
equivalently specified `moov.trak[0].mdia.minf.dinf.dref`).  Note that
an index may be optionally supplied for any given atom in the FQAL.

Further demonstrating the index notation, to view the 171 byte `stsd`
atom under the second `trak` atom, we would specify the FQAL as follows:

```
$ vftk atom --dump=moov.trak[1].mdia.minf.stbl.stsd foo.mp4
171 byte hex dump of atom 'moov.trak[1].mdia.minf.stbl.stsd':
0001a720:                               0000 00ab              ....
0001a730: 7374 7364 0000 0000 0000 0001 0000 009b  stsd............
0001a740: 6176 6331 0000 0000 0000 0001 0000 0000  avc1............
0001a750: 0000 0000 0000 0000 0000 0000 0500 02d0  ................
0001a760: 0048 0000 0048 0000 0000 0000 0001 0000  .H...H..........
0001a770: 0000 0000 0000 0000 0000 0000 0000 0000  ................
0001a780: 0000 0000 0000 0000 0000 0000 0000 0018  ................
0001a790: ffff 0000 0031 6176 6343 0164 001f ffe1  .....1avcC.d....
0001a7a0: 0019 6764 001f ac24 8401 4016 ec04 4000  ..gd...$..@...@.
0001a7b0: 0003 0040 0000 0c23 c60c 9201 0005 68ee  ...@...#......h.
0001a7c0: 32c8 b000 0000 1462 7472 7400 060d d500  2......btrt.....
0001a7d0: 51fb 7800 1e62 70                        Q.x..bp
```

**Note**: *To the best of my knowledge, Fully Qualified Atom Location
(FQAL) is not an official term.  I just came up with it because I don't
believe there is an official named means to specify a given atom in such
a fashion.*

In addition to being able to view raw hex dumps of atom contents, you
can alternatively tell `vftk` to parse the contents into the fields
specified by ISO/IEC 14496-12.  Simply use `--parse` instead of
`--dump` to view the parsed atom.

For example, below is the raw hex dump of atom `moov.mvhd`:

```
$ vftk atom --dump=moov.mvhd foo.mp4
108 byte hex dump of atom 'moov.mvhd':
00000020:           0000 006c 6d76 6864 0000 0000      ...lmvhd....
00000030: c76f 4052 c76f 4052 0000 0258 0005 75fc  .o@R.o@R...X..u.
00000040: 0001 0000 0100 0000 0000 0000 0000 0000  ................
00000050: 0001 0000 0000 0000 0000 0000 0000 0000  ................
00000060: 0001 0000 0000 0000 0000 0000 0000 0000  ................
00000070: 4000 0000 0000 0000 0000 0000 0000 0000  @...............
00000080: 0000 0000 0000 0000 0000 0000 0000 0003  ................
```

While viewing this raw atom contents may be preferable in some
situations, it is probably more useful to view this data once it has
been parsed by `vftk` with the `--parse` option:

```
$ vftk atom --parse=moov.mvhd foo.mp4
Parsed contents of atom 'moov.mvhd':
  version+flags: 0x00000000
  creation_time: 0xc76f4052
  modification_time: 0xc76f4052
  timescale: 0x00000258
  duration: 0x000575fc
  rate: 0x00010000
  volume: 0x0100
  reserved: 0x0000
  reserved[]: 0x00000000 0x00000000
  matrix[]: 0x00010000 0x00000000 0x00000000 0x00000000 0x00010000 0x00000000 0x00000000 0x00000000 0x40000000
  pre_defined[]: 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000 0x00000000
  next_track_ID: 0x00000003
```

Field names, such as `modification_time` match those given in the
ISO/IEC 14496-12 standard.  When interpreting these values, one should
reference the standard.  For example, `creation_time` and
`modification_time` are unsigned integers representing the number of
seconds that have passed since midnight, January 1st, 1904 (UTC).
Similarly nuanced, `volume` is a fixed point `8-bit.8-bit` number,
whereas `rate` is a fixed point `16-bit.16-bit` number.  In future
versions, `vftk` may polish the user interface to perform this
interpretation for you.  However, it is the goal of `vftk` to provide
raw data access to the user, so I will have to find a way to do this
without sacrificing transparency while minimizing clutter.



### Mode `mv`: Motion Vector Extraction

```
$ vftk mv --help
Usage: vftk mv [OPTIONS...] INPUT

Options:
  -f, --forward             write out forward motion vectors
  -b, --backward            write out backward motion vectors
  -h, --help                display this help text and exit
```

This mode dumps motion vector information for each macroblock within
each frame.  Information is written to standard output with one
macroblock/motion vector per line in the following format:

```
frame_num [direction]: macroblock_dims: (src_x, src_y) => (dst_x, dst_y)
```

The flag `--forward` causes forward predicted macroblocks to be
displayed, where as the `--backward` flag displays backward predicted
macroblocks.  For example, to show all backward predicted
macroblocks/motion vectors:

```
$ vftk mv --backward foo.mp4
2 [-1]: 16x16: (   8,    8) => (   8,    8)
2 [-1]: 16x16: (  24,    8) => (  24,    8)
2 [-1]: 16x16: (  40,    8) => (  40,    8)
2 [-1]: 16x16: (  56,    8) => (  56,    8)
2 [-1]: 16x16: (  72,    8) => (  72,    8)
2 [-1]: 16x16: (  88,    8) => (  88,    8)
2 [-1]: 16x16: ( 104,    8) => ( 104,    8)
2 [-1]: 16x16: ( 120,    8) => ( 120,    8)
2 [-1]: 16x16: ( 136,    8) => ( 136,    8)
...
```


### Mode `perror`: Macroblock Prediction Error

**Currently only available for H.264 video**

```
$ vftk perror --help
Usage: vftk perror [OPTIONS...] INPUT

Options:
  -V, --vis-prefix=PREFIX   write prediction error visualization frames to
                            disk using file names starting with PREFIX
  -h, --help                display this help text and exit
```


For each frame, this mode will compute the normalized sum of absolute
errors between macroblocks mapping from the current frame into other
frames (i.e. interframe mappings).  This is accomplished via a patch
that has been applied to FFMpeg's H.264 decoder, which directly extracts
the DCT coefficients encoding the prediction error.

Using `vftk`, obtaining the per frame prediction errors is simple:
```
$ vftk perror big_buck_bunny.mp4
0, 0.031111
1, 0.000000
2, 0.000000
3, 0.000000
4, 0.000234
5, 0.000464
6, 0.000799
7, 0.001109
8, 0.000577
9, 0.001139
10, 0.005720
11, 0.009842
12, 0.018132
13, 0.027678
14, 0.042114
15, 0.025482
...
```

Additionally, the `--vis-prefix` option may be used to dump a per-frame
visualization of the error.  This option requires a parameter, which
will be used as the prefix of the filename for each dumped error frame
-- the suffix being, of course, the corresponding frame number.  For
example:

```
$ vftk perror --vis-prefix=err input.mp4
```

will write the per-frame prediction errors to standard output and a
series of prediction error frame dumps will be written to disk with
filenames `err00002.tiff`, `err00003.tiff`, etc.  The following is an
animated GIF assembled using a small subset of prediction error
visualization frames from the H.264 encoded version of the notorious Big
Buck Bunny short:

![Prediction Error Visualization](misc/perror.gif)
