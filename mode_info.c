/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "internal/argparse.h"
#include "internal/util.h"

#include "error.h"
#include "video.h"

#define BUFFER_SIZE 1024

#define rnprint(len, str) \
for (int i=0; i<len; i++) printf(str)


typedef struct {
    int help;
    int fast;
} Opts;


static Opts opts = {
    .help = 0,
    .fast = 0
};


static void print_usage (ArgParse* P)
{
    printf ("Usage: vftk info [OPTIONS...] INPUT\n"
            "\n"
            "Options:\n");

    argparse_print (P);
}


static ArgParse* _parse (int argc, char* argv[])
{
    int ret;

    ArgParse* P = argparse_new (argc, argv);

    argparse_add (P, ARG_FLAG, "-h", "--help",
            "display this help text and exit", &opts.help);

    argparse_add (P, ARG_FLAG, "-f", "--fast",
            "don't display info that requires scanning the "
            "entire video stream",
            &opts.fast);

    if (!argparse (P)) {
        ret = EXIT_FAILURE;
        goto out;
    }

    if (opts.help) {
        print_usage (P);
        ret = EXIT_SUCCESS;
        goto out;
    }

    if (P->argc < 1) {
        fprintf (stderr, "Must specify INPUT\n");
        ret = EXIT_FAILURE;
        goto out;
    }

    return P;

out:
    argparse_destroy (&P);
    exit (ret);
}


void _dump_info (Video* V, ArgParse* P)
{
    int N, M, have_NM;

    int is_fast = opts.fast;

    printf ("File: %s\n", P->argv[0]);
    rnprint (strlen("File: ") + strlen(P->argv[0]), "=");
    printf ("\n");
    fflush (stdout);

    if (!is_fast)
        have_NM = video_peek_gop (V, &M, &N);

    printf ("Display Dimensions: %d x %d\n", video_peek_width (V), video_peek_height (V));
    printf ("Profile           : %s\n", util_profile2str (video_peek_profile (V)));
    printf ("Level             : %d.%d\n", video_peek_level (V)/10, video_peek_level (V)%10);
    printf ("Bitrate           : %"PRId64" b/s\n", video_peek_bitrate (V));
    printf ("Total Frames      : %"PRId64"\n", video_peek_nframes (V));
    printf ("Frame Rate        : %s\n", video_is_vfr (V) ? "Variable" : "Fixed");

    if (!is_fast) {
        printf ("GOP Structure     : ");

        if (have_NM)
            printf ("Fixed Length (N=%d, M=%d)\n", N, M);
        else
            printf ("Unfixed\n");

        printf ("                    %.*s%s\n",
                54, video_peek_all_frame_types (V),
                video_peek_nframes (V) > 54 ? "..." : "");
    }
}


int mode_info (int argc, char* argv[])
{
    ErrorCode err;
    ArgParse* P;
    Video* V;

    int ret = EXIT_SUCCESS;;


    P = _parse (argc, argv);

    err = video_open (&V, P->argv[0]);
    if (err) {
        fprintf (stderr, "%s ['%s']\n", err2str (err), P->argv[0]);
        ret = EXIT_FAILURE;
        goto out;
    }

    _dump_info (V, P);

    video_destroy (&V);
out:
    argparse_destroy (&P);
    return ret;
}
