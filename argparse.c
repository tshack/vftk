/*
MIT License

Copyright (c) 2019 James A. Shackleford

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software. */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "internal/argparse.h"

#include "list.h"

#define SCREEN_WIDTH 80

#define to_def(ptr) container_of(ptr, ArgDef, node);

typedef struct {
    void* storage;
    uint8_t type;
    const char* argshort;
    const char* arglong;
    size_t arglong_len;
    const char* help;
    uint8_t required;
    uint8_t found;
    List node;
} ArgDef;


static inline uint8_t _type (uint8_t flags)
{
    return flags & 0x0F;
}


static inline uint8_t _is_required (uint8_t flags)
{
    return (flags >> 4) & 0x01;
}


static inline size_t _get_arglong_len (const char* token)
{
    size_t n = 0;

    while (!(*token == '=' || *token++ == ' '))
        n++;

    return n;
}


static inline const char* _get_name (ArgDef* def)
{
    if (def->arglong)
        return def->arglong;
    else if (def->argshort)
        return def->argshort;
    else
        return "(undefined)";
}


ArgDef* _find_def_short (ArgParse* ap, char token)
{
    ArgDef* def;
    List* cur;

    list_foreach (&ap->head, cur) {
        def = to_def (cur);

        if (def->argshort && def->argshort[1] == token)
            return def;
    }

    return NULL;
}


ArgDef* _find_def_long (ArgParse* ap, char* token)
{
    ArgDef* def;
    List* cur;

    list_foreach (&ap->head, cur) {
        def = to_def (cur);

        if ( def->arglong && !strncmp (def->arglong, token, def->arglong_len) )
            return def;
    }

    return NULL;
}


static int _is_int (char* token)
{
    for (; *token != '\0'; token++)
        if (!isdigit (*token))
            return 0;

    return 1;
}


static int _is_float (char* token)
{
    for (; *token != '\0'; token++)
        if (!(isdigit (*token) || (*token == '.') || (*token == '-')))
            return 0;

    return 1;
}


static int _process_int (ArgDef* def, char* val)
{
    if (!_is_int (val))
        return 0;

    def->found = 1;
    *(int*)def->storage = atoi(val);

    return 1;
}


static int _process_float (ArgDef* def, char* val)
{
    if (!_is_float (val))
        return 0;

    def->found = 1;
    *(float*)def->storage = atof(val);

    return 1;
}


static int _process_str (ArgDef* def, char* val)
{
    def->found = 1;
    *(char**)def->storage = val;

    return 1;
}


static int _def_set_value (ArgDef* def, char* val)
{
    switch (def->type) {
    case ARG_INT:
        return _process_int   (def, val);
        break;
    case ARG_FLOAT:
        return _process_float (def, val);
        break;
    case ARG_STR:
        return _process_str   (def, val);
        break;
    default:
        return 0;
    };
}


static int _def_process_long (ArgParse* ap, ArgDef* def, char* arg)
{
    char* val;
    int ret;

    if (def->type == ARG_FLAG) {
        *(int*)def->storage = 1;
        def->found = 1;
        return 1;
    } else if ((strlen(arg) == def->arglong_len
                    || strlen(arg) == def->arglong_len + 1) ) {
        printf ("Argument '%s' requires a value.\n", arg);
        return 0;
    }

    val = arg + def->arglong_len + 1;

    ret = _def_set_value (def, val);

    if (!ret)
        printf ("Invalid value '%s' supplied to '%s'.\n", val, arg);

    return ret;
}


static int _def_process_short_flag (ArgDef* def)
{
    if (def->type == ARG_FLAG) {
        (*(int*)def->storage)++;
        return 1;
    }

    return 0;
}


static int _def_process_short_val (ArgParse* ap, ArgDef* def, int* i)
{
    int argc = ap->argc;
    char** argv = ap->argv;
    char* val;
    int ret;

    if ( (*i + 1) >= argc || ( _find_def_short (ap, argv[*i + 1][1])
                               && (argv[*i + 1][0] == '-')
                             )
       ) {
        printf ("Argument '%s' requires a value.\n", argv[*i]);
        return 0;
    }

    val = argv[++(*i)];

    ret = _def_set_value (def, val);

    if (!ret)
        printf ("Invalid value '%s' supplied to '%s'.\n", val, argv[*i - 1]);

    return ret;
}


static int _all_required_found (ArgParse* ap)
{
    List* cur;
    ArgDef* def;

    list_foreach (&ap->head, cur) {
        def = to_def (cur);

        if (def->required && !def->found) {
            printf ("Argument '%s' is required\n", _get_name (def));

            return 0;
        }
    }

    return 1;
}


static int _word_len (const char* word)
{
    int n = 0;

    while (*word && *word++ != ' ' )
        n++;

    return n;
}


static int _token_is_shortarg (char* token) {
    if (token[0] == '\0' || token[1] == '\0')
        return 0;

    if (token[0] == '-' && token[1] != '-')
        return 1;

    return 0;
}


static int _token_is_longarg (char* token) {
    if (token[0] == '\0' || token[1] == '\0')
        return 0;

    if (token[0] == '-' && token[1] == '-')
        return 1;

    return 0;
}


static int _argparse_compound_flags (ArgParse* ap, char* token)
{
    int i;
    ArgDef* def;

    for (i=1; token[i]; i++) {
        if ( !(def = _find_def_short (ap, token[i])) ) {
            printf ("Invalid argument: -%c\n", token[i]);
            return 0;
        }

        if (def->type != ARG_FLAG) {
            printf ("Argument -'%c' requires a value.\n", token[i]);
            return 0;
        }

        if (!_def_process_short_flag (def))
            return 0;
    }

    return 1;
}


static int _argparse_short (ArgParse* ap, char* token, int* i)
{
    ArgDef* def;

    if ( !(def = _find_def_short (ap, token[1])) ) {
        printf ("Invalid argument: -%c\n", token[1]);
        return 0;
    }

    if (def->type == ARG_FLAG) {
        if (!_def_process_short_flag (def))
            return 0;
    } else {
        if (def->found) {
            printf ("Argument '%s' supplied more than once.\n", def->argshort);
            return 0;
        }

        if (!_def_process_short_val (ap, def, i))
            return 0;
    }

    return 1;
}


static int _argparse_long (ArgParse* ap, char* token)
{
    ArgDef* def;

    if ( !(def = _find_def_long (ap, token)) ) {
        printf ("Invalid argument: %s\n", token);
        return 0;
    }

    if (def->found) {
        printf ("Argument '%s' supplied more than once.\n", def->arglong);
        return 0;
    }

    if (!_def_process_long (ap, def, token))
        return 0; /* invalid arg arg */

    return 1;
}


int argparse (ArgParse* ap)
{
    int i;

    int argc = ap->argc;
    char** argv = ap->argv;

    for (i=1; i<argc; i++) {
        if (_token_is_shortarg (argv[i])) {
            if (strlen (argv[i]) > 2) {
                if (!_argparse_compound_flags (ap, argv[i]))
                    return 0;
            } else {
                if (!_argparse_short (ap, argv[i], &i))
                    return 0;
            }
        } else if (_token_is_longarg (argv[i])) {
            if (!_argparse_long (ap, argv[i]))
                return 0;
        } else {
            break;
        }
    }

    ap->argv = &argv[i];
    ap->argc = argc - i;
    return 1;
}


int argparse_check_required (ArgParse* ap)
{
    if (!_all_required_found (ap))
        return 0;

    return 1;
}


void argparse_print (ArgParse* ap)
{
    ArgDef* def;
    List* cur;
    const char* help;
    int n;

    list_foreach (&ap->head, cur) {
        def = to_def (cur);

        help = def->help;

        n  = printf ("  %s", def->argshort ? def->argshort : "  ");
        n += printf ("%c ", def->argshort ? ',' : ' ');
        n += printf ("%s  ", def->arglong ? def->arglong : "");

        while (*help) {
            while (n < 28)
                n += printf (" ");

            while (*help && n < SCREEN_WIDTH - 1) {
                if (n + _word_len (help) < SCREEN_WIDTH - 1) {
                    while (_word_len (help) > 0)
                        n += printf ("%c", *help++);

                    if (*help && *help == ' ')
                        n += printf ("%c", *help++);
                } else
                    break;
            }

            if (*help && *help == ' ')
                help++;

            printf ("\n");
            n = 0;
        }
    }
}


void argparse_add (
    ArgParse* ap,
    uint8_t flags,
    const char* argshort,
    const char* arglong,
    const char* help,
    void* storage)
{
    ArgDef* def = malloc (sizeof(*def));
    list_init (&def->node);

    def->storage = storage;
    def->type = _type (flags);
    def->argshort = argshort;
    def->arglong = arglong;
    def->arglong_len = arglong ? _get_arglong_len (arglong) : 0;
    def->help = help;
    def->required = _is_required (flags);
    def->found = 0;

    list_add_before (&ap->head, &def->node);
}


ArgParse* argparse_new (int argc, char* argv[])
{
    ArgParse* ap = malloc (sizeof(*ap));
    list_init (&ap->head);

    ap->argc = argc;
    ap->argv = argv;

    return ap;
}


void argparse_destroy (ArgParse** ap)
{
    List *cur, *sav;
    ArgDef* def;

    list_foreach_safe (&(*ap)->head, cur, sav) {
        def = to_def (cur);

        list_remove (cur);
        free (def);
    }

    free (*ap);

    *ap = NULL;
}
