cmake_minimum_required(VERSION 2.8.5)

project(vftk)

set (VFTK_DEFAULT_BUILD "Develop")

set (LIBVFTK_VERSION_MAJOR "0")
set (LIBVFTK_VERSION_MINOR "5")
set (LIBVFTK_VERSION_PATCH "4")
set (LIBVFTK_VERSION ${LIBVFTK_VERSION_MAJOR}.${LIBVFTK_VERSION_MINOR}.${LIBVFTK_VERSION_PATCH})
if (NOT VFTK_DEFAULT_BUILD STREQUAL "Release")
    set (LIBVFTK_VERSION "${LIBVFTK_VERSION}-alpha")
endif ()

include(cmake/macros.cmake)
include(cmake/ExternalFFmpeg.cmake)
set (CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

set(BUILD_CYTHON   OFF CACHE BOOL "Build Python Module?")
set(BUILD_TESTING  ON  CACHE BOOL "Build Unit Tests?")

# put build targets in sane locations
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# vftk Dependencies
find_package (TIFF REQUIRED)

# FFmpeg Dependencies
find_package (Threads REQUIRED)
find_package (ZLIB REQUIRED)
find_package (X11 REQUIRED)
find_package (VAAPI REQUIRED)
find_package (VDPAU REQUIRED)

if (BUILD_TESTING)
    enable_testing ()
endif ()

ExternalFFmpeg(ffmpeg
    # Git tag to checkout
    "n4.0"
    # Git clone destination
    "${CMAKE_BINARY_DIR}/ffmpeg/src"
    # Install destination
    "${CMAKE_BINARY_DIR}/ffmpeg"
)

include_directories (
    ${FFMPEG_INCLUDE_DIRS}
    ${TIFF_INCLUDE_DIRS}
    ${CMAKE_SOURCE_DIR}/include
)

set (VFTK_LIBRARIES
    ${TIFF_LIBRARIES}
    ${FFMPEG_LIBRARIES}
    "m"
)

set (SRC_LIBVFTK
    tiff.c
    frame.c
    video.c
    error.c
    list.c
    preproc.c
    preproc_gopnm.c
    preproc_ts_luts.c
    preproc_ftypes.c
    atom_tree.c
    atom_parse.c
)


# JAS 2019-02-18: gcc does not produce relocs for libavcodec.a(hevec_cabac.o)
# via -fPIC for ff_h264_cabac_tables due to it being optimized with hand
# written assembly (i.e. ff_h264_cabac_tables is an R_X86_64_PC32 relocation).
# The linker flag '-Bsymbolic' is necessary for resolution.
set (CMAKE_SHARED_LINKER_FLAGS "-Wl,-Bsymbolic")

add_library (libvftk SHARED ${SRC_LIBVFTK})
add_dependencies (libvftk ffmpeg)
target_link_libraries (libvftk PUBLIC ${VFTK_LIBRARIES})
set_target_properties (libvftk PROPERTIES
    OUTPUT_NAME vftk
    VERSION ${LIBVFTK_VERSION}
    SOVERSION ${LIBVFTK_VERSION_MAJOR}
)


set (SRC_VFTK_BIN
    vftk.c
    argparse.c
    mode_info.c
    mode_mv.c
    mode_ftypes.c
    mode_frames.c
    mode_perror.c
    mode_tsdump.c
    mode_atom.c
)

add_subdirectory (cython)

add_executable (vftk-bin ${SRC_VFTK_BIN})
target_link_libraries (vftk-bin libvftk)
set_target_properties (vftk-bin PROPERTIES OUTPUT_NAME vftk)

add_subdirectory (test)


# Create our own build type for general development
set (CMAKE_C_FLAGS_DEVELOP "-O2 -std=c99 -g -Wall" CACHE STRING "C flags used during development")
set (CMAKE_CXX_FLAGS_DEVELOP "-O2 -std=c++03 -g -Wall" CACHE STRING "C++ flags used during development")
mark_as_advanced (CMAKE_C_FLAGS_DEVELOP CMAKE_CXX_FLAGS_DEVELOP)

# Set default build type if not specified
if (NOT CMAKE_BUILD_TYPE)
    set (CMAKE_BUILD_TYPE ${VFTK_DEFAULT_BUILD} CACHE STRING
        "Options are: Release Debug Develop Coverage RelWithDebInfo MinSizeRel."
        FORCE)
endif()


### BUILD INSTALL TARGETS ##################################################

# JAS 2019-02-19: Installing to the default prefix /usr/local creates
# several issues due to non-compliance with the Filesystem Hierarchy
# Standard (FHS) exhibited by many UNIX systems & Python.  In such
# cases, /usr/local/lib is not in the ldconfig path and, more
# importantly, not searched by Python for modules.
if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "/usr"
        CACHE PATH
        "Install path prefix, prepended onto install directories."
        FORCE
    )
endif ()

install (DIRECTORY include/
    DESTINATION include/vftk
    PATTERN "internal" EXCLUDE
)
install (TARGETS libvftk DESTINATION lib)
install (TARGETS vftk-bin DESTINATION bin)


### BUILD UNINSTALL TARGET #################################################
if (NOT TARGET uninstall)
    configure_file (
        "${CMAKE_SOURCE_DIR}/cmake/uninstall.cmake.in"
        "${CMAKE_BINARY_DIR}/uninstall.cmake"
        IMMEDIATE @ONLY
    )

    add_custom_target (uninstall
        COMMAND ${CMAKE_COMMAND} -P ${CMAKE_BINARY_DIR}/uninstall.cmake)
endif ()
