From e07027eb78e47380754996e74c372cddb6603c00 Mon Sep 17 00:00:00 2001
From: James A Shackleford <shack@linux.com>
Date: Sun, 11 Nov 2018 15:24:40 -0500
Subject: [PATCH] libavcodec: h264: save frame prediction error

This patch adds an additional AVFrame to H264Picture, which is carried
through to the H.264 macroblock decoder where its pixel buffer is
populated with the macroblock prediction error residuals derived from
the DCT coefficients.  Upon frame presentation (i.e. got_frame = 1), the
new H264Context member `perrorf_ptr` is set to point to the
corresponding prediction error AVFrame for the presented frame.
---
 libavcodec/h264_mb_template.c | 29 +++++++++++++++++++++++++++++
 libavcodec/h264_picture.c     |  4 ++++
 libavcodec/h264_slice.c       | 19 +++++++++++++++++++
 libavcodec/h264dec.c          | 17 +++++++++++++++++
 libavcodec/h264dec.h          |  6 ++++++
 5 files changed, 75 insertions(+)

diff --git a/libavcodec/h264_mb_template.c b/libavcodec/h264_mb_template.c
index d5ea26a6e3..d0ae6d9b03 100644
--- a/libavcodec/h264_mb_template.c
+++ b/libavcodec/h264_mb_template.c
@@ -45,6 +45,7 @@ static av_noinline void FUNC(hl_decode_mb)(const H264Context *h, H264SliceContex
     const int mb_xy   = sl->mb_xy;
     const int mb_type = h->cur_pic.mb_type[mb_xy];
     uint8_t *dest_y, *dest_cb, *dest_cr;
+    uint8_t *perror_y, *perror_cb, *perror_cr;
     int linesize, uvlinesize /*dct_offset*/;
     int i, j;
     const int *block_offset = &h->block_offset[0];
@@ -57,6 +58,10 @@ static av_noinline void FUNC(hl_decode_mb)(const H264Context *h, H264SliceContex
     dest_cb = h->cur_pic.f->data[1] +  (mb_x << PIXEL_SHIFT) * 8 + mb_y * sl->uvlinesize * block_h;
     dest_cr = h->cur_pic.f->data[2] +  (mb_x << PIXEL_SHIFT) * 8 + mb_y * sl->uvlinesize * block_h;
 
+    perror_y  = h->cur_pic.perrorf->data[0] + ((mb_x << PIXEL_SHIFT)     + mb_y * sl->linesize)  * 16;
+    perror_cb = h->cur_pic.perrorf->data[1] +  (mb_x << PIXEL_SHIFT) * 8 + mb_y * sl->uvlinesize * block_h;
+    perror_cr = h->cur_pic.perrorf->data[2] +  (mb_x << PIXEL_SHIFT) * 8 + mb_y * sl->uvlinesize * block_h;
+
     h->vdsp.prefetch(dest_y  + (sl->mb_x & 3) * 4 * sl->linesize   + (64 << PIXEL_SHIFT), sl->linesize,       4);
     h->vdsp.prefetch(dest_cb + (sl->mb_x & 7)     * sl->uvlinesize + (64 << PIXEL_SHIFT), dest_cr - dest_cb, 2);
 
@@ -70,6 +75,10 @@ static av_noinline void FUNC(hl_decode_mb)(const H264Context *h, H264SliceContex
             dest_y  -= sl->linesize * 15;
             dest_cb -= sl->uvlinesize * (block_h - 1);
             dest_cr -= sl->uvlinesize * (block_h - 1);
+
+            perror_y  -= sl->linesize * 15;
+            perror_cb -= sl->uvlinesize * (block_h - 1);
+            perror_cr -= sl->uvlinesize * (block_h - 1);
         }
         if (FRAME_MBAFF(h)) {
             int list;
@@ -187,6 +196,16 @@ static av_noinline void FUNC(hl_decode_mb)(const H264Context *h, H264SliceContex
             }
         }
 
+        for (j=0; j<16; j++)
+            for (i=0; i<16; i++)
+                perror_y[linesize*j + i] = dest_y[linesize*j + i];
+
+        for (j=0; j<8; j++)
+            for (i=0; i<8; i++) {
+                perror_cb[uvlinesize*j + i] = dest_cb[uvlinesize*j + i];
+                perror_cr[uvlinesize*j + i] = dest_cr[uvlinesize*j + i];
+            }
+
         hl_decode_mb_idct_luma(h, sl, mb_type, SIMPLE, transform_bypass,
                                PIXEL_SHIFT, block_offset, linesize, dest_y, 0);
 
@@ -245,6 +264,16 @@ static av_noinline void FUNC(hl_decode_mb)(const H264Context *h, H264SliceContex
             }
         }
     }
+
+    for (j=0; j<16; j++)
+        for (i=0; i<16; i++)
+            perror_y[linesize*j + i] = (uint8_t)((((int16_t)dest_y[linesize*j + i] - (int16_t)perror_y[linesize*j + i])/ 2) + 128);
+
+    for (j=0; j<8; j++)
+        for (i=0; i<8; i++) {
+            perror_cb[uvlinesize*j + i] = (uint8_t)((((int16_t)dest_cb[uvlinesize*j + i] - (int16_t)perror_cb[uvlinesize*j + i]) / 2) + 128);
+            perror_cr[uvlinesize*j + i] = (uint8_t)((((int16_t)dest_cr[uvlinesize*j + i] - (int16_t)perror_cr[uvlinesize*j + i]) / 2) + 128) ;
+        }
 }
 
 #if !SIMPLE || BITS == 8
diff --git a/libavcodec/h264_picture.c b/libavcodec/h264_picture.c
index e833835a77..ae2daa9c5e 100644
--- a/libavcodec/h264_picture.c
+++ b/libavcodec/h264_picture.c
@@ -53,6 +53,8 @@ void ff_h264_unref_picture(H264Context *h, H264Picture *pic)
     ff_thread_release_buffer(h->avctx, &pic->tf);
     av_buffer_unref(&pic->hwaccel_priv_buf);
 
+    av_frame_unref (pic->perrorf);
+
     av_buffer_unref(&pic->qscale_table_buf);
     av_buffer_unref(&pic->mb_type_buf);
     for (i = 0; i < 2; i++) {
@@ -76,6 +78,8 @@ int ff_h264_ref_picture(H264Context *h, H264Picture *dst, H264Picture *src)
     if (ret < 0)
         goto fail;
 
+    av_frame_ref(dst->perrorf, src->perrorf);
+
     dst->qscale_table_buf = av_buffer_ref(src->qscale_table_buf);
     dst->mb_type_buf      = av_buffer_ref(src->mb_type_buf);
     if (!dst->qscale_table_buf || !dst->mb_type_buf) {
diff --git a/libavcodec/h264_slice.c b/libavcodec/h264_slice.c
index d71ddbe9ba..d6307c07e8 100644
--- a/libavcodec/h264_slice.c
+++ b/libavcodec/h264_slice.c
@@ -185,6 +185,20 @@ static int init_table_pools(H264Context *h)
     return 0;
 }
 
+static int h264_perrorf_get_buffer(H264Context* h, H264Picture* pic)
+{
+    int ret;
+
+    pic->perrorf->width = pic->f->width;
+    pic->perrorf->height = pic->f->height;
+    pic->perrorf->format = pic->f->format;
+
+    if ((ret = av_frame_get_buffer (pic->perrorf, 0)) < 0)
+        return ret;
+
+    return 0;
+}
+
 static int alloc_picture(H264Context *h, H264Picture *pic)
 {
     int i, ret = 0;
@@ -197,6 +211,11 @@ static int alloc_picture(H264Context *h, H264Picture *pic)
     if (ret < 0)
         goto fail;
 
+    ret = h264_perrorf_get_buffer(h, pic);
+
+    if (ret < 0)
+        goto fail;
+
     if (h->avctx->hwaccel) {
         const AVHWAccel *hwaccel = h->avctx->hwaccel;
         av_assert0(!pic->hwaccel_picture_private);
diff --git a/libavcodec/h264dec.c b/libavcodec/h264dec.c
index 7494c7a8f2..c2b8e976cb 100644
--- a/libavcodec/h264dec.c
+++ b/libavcodec/h264dec.c
@@ -339,16 +339,28 @@ static int h264_init_context(AVCodecContext *avctx, H264Context *h)
         h->DPB[i].f = av_frame_alloc();
         if (!h->DPB[i].f)
             return AVERROR(ENOMEM);
+
+        h->DPB[i].perrorf = av_frame_alloc();
+        if (!h->DPB[i].perrorf)
+            return AVERROR(ENOMEM);
     }
 
     h->cur_pic.f = av_frame_alloc();
     if (!h->cur_pic.f)
         return AVERROR(ENOMEM);
 
+    h->cur_pic.perrorf = av_frame_alloc();
+    if (!h->cur_pic.perrorf)
+        return AVERROR(ENOMEM);
+
     h->last_pic_for_ec.f = av_frame_alloc();
     if (!h->last_pic_for_ec.f)
         return AVERROR(ENOMEM);
 
+    h->last_pic_for_ec.perrorf = av_frame_alloc();
+    if (!h->last_pic_for_ec.perrorf)
+        return AVERROR(ENOMEM);
+
     for (i = 0; i < h->nb_slice_ctx; i++)
         h->slice_ctx[i].h264 = h;
 
@@ -366,6 +378,7 @@ static av_cold int h264_decode_end(AVCodecContext *avctx)
     for (i = 0; i < H264_MAX_PICTURE_COUNT; i++) {
         ff_h264_unref_picture(h, &h->DPB[i]);
         av_frame_free(&h->DPB[i].f);
+        av_frame_free(&h->DPB[i].perrorf);
     }
     memset(h->delayed_pic, 0, sizeof(h->delayed_pic));
 
@@ -381,8 +394,10 @@ static av_cold int h264_decode_end(AVCodecContext *avctx)
 
     ff_h264_unref_picture(h, &h->cur_pic);
     av_frame_free(&h->cur_pic.f);
+    av_frame_free(&h->cur_pic.perrorf);
     ff_h264_unref_picture(h, &h->last_pic_for_ec);
     av_frame_free(&h->last_pic_for_ec.f);
+    av_frame_free(&h->last_pic_for_ec.perrorf);
 
     return 0;
 }
@@ -909,6 +924,8 @@ static int finalize_frame(H264Context *h, AVFrame *dst, H264Picture *out, int *g
 
         *got_frame = 1;
 
+        h->perrorf_ptr = out->perrorf;
+
         if (CONFIG_MPEGVIDEO) {
             ff_print_debug_info2(h->avctx, dst, NULL,
                                  out->mb_type,
diff --git a/libavcodec/h264dec.h b/libavcodec/h264dec.h
index 1d9723260d..77828bfbb3 100644
--- a/libavcodec/h264dec.h
+++ b/libavcodec/h264dec.h
@@ -127,6 +127,7 @@ typedef struct MMCO {
 
 typedef struct H264Picture {
     AVFrame *f;
+    AVFrame *perrorf;
     ThreadFrame tf;
 
     AVBufferRef *qscale_table_buf;
@@ -510,6 +511,11 @@ typedef struct H264Context {
      */
     int recovery_frame;
 
+    /**
+     * pointer to prediction error frame
+     */
+    AVFrame* perrorf_ptr;
+
 /**
  * We have seen an IDR, so all the following frames in coded order are correctly
  * decodable.
-- 
2.19.1

