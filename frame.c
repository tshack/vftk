/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include "libavutil/imgutils.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/motion_vector.h"

#include "error.h"
#include "frame.h"

static void _set_type (Frame* F, AVFrame* avframe)
{
    switch (avframe->pict_type) {
        case AV_PICTURE_TYPE_I:
            F->type = I_FRAME;
            break;
        case AV_PICTURE_TYPE_P:
            F->type = P_FRAME;
            break;
        case AV_PICTURE_TYPE_B:
            F->type = B_FRAME;
            break;
        case AV_PICTURE_TYPE_NONE:
        default:
            F->type = UNKNOWN_FRAME;
    }
}


ErrorCode frame_set_picture (Frame* F, AVFrame* avframe)
{
    int ret;
    uint8_t *p, *p0;
    unsigned int i, j;
    struct SwsContext* sws;
    uint8_t* rgb24_data[4];
    int rgb24_linesize[4];

    ErrorCode err = SUCCESS;

    sws = sws_getContext (avframe->width, avframe->height, avframe->format,
                          avframe->width, avframe->height, AV_PIX_FMT_RGB24,
                          SWS_BICUBIC, NULL, NULL, NULL);

    if (!sws) {
        err = EFRAME_RGB24;
        goto out;
    }

    ret = av_image_alloc (rgb24_data, rgb24_linesize,
                          avframe->width, avframe->height,
                          AV_PIX_FMT_RGB24, 1);

    if (ret < 0) {
        err = EFRAME_RGB24;
        goto out_sws;
    }

    sws_scale (sws, (const uint8_t * const*)avframe->data, avframe->linesize,
               0, avframe->height, rgb24_data, rgb24_linesize);

    if (F->R) free (F->R);
    if (F->G) free (F->G);
    if (F->B) free (F->B);

    F->R = malloc (avframe->width * avframe->height * sizeof (*F->R));
    F->G = malloc (avframe->width * avframe->height * sizeof (*F->G));
    F->B = malloc (avframe->width * avframe->height * sizeof (*F->B));

    p0 = rgb24_data[0];
    for (j=0; j<avframe->height; j++) {
        p = p0;
        for (i=0; i<avframe->width; i++) {
            F->R[avframe->width*j + i] = p[3*i+0];
            F->G[avframe->width*j + i] = p[3*i+1];
            F->B[avframe->width*j + i] = p[3*i+2];
        }
        p0 += rgb24_linesize[0];
    }

    F->w = avframe->width;
    F->h = avframe->height;
    _set_type (F, avframe);

    /* Frame Properties */
    F->chroma_location = avframe->chroma_location;
    F->color_primaries = avframe->color_primaries;
    F->color_range = avframe->color_range;
    F->color_xfer_characteristic = avframe->color_trc;
    F->is_interlaced = avframe->interlaced_frame;

    av_freep (rgb24_data);
out_sws:
    sws_freeContext (sws);
out:
    return err;
}


ErrorCode frame_set_motion_vecs (Frame* F, AVFrame* avframe)
{
    AVFrameSideData* sd;

    if (F->motion_vecs)
        free (F->motion_vecs);

    sd = av_frame_get_side_data (avframe, AV_FRAME_DATA_MOTION_VECTORS);

    if (!sd) {
        F->num_motion_vecs = 0;
        F->motion_vecs = NULL;
        return EFRAME_MV;
    }

    F->num_motion_vecs = sd->size/sizeof(AVMotionVector);
    F->motion_vecs = malloc (sd->size);
    memcpy (F->motion_vecs, sd->data, sd->size);

    return SUCCESS;
}


void frame_destroy (Frame** F)
{

    if (!*F)
        return;

    if ((*F)->R)
        free ((*F)->R);

    if ((*F)->G)
        free ((*F)->G);

    if ((*F)->B)
        free ((*F)->B);

    if ((*F)->motion_vecs)
        free ((*F)->motion_vecs);

    free (*F);
    *F = NULL;
}


Frame* frame_new ()
{
    Frame* F = malloc (sizeof(*F));
    memset (F, 0, sizeof(*F));

    return F;
}


Frame* frame_new_blank (int width, int height, uint8_t* rgb)
{
    int n;

    Frame* F = frame_new ();
    F->R = malloc (width * height * sizeof(*F->R));
    F->G = malloc (width * height * sizeof(*F->G));
    F->B = malloc (width * height * sizeof(*F->B));
    F->w = width;
    F->h = height;

    for (n=0; n<width*height; n++) {
        F->R[n] = rgb[0];
        F->G[n] = rgb[1];
        F->B[n] = rgb[2];
    }

    return F;
}


Frame* frame_clone_empty (Frame* src)
{
    Frame* F = frame_new ();

    F->R = malloc (src->w * src->h * sizeof(*src->R));
    F->G = malloc (src->w * src->h * sizeof(*src->G));
    F->B = malloc (src->w * src->h * sizeof(*src->B));
    F->w = src->w;
    F->h = src->h;

    return F;
}


void frame_fill (Frame* F, uint8_t* rgb)
{
    memset (F->R, rgb[0], sizeof(*F->R) * F->w * F->h);
    memset (F->G, rgb[1], sizeof(*F->G) * F->w * F->h);
    memset (F->B, rgb[2], sizeof(*F->B) * F->w * F->h);
}
