/**************************************************************
 * vftk (command line utility)
 *   James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *
 *****/
#include <stdlib.h>
#include <stdio.h>

#include <libavformat/avformat.h>

#include "error.h"
#include "video.h"


typedef int (*vftk_mode_t)(int argc, char* argv[]);

typedef struct {
    const char* name;
    const char* description;
    vftk_mode_t mode;
} ModeEntry;


int mode_info   (int argc, char* argv[]);
int mode_frames (int argc, char* argv[]);
int mode_mv     (int argc, char* argv[]);
int mode_ftypes (int argc, char* argv[]);
int mode_perror (int argc, char* argv[]);
int mode_tsdump (int argc, char* argv[]);
int mode_atom   (int argc, char* argv[]);


static ModeEntry modes[] = {
    { "info",     "General Video Information",             mode_info     },
    { "ftypes",   "List Frame Types",                      mode_ftypes   },
    { "frames",   "Frame Extraction",                      mode_frames   },
    { "tsdump",   "Frame Decode/Presentation Timestamps",  mode_tsdump   },
    { "atom",     "Explore container atoms",               mode_atom     },
    { "mv",       "Motion Vector Extraction",              mode_mv       },
    { "perror",   "Frame Prediction Errors",               mode_perror   },
    { NULL}
};


static vftk_mode_t _select_mode (const char* selection)
{
    int i;

    for (i=0; modes[i].name; i++) {
        if (!strcmp (modes[i].name, selection))
            return modes[i].mode;
    }

    return NULL;
}


static void print_usage ()
{
    int i, j, max;

    for (i=0, max=0; modes[i].name; i++)
        if (strlen (modes[i].name) > max)
            max = strlen (modes[i].name);

    printf ("Usage: vftk MODE [--help]\n"
            "\n"
            "  Modes:\n");

    for (i=0; modes[i].name; i++) {
        printf ("   %s ", modes[i].name);
        for (j=0; j<max-strlen(modes[i].name); j++)
            printf (".");

        for (j=0; j<5; j++)
            printf (".");

        printf (" %s\n", modes[i].description);
    }

    printf ("\n");
}


int main (int argc, char* argv[])
{
    vftk_mode_t mode;

    av_log_set_level (AV_LOG_QUIET);

    if (argc < 2) {
        print_usage ();
        return EXIT_FAILURE;
    }

    if ( !(mode = _select_mode (argv[1])) ) {
        print_usage ();
        printf ("Invalid mode '%s'\n", argv[1]);
        return EXIT_FAILURE;
    } else
        return mode (--argc, ++argv);
}
