/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "internal/atom_test.h"

#include "atom.h"
#include "list.h"

#define CHECK_VALUE(name) _check_val (parse, #name, &test_vector[i].name);
#define CHECK_ARRAY(name) _check_val (parse, #name"[]", &test_vector[i].name);

typedef struct {
    char* fn;
    uint64_t size;
    long offset;
    char* fields[12];
    uint32_t version_flags;
    uint64_t creation_time;
    uint64_t modification_time;
    uint32_t track_ID;
    uint64_t duration;
    int16_t layer;
    int16_t alternate_group;
    int16_t volume;
    int32_t matrix[9];
    uint32_t width;
    uint32_t height;
} Test;


static const Test test_vector[] = {
    { .fn = "./videos/30fps_closedgop_N10_M1.mp4",
      .size = 92,
      .offset = 0x1f184,
      .fields = {
          "version+flags",
          "creation_time",
          "modification_time",
          "track_ID",
          "duration",
          "layer",
          "alternate_group",
          "volume",
          "matrix[]",
          "width",
          "height",
          NULL
      },
      .version_flags = 0x00000003,
      .creation_time = 0x00000000,
      .modification_time = 0x00000000,
      .track_ID = 0x00000001,
      .duration = 0x0000070a,
      .layer = 0x0000,
      .alternate_group = 0x0000,
      .volume = 0x0000,
      .matrix = { 0x00010000, 0x00000000, 0x00000000,
                  0x00000000, 0x00010000, 0x00000000,
                  0x00000000, 0x00000000, 0x40000000
                },
      .width = 0x02d00000,
      .height = 0x01940000
    },
    { NULL }
};


int main (int argc, char* argv[])
{
    AtomTree* at;
    Atom* atom;
    List* parse;
    int i;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Creating atom tree for '%s'\n", test_vector[i].fn);
        if ( !(at = atom_tree_create(test_vector[i].fn)) )
            return EXIT_FAILURE;

        atom = _find_atom (at, "moov.trak[0].tkhd");

        _check_size (atom, test_vector[i].size);
        _check_offset (atom, test_vector[i].offset);

        parse = atom_parse (at, atom);

        _check_fields (atom, parse, test_vector[i].fields);
        _check_val (parse, "version+flags", &test_vector[i].version_flags);

        CHECK_VALUE (creation_time);
        CHECK_VALUE (modification_time);
        CHECK_VALUE (track_ID);
        CHECK_VALUE (duration);
        CHECK_VALUE (layer);
        CHECK_VALUE (alternate_group);
        CHECK_VALUE (volume);
        CHECK_ARRAY (matrix);
        CHECK_VALUE (width);
        CHECK_VALUE (height);

        atom_parse_destroy (&parse);
        atom_tree_destroy (&at);
    }

    return EXIT_SUCCESS;
}
