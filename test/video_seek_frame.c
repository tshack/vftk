/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#include "error.h"
#include "video.h"


static char* test_vector[] = {
    "./videos/30fps_closedgop_N10_M1.mp4",
    "./videos/30fps_closedgop_N10_M4.mp4",
    "./videos/30fps_opengop_N10_M4.mp4",
    "./videos/vfr_closedgop_N10_M1.mp4",
    "./videos/vfr_closedgop_N10_M4.mp4",
    "./videos/vfr_opengop_N10_M4.mp4",
    NULL
};


int main (int argc, char* argv[])
{
    int i;
    int dir;
    Video* V;
    int64_t f, step;
    int64_t result;

    for (i=0; test_vector[i]; i++) {
        fprintf (stderr, "Opening '%s'\n", test_vector[i]);

        if (video_open (&V, test_vector[i]) != SUCCESS)
            return EXIT_FAILURE;

        f = 0;
        dir = 1;
        step = video_peek_nframes (V) - 3;

        fprintf (stderr, "Result\tExpected\n");

        do {
            if (!video_seek_frame (V, f)) {
                fprintf (stderr, "video_seek_frame () returned failure\n");
                return EXIT_FAILURE;
            }

            result = video_peek_framenumber (V);
            fprintf (stderr, "%03"PRId64"\t", result);
            fprintf (stderr, "%03"PRId64"\n", f);

            if (result != f)
                return EXIT_FAILURE;

            f += dir * step;
            dir *= -1;
            step--;
        } while (step >= 0);

        video_destroy (&V);
    }

    return EXIT_SUCCESS;
}
