/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "internal/atom_test.h"

#include "atom.h"
#include "list.h"

#define CHECK_VALUE(name) _check_val (parse, #name, &test_vector[i].name);
#define CHECK_ARRAY(name) _check_val (parse, #name"[]", &test_vector[i].name);

typedef struct {
    char* fn;
    uint64_t size;
    long offset;
    char* fields[4];
    char* major_brand[2];
    uint32_t minor_version;
    char* compatible_brands[5];
} Test;


static const Test test_vector[] = {
    { .fn = "./videos/30fps_closedgop_N10_M1.mp4",
      .size = 32,
      .offset = 0,
      .fields = {
          "major_brand",
          "minor_version",
          "compatible_brands[]",
          NULL
      },
      .major_brand = { "isom", NULL },
      .minor_version = 0x00000200,
      .compatible_brands = {
        "isom", "iso2", "avc1", "mp41", NULL
      }
    },
    { NULL }
};


int main (int argc, char* argv[])
{
    AtomTree* at;
    Atom* atom;
    List* parse;
    int i;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Creating atom tree for '%s'\n", test_vector[i].fn);
        if ( !(at = atom_tree_create(test_vector[i].fn)) )
            return EXIT_FAILURE;

        atom = _find_atom (at, "ftyp");

        _check_size (atom, test_vector[i].size);
        _check_offset (atom, test_vector[i].offset);

        parse = atom_parse (at, atom);

        _check_fields (atom, parse, test_vector[i].fields);

        CHECK_VALUE (major_brand);
        CHECK_VALUE (minor_version);
        CHECK_ARRAY (compatible_brands);

        atom_parse_destroy (&parse);
        atom_tree_destroy (&at);
    }

    return EXIT_SUCCESS;
}
