/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "internal/atom_test.h"

#include "atom.h"
#include "list.h"

#define CHECK_VALUE(name) _check_val (parse, #name, &test_vector[i].name);

typedef struct {
    char* fn;
    uint64_t size;
    long offset;
    char* fields[7];
    uint32_t version_flags;
    uint32_t entry_count;
    uint64_t segment_duration;
    int64_t media_time;
    uint32_t media_rate_integer;
    int32_t media_rate_fraction;
} Test;


static const Test test_vector[] = {
    { .fn = "./videos/30fps_closedgop_N10_M1.mp4",
      .size = 28,
      .offset = 0x1f1e8,
      .fields = {
          "version+flags",
          "entry_count",
          "segment_duration",
          "media_time",
          "media_rate_integer",
          "media_rate_fraction",
          NULL
      },
      .version_flags = 0x00000000,
      .entry_count = 0x00000001,
      .segment_duration = 0x0000070a,
      .media_time = 0x00000000,
      .media_rate_integer = 0x0001,
      .media_rate_fraction = 0x0000
    },
    { NULL }
};


int main (int argc, char* argv[])
{
    AtomTree* at;
    Atom* atom;
    List* parse;
    int i;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Creating atom tree for '%s'\n", test_vector[i].fn);
        if ( !(at = atom_tree_create(test_vector[i].fn)) )
            return EXIT_FAILURE;

        atom = _find_atom (at, "moov.trak[0].edts.elst");

        _check_size (atom, test_vector[i].size);
        _check_offset (atom, test_vector[i].offset);

        parse = atom_parse (at, atom);

        _check_fields (atom, parse, test_vector[i].fields);
        _check_val (parse, "version+flags", &test_vector[i].version_flags);

        CHECK_VALUE (entry_count);
        CHECK_VALUE (segment_duration);
        CHECK_VALUE (media_time);
        CHECK_VALUE (media_rate_integer);
        CHECK_VALUE (media_rate_fraction);

        atom_parse_destroy (&parse);
        atom_tree_destroy (&at);
    }

    return EXIT_SUCCESS;
}
