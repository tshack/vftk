/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>

#include "internal/util.h"

#include "error.h"
#include "frame.h"
#include "video.h"

typedef struct {
    char* fn;
    int64_t fnum_expected;
    FrameType ftype_expected;
    double time_expected;
    size_t w_expected;
    size_t h_expected;
    uint16_t R_expected;
    uint16_t G_expected;
    uint16_t B_expected;
} Test;


static const Test test_vector[] = {
    { "./videos/30fps_closedgop_N10_M1.mp4",
      0, I_FRAME, 0.0, 720, 404, 21176,  7361, 39759
    },
    { "./videos/30fps_closedgop_N10_M4.mp4",
      0, I_FRAME, 0.0, 720, 404, 42466, 48712, 32313
    },
    { "./videos/30fps_opengop_N10_M4.mp4",
      0, I_FRAME, 0.0, 720, 404, 42466, 48712, 32313
    },
    { "./videos/vfr_closedgop_N10_M1.mp4",
      0, I_FRAME, 0.0, 720, 404, 56522,  6675, 65075
    },
    { "./videos/vfr_closedgop_N10_M4.mp4",
      0, I_FRAME, 0.0, 720, 404, 21346, 63044, 45616
    },
    { "./videos/vfr_opengop_N10_M4.mp4",
      0, I_FRAME, 0.0, 720, 404, 21346, 63044, 45616
    },
    { NULL }
};


void _hash_frame (Frame* F, uint16_t* R, uint16_t* G, uint16_t* B)
{
    size_t i, j;
    *R = *G = *B = 5381;

    for (j=0; j<F->h; j++)
        for (i=0; i<F->w; i++) {
            *R = *R * 33 + F->R[j*F->w + i];
            *G = *G * 33 + F->G[j*F->w + i];
            *B = *B * 33 + F->B[j*F->w + i];
        }
}


int _pixels_good (Frame* F, int i)
{
    uint16_t R_result;
    uint16_t G_result;
    uint16_t B_result;

    _hash_frame (F, &R_result, &G_result, &B_result);

    fprintf (stderr, "R Channel Hash | Result: %05"PRIu16"  Expected: %05"PRIu16"\n",
            R_result, test_vector[i].R_expected);

    fprintf (stderr, "G Channel Hash | Result: %05"PRIu16"  Expected: %05"PRIu16"\n",
            G_result, test_vector[i].G_expected);

    fprintf (stderr, "B Channel Hash | Result: %05"PRIu16"  Expected: %05"PRIu16"\n",
            B_result, test_vector[i].B_expected);

    if (   R_result != test_vector[i].R_expected
        || G_result != test_vector[i].G_expected
        || B_result != test_vector[i].B_expected
        ) return 0;

    return 1;
}


int _size_good (Frame* F, int i)
{
    fprintf (stderr, "Frame Width    | Result: %5zu  Expected: %5zu\n",
            F->w, test_vector[i].w_expected);

    fprintf (stderr, "Frame Height   | Result: %5zu  Expected: %5zu\n",
            F->h, test_vector[i].h_expected);

    if (F->w != test_vector[i].w_expected || F->h != test_vector[i].h_expected)
        return 0;

    return 1;
}


int _type_good (Frame* F, int i)
{
    fprintf (stderr, "Frame Type     | Result: %5s  Expected: %5s\n",
            _frametype2str (F->type),
            _frametype2str (test_vector[i].ftype_expected));

    if (F->type != test_vector[i].ftype_expected)
        return 0;

    return 1;
}


int _time_good (Frame* F, int i)
{
    fprintf (stderr, "Frame Time     | Result: %1.3f  Expected: %1.3f\n",
            F->time_s, test_vector[i].time_expected);

    if (fabs(F->time_s - test_vector[i].time_expected) > 0.00001)
        return 0;

    return 1;
}


int main (int argc, char* argv[])
{
    int i;
    Video* V;

    Frame* F = frame_new ();

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Opening '%s'\n", test_vector[i].fn);

        if (video_open (&V, test_vector[i].fn) != SUCCESS)
            return EXIT_FAILURE;

        video_get_frame (V, F);

        if (!_time_good   (F, i)) return EXIT_FAILURE;
        if (!_type_good   (F, i)) return EXIT_FAILURE;
        if (!_size_good   (F, i)) return EXIT_FAILURE;
        if (!_pixels_good (F, i)) return EXIT_FAILURE;

        video_destroy (&V);
    }

    frame_destroy (&F);

    return EXIT_SUCCESS;
}
