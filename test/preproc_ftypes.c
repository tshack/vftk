/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "internal/preproc.h"

#include "error.h"
#include "video.h"

typedef struct {
    char* fn;
    char* expected;
} Test;


static const Test test_vector[] = {
    { "./videos/30fps_closedgop_N10_M1.mp4",
      "IPPPPPPPPPIPPPPPPPPPIPPPPPPPPPIPPPPPPPPPIPPPPPPPPPIPPP"
    },

    { "./videos/30fps_closedgop_N10_M4.mp4",
      "IPBBBPPPBPIPBBPBPPBPIBPBBBPBBPIBBPBPPPBPIBPPBBBPBPIB"
    },

    { "./videos/30fps_opengop_N10_M4.mp4",
      "IPBBBPPPBBIPBBPBPPBBIBPBBBPBBBIBBPBPPPBBIBPPBBBPBBIB"
    },

    { "./videos/vfr_closedgop_N10_M1.mp4",
      "IPPPPPPPPPIPPPPPPPPPIPPPPPPPPPIPPPPPPPPPIPPPP"
    },

    { "./videos/vfr_closedgop_N10_M4.mp4",
      "IPPBBPPBBPIBPPPBBPPPIBPBBBPPBPIPBBBPPBBPIBP"
    },

    { "./videos/vfr_opengop_N10_M4.mp4",
      "IPPBBPPBBPIBPPPBBPPBIBPBBBPPBPIPBBBPPBBPIBP"
    },

    { NULL }
};


int main (int argc, char* argv[])
{
    int i;
    Video* V;
    char* result;
    ErrorCode ret;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Opening '%s' ", test_vector[i].fn);

        ret = video_open (&V, test_vector[i].fn);
        if (ret) {
            fprintf (stderr, "FAILED! %s\n", err2str(ret));
            return EXIT_FAILURE;
        }
        fprintf (stderr, "SUCCESS\n");

        result = preproc_export (V, "all_frame_types").ptr_;

        fprintf (stderr, "  Result: %s\n", result);
        fprintf (stderr, "Expected: %s\n\n", test_vector[i].expected);

        if (strcmp (result, test_vector[i].expected))
            return EXIT_FAILURE;

        video_destroy (&V);
    }

    return EXIT_SUCCESS;
}
