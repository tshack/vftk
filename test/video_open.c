/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>

#include "error.h"
#include "video.h"

typedef struct {
    char* fn;
    ErrorCode expected;
} Test;


static const Test test_vector[] = {
    { "./videos/30fps_closedgop_N10_M1.mp4", SUCCESS  },
    { "./videos/30fps_closedgop_N10_M4.mp4", SUCCESS  },
    { "./videos/30fps_opengop_N10_M4.mp4",   SUCCESS  },
    { "./videos/vfr_closedgop_N10_M1.mp4",   SUCCESS  },
    { "./videos/vfr_closedgop_N10_M4.mp4",   SUCCESS  },
    { "./videos/vfr_opengop_N10_M4.mp4",     SUCCESS  },
    { "./videos/broken.mp4",                 EFILE_IO },
    { "./videos/something_random.xyz",       EFILE_IO },
    { NULL }
};


int main (int argc, char* argv[])
{
    int i;
    Video* V;
    ErrorCode ret;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Opening '%s'\n", test_vector[i].fn);

        ret = video_open (&V, test_vector[i].fn);

        fprintf (stderr, "  Result: %i -- %s\n", ret, err2str(ret));
        fprintf (stderr, "Expected: %i -- %s\n\n",
                ret, err2str(test_vector[i].expected));

        if (ret != test_vector[i].expected)
            return EXIT_FAILURE;

        video_destroy (&V);
    }

    return EXIT_SUCCESS;
}
