/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#include "internal/util.h"

#include "error.h"
#include "frame.h"
#include "video.h"

typedef struct {
    char* fn;
    FrameType expected;
} Test;


static const Test test_vector[] = {
    { "./videos/30fps_closedgop_N10_M1.mp4", P_FRAME },
    { "./videos/30fps_closedgop_N10_M4.mp4", P_FRAME },
    { "./videos/30fps_opengop_N10_M4.mp4",   B_FRAME },
    { "./videos/vfr_closedgop_N10_M1.mp4",   P_FRAME },
    { "./videos/vfr_closedgop_N10_M4.mp4",   P_FRAME },
    { "./videos/vfr_opengop_N10_M4.mp4",     B_FRAME },
    { NULL }
};


int main (int argc, char* argv[])
{
    int i;
    Video* V;
    FrameType result;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Opening '%s'\n", test_vector[i].fn);

        if (video_open (&V, test_vector[i].fn) != SUCCESS)
            return EXIT_FAILURE;

        if (!video_seek_frame (V, 19))
            return EXIT_FAILURE;

        result = video_peek_frametype (V);

        fprintf (stderr, "  Result: %s\n", _frametype2str(result));
        fprintf (stderr, "Expected: %s\n\n", _frametype2str(test_vector[i].expected));

        if (result != test_vector[i].expected)
            return EXIT_FAILURE;

        video_destroy (&V);
    }

    return EXIT_SUCCESS;
}
