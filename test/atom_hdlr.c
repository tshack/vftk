/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "internal/atom_test.h"

#include "atom.h"
#include "list.h"

#define CHECK_VALUE(name) _check_val (parse, #name, &test_vector[i].name);
#define CHECK_ARRAY(name) _check_val (parse, #name"[]", &test_vector[i].name);

typedef struct {
    char* fn;
    uint64_t size;
    long offset;
    char* fields[6];
    uint32_t version_flags;
    uint32_t pre_defined;
    uint32_t handler_type;
    uint32_t reserved[3];
    char* name[2];
} Test;


static const Test test_vector[] = {
    { .fn = "./videos/30fps_closedgop_N10_M1.mp4",
      .size = 45,
      .offset = 0x1f22c,
      .fields = {
          "version+flags",
          "pre_defined",
          "handler_type",
          "reserved[]",
          "name",
          NULL
      },
      .version_flags = 0x00000000,
      .pre_defined = 0x00000000,
      .handler_type = 0x76696465,
      .reserved = { 0x00000000, 0x00000000, 0x00000000 },
      .name = { "VideoHandler", NULL }
    },
    { NULL }
};


int main (int argc, char* argv[])
{
    AtomTree* at;
    Atom* atom;
    List* parse;
    int i;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Creating atom tree for '%s'\n", test_vector[i].fn);
        if ( !(at = atom_tree_create(test_vector[i].fn)) )
            return EXIT_FAILURE;

        atom = _find_atom (at, "moov.trak[0].mdia.hdlr");

        _check_size (atom, test_vector[i].size);
        _check_offset (atom, test_vector[i].offset);

        parse = atom_parse (at, atom);

        _check_fields (atom, parse, test_vector[i].fields);
        _check_val (parse, "version+flags", &test_vector[i].version_flags);

        CHECK_VALUE (pre_defined);
        CHECK_VALUE (handler_type);
        CHECK_ARRAY (reserved);
        CHECK_VALUE (name);

        atom_parse_destroy (&parse);
        atom_tree_destroy (&at);
    }

    return EXIT_SUCCESS;
}
