/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "internal/atom_test.h"

#include "atom.h"
#include "list.h"

#define CHECK_VALUE(name) _check_val (parse, #name, &test_vector[i].name);

typedef struct {
    char* fn;
    uint64_t size;
    long offset;
    char* fields[6];
    uint32_t version_flags;
    uint32_t entry_count;
    uint32_t first_chunk;
    uint32_t samples_per_chunk;
    uint32_t sample_description_index;
} Test;


static const Test test_vector[] = {
    { .fn = "./videos/30fps_closedgop_N10_M1.mp4",
      .size = 28,
      .offset = 0x1f377,
      .fields = {
          "version+flags",
          "entry_count",
          "first_chunk",
          "samples_per_chunk",
          "sample_description_index",
          NULL
      },
      .version_flags = 0x00000000,
      .entry_count = 0x00000001,
      .first_chunk = 0x00000001,
      .samples_per_chunk = 0x00000036,
      .sample_description_index = 0x00000001
    },
    { NULL }
};


int main (int argc, char* argv[])
{
    AtomTree* at;
    Atom* atom;
    List* parse;
    int i;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Creating atom tree for '%s'\n", test_vector[i].fn);
        if ( !(at = atom_tree_create(test_vector[i].fn)) )
            return EXIT_FAILURE;

        atom = _find_atom (at, "moov.trak[0].mdia.minf.stbl.stsc");

        _check_size (atom, test_vector[i].size);
        _check_offset (atom, test_vector[i].offset);

        parse = atom_parse (at, atom);

        _check_fields (atom, parse, test_vector[i].fields);
        _check_val (parse, "version+flags", &test_vector[i].version_flags);

        CHECK_VALUE (entry_count);
        CHECK_VALUE (first_chunk);
        CHECK_VALUE (samples_per_chunk);
        CHECK_VALUE (sample_description_index);

        atom_parse_destroy (&parse);
        atom_tree_destroy (&at);
    }

    return EXIT_SUCCESS;
}
