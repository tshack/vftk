/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#include "error.h"
#include "video.h"

typedef struct {
    char* fn;
    int64_t expected;
} Test;


static const Test test_vector[] = {
    { "./videos/30fps_closedgop_N10_M1.mp4", 54 },
    { "./videos/30fps_closedgop_N10_M4.mp4", 54 },
    { "./videos/30fps_opengop_N10_M4.mp4",   54 },
    { "./videos/vfr_closedgop_N10_M1.mp4",   45 },
    { "./videos/vfr_closedgop_N10_M4.mp4",   45 },
    { "./videos/vfr_opengop_N10_M4.mp4",     45 },
    { NULL }
};


int main (int argc, char* argv[])
{
    int i;
    Video* V;
    int64_t result;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Opening '%s'\n", test_vector[i].fn);

        if (video_open (&V, test_vector[i].fn) != SUCCESS)
            return EXIT_FAILURE;

        result = video_peek_nframes (V);

        fprintf (stderr, "  Result: %"PRId64"\n", result);
        fprintf (stderr, "Expected: %"PRId64"\n\n", test_vector[i].expected);

        if (result != test_vector[i].expected)
            return EXIT_FAILURE;

        video_destroy (&V);
    }

    return EXIT_SUCCESS;
}
