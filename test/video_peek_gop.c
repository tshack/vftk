/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "error.h"
#include "video.h"

typedef struct {
    char* fn;
    int N;
    int M;
} Test;


static const Test test_vector[] = {
    { "./videos/30fps_closedgop_N10_M1.mp4", 10, 1 },
    { "./videos/30fps_closedgop_N10_M4.mp4", 10, 4 },
    { "./videos/30fps_opengop_N10_M4.mp4",   10, 4 },
    { "./videos/vfr_closedgop_N10_M1.mp4",   10, 1 },
    { "./videos/vfr_closedgop_N10_M4.mp4",   10, 4 },
    { "./videos/vfr_opengop_N10_M4.mp4",     10, 4 },
    { NULL }
};


int main (int argc, char* argv[])
{
    int i, N, M;
    Video* V;
    ErrorCode ret;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Opening '%s' ", test_vector[i].fn);

        ret = video_open (&V, test_vector[i].fn);
        if (ret) {
            fprintf (stderr, "FAILED! %s\n", err2str(ret));
            return EXIT_FAILURE;
        }
        fprintf (stderr, "SUCCESS\n");

        video_peek_gop (V, &M, &N);

        fprintf (stderr, "  Result: N = %3i, M = %3i\n", N, M);
        fprintf (stderr, "Expected: N = %3i, M = %3i\n\n",
                test_vector[i].N, test_vector[i].M);

        if (N != test_vector[i].N || M != test_vector[i].M)
            return EXIT_FAILURE;

        video_destroy (&V);
    }

    return EXIT_SUCCESS;
}
