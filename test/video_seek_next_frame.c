/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#include "error.h"
#include "video.h"


static char* test_vector[] = {
    "./videos/30fps_closedgop_N10_M1.mp4",
    "./videos/30fps_closedgop_N10_M4.mp4",
    "./videos/30fps_opengop_N10_M4.mp4",
    "./videos/vfr_closedgop_N10_M1.mp4",
    "./videos/vfr_closedgop_N10_M4.mp4",
    "./videos/vfr_opengop_N10_M4.mp4",
    NULL
};


int main (int argc, char* argv[])
{
    int i;
    Video* V;
    int64_t f;
    int64_t result;

    for (i=0; test_vector[i]; i++) {
        fprintf (stderr, "Opening '%s'\n", test_vector[i]);

        if (video_open (&V, test_vector[i]) != SUCCESS)
            return EXIT_FAILURE;

        fprintf (stderr, "Result\tExpected\n");

        f = 0;

        do {
            result = video_peek_framenumber (V);
            fprintf (stderr, "%06"PRId64"\t", result);
            fprintf (stderr, "%06"PRId64"\n", f);

            if (result != f)
                return EXIT_FAILURE;

            f++;
        } while (video_seek_next_frame(V));

        video_destroy (&V);
    }

    return EXIT_SUCCESS;
}
