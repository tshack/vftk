/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

#include "error.h"
#include "video.h"

typedef struct {
    char* fn;
    int expected;
} Test;


static const Test test_vector[] = {
    { "./videos/30fps_closedgop_N10_M1.mp4", 404 },
    { "./videos/30fps_closedgop_N10_M4.mp4", 404 },
    { "./videos/30fps_opengop_N10_M4.mp4",   404 },
    { "./videos/vfr_closedgop_N10_M1.mp4",   404 },
    { "./videos/vfr_closedgop_N10_M4.mp4",   404 },
    { "./videos/vfr_opengop_N10_M4.mp4",     404 },
    { NULL }
};


int main (int argc, char* argv[])
{
    int i;
    Video* V;
    int result;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Opening '%s'\n", test_vector[i].fn);

        if (video_open (&V, test_vector[i].fn) != SUCCESS)
            return EXIT_FAILURE;

        result = video_peek_height (V);

        fprintf (stderr, "  Result: %d\n", result);
        fprintf (stderr, "Expected: %d\n\n", test_vector[i].expected);

        if (result != test_vector[i].expected)
            return EXIT_FAILURE;

        video_destroy (&V);
    }

    return EXIT_SUCCESS;
}
