/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "internal/atom_test.h"

#include "atom.h"
#include "list.h"

#define CHECK_VALUE(name) _check_val (parse, #name, &test_vector[i].name);

typedef struct {
    char* fn;
    uint64_t size;
    long offset;
    char* fields[8];
    uint32_t version_flags;
    uint64_t creation_time;
    uint64_t modification_time;
    uint32_t timescale;
    uint64_t duration;
    uint16_t language;
    uint16_t pre_defined;
} Test;


static const Test test_vector[] = {
    { .fn = "./videos/30fps_closedgop_N10_M1.mp4",
      .size = 32,
      .offset = 0x1f20c,
      .fields = {
          "version+flags",
          "creation_time",
          "modification_time",
          "timescale",
          "duration",
          "language",
          "pre_defined",
          NULL
      },
      .version_flags = 0x00000000,
      .creation_time = 0x00000000,
      .modification_time = 0x00000000,
      .timescale = 0x00007530,
      .duration = 0x0000d326,
      .language = 0x55c4,
      .pre_defined = 0x0000
    },
    { NULL }
};


int main (int argc, char* argv[])
{
    AtomTree* at;
    Atom* atom;
    List* parse;
    int i;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Creating atom tree for '%s'\n", test_vector[i].fn);
        if ( !(at = atom_tree_create(test_vector[i].fn)) )
            return EXIT_FAILURE;

        atom = _find_atom (at, "moov.trak[0].mdia.mdhd");

        _check_size (atom, test_vector[i].size);
        _check_offset (atom, test_vector[i].offset);

        parse = atom_parse (at, atom);

        _check_fields (atom, parse, test_vector[i].fields);
        _check_val (parse, "version+flags", &test_vector[i].version_flags);

        CHECK_VALUE (creation_time);
        CHECK_VALUE (modification_time);
        CHECK_VALUE (timescale);
        CHECK_VALUE (duration);
        CHECK_VALUE (language);
        CHECK_VALUE (pre_defined);

        atom_parse_destroy (&parse);
        atom_tree_destroy (&at);
    }

    return EXIT_SUCCESS;
}
