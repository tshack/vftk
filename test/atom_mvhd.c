/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "internal/atom_test.h"

#include "atom.h"
#include "list.h"

#define CHECK_VALUE(name) _check_val (parse, #name, &test_vector[i].name);
#define CHECK_ARRAY(name) _check_val (parse, #name"[]", &test_vector[i].name);

typedef struct {
    char* fn;
    uint64_t size;
    long offset;
    char* fields[10];
    uint32_t version_flags;
    uint64_t creation_time;
    uint64_t modification_time;
    uint32_t timescale;
    uint64_t duration;
    uint32_t rate;
    uint16_t volume;
    int32_t matrix[9];
    uint32_t next_track_ID;
} Test;


static const Test test_vector[] = {
    { .fn = "./videos/30fps_closedgop_N10_M1.mp4",
      .size = 108,
      .offset = 0x1f110,
      .fields = {
          "version+flags",
          "creation_time",
          "modification_time",
          "timescale",
          "duration",
          "rate",
          "volume",
          "matrix[]",
          "next_track_ID",
          NULL
      },
      .version_flags = 0x00000000,
      .creation_time = 0x00000000,
      .modification_time = 0x00000000,
      .timescale = 0x000003e8,
      .duration = 0x0000070a,
      .rate = 0x00010000,
      .volume = 0x0100,
      .matrix = { 0x00010000, 0x00000000, 0x00000000,
                  0x00000000, 0x00010000, 0x00000000,
                  0x00000000, 0x00000000, 0x40000000
                },
      .next_track_ID = 0x00000002
    },
    { NULL }
};


int main (int argc, char* argv[])
{
    AtomTree* at;
    Atom* atom;
    List* parse;
    int i;

    for (i=0; test_vector[i].fn; i++) {
        fprintf (stderr, "Creating atom tree for '%s'\n", test_vector[i].fn);
        if ( !(at = atom_tree_create(test_vector[i].fn)) )
            return EXIT_FAILURE;

        atom = _find_atom (at, "moov.mvhd");

        _check_size (atom, test_vector[i].size);
        _check_offset (atom, test_vector[i].offset);

        parse = atom_parse (at, atom);

        _check_fields (atom, parse, test_vector[i].fields);
        _check_val (parse, "version+flags", &test_vector[i].version_flags);

        CHECK_VALUE (creation_time);
        CHECK_VALUE (modification_time);
        CHECK_VALUE (timescale);
        CHECK_VALUE (duration);
        CHECK_VALUE (rate);
        CHECK_VALUE (volume);
        CHECK_ARRAY (matrix);
        CHECK_VALUE (next_track_ID);

        atom_parse_destroy (&parse);
        atom_tree_destroy (&at);
    }

    return EXIT_SUCCESS;
}
