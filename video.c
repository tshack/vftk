/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "internal/video.h"
#include "internal/preproc.h"
#include "internal/util.h"

#include "error.h"
#include "frame.h"
#include "list.h"
#include "video.h"

#define max(a,b) a > b ? a : b
#define clamp(a, l, h) a < l ? l : a > h ? h : a


static FrameType _avframetype2internal (enum AVPictureType pict_type)
{
    if (AV_PICTURE_TYPE_I == pict_type)
        return I_FRAME;
    else if (AV_PICTURE_TYPE_P == pict_type)
        return P_FRAME;
    else if (AV_PICTURE_TYPE_B == pict_type)
        return B_FRAME;
    else
        return UNKNOWN_FRAME;
}


void video_destroy (Video** V)
{
    if (!*V)
        return;

    if ((*V)->filename)
        free ((*V)->filename);

    if ((*V)->avframe) {
        av_frame_unref ((*V)->avframe);
        av_frame_free (&(*V)->avframe);
    }

    if ((*V)->decoder)
        avcodec_free_context (&(*V)->decoder);

    if ((*V)->container)
        avformat_close_input (&(*V)->container);

    preprocess_fini (*V);

    free (*V);
    *V = NULL;
}


ErrorCode video_open (Video** V, char* infile)
{
    ErrorCode err;
    AVCodec* dec = NULL;
    AVDictionary* codec_opts = NULL;

    *V = malloc (sizeof(**V));
    memset (*V, 0, sizeof(**V));
    (*V)->stream_idx = -1;
    (*V)->filename = malloc (strlen (infile) + 1);
    strcpy ((*V)->filename, infile);

    list_init (&(*V)->preprocs);

    if (avformat_open_input (&(*V)->container, infile, NULL, NULL) < 0) {
        err = EFILE_IO;
        goto out;
    }

    if (avformat_find_stream_info ((*V)->container, NULL) < 0) {
        err = ESTREAM_INFO;
        goto out;
    }

    (*V)->stream_idx = av_find_best_stream ((*V)->container, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if ((*V)->stream_idx < 0) {
        err = ESTREAM;
        goto out;
    }

    (*V)->stream = (*V)->container->streams[(*V)->stream_idx];

    (*V)->decoder = avcodec_alloc_context3 (dec);
    if (!(*V)->decoder) {
        err = ECODEC_ALLOC;
        goto out;
    }

    if (avcodec_parameters_to_context ((*V)->decoder, (*V)->stream->codecpar) < 0) {
        err = ECODEC_PARAMS;
        goto out;
    }

    av_dict_set (&codec_opts, "flags2", "+export_mvs", 0);

    /* initialize video decoder */
    if (avcodec_open2 ((*V)->decoder, dec, &codec_opts) < 0) {
        err = ECODEC_IO;
        goto out;
    }

    (*V)->avframe = av_frame_alloc ();
    if (!(*V)->avframe) {
        err = EFRAME_ALLOC;
        goto out;
    }

    (*V)->h = (*V)->decoder->priv_data;

    if ( (err = preprocess_packets (*V)) != SUCCESS )
        goto out;

    if (video_peek_nframes (*V) < 1) {
        err = ESTREAM_NOFRAMES;
        goto out;
    }

    video_seek_frame (*V, 0);

    return SUCCESS;

out:
    video_destroy (V);
    return err;
}


/* return 0: End of video
 *        1: got frame    */
int video_seek_next_frame (Video* V)
{
    int ret;
    int is_eof;

tryagain:

    while (!(is_eof = av_read_frame (V->container, &V->pkt))) {
        if (V->pkt.stream_index == V->stream_idx)
            break;
        else
            av_packet_unref (&V->pkt);
    }

    if (is_eof)
        return 0;

    if (avcodec_send_packet (V->decoder, &V->pkt) < 0) {
        fprintf (stderr, "Error sending packet to decoder! (Aborting)\n");
        exit (EXIT_FAILURE);
    }

    av_packet_unref (&V->pkt);
    av_frame_unref (V->avframe); /* from previous cycle */

    while ((ret = avcodec_receive_frame (V->decoder, V->avframe)) >= 0) {
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            break;

        if (ret == 0)
            return 1;
    }

    goto tryagain;
}


static int _seek_frame_sane (Video* V, int64_t framenumber)
{
    int64_t nframes = video_peek_nframes (V);
    int64_t* pts_lut = preproc_export (V, "pts_lut").ptr_;
    int64_t* dts_lut = preproc_export (V, "dts_lut").ptr_;
    int64_t dest_dts = dts_lut[clamp(framenumber, 0, nframes - 1)];
    int64_t dest_pts = pts_lut[clamp(framenumber, 0, nframes - 1)];

    if (av_seek_frame (V->container, V->stream_idx,
            dest_dts, AVSEEK_FLAG_BACKWARD) < 0)
        return 0;

    avcodec_flush_buffers (V->decoder);

    while (video_seek_next_frame (V) && V->avframe->pts < dest_pts);

    return V->avframe->pts == dest_pts;
}


static int _seek_frame_panic (Video* V, int64_t framenumber)
{
    int64_t nframes = video_peek_nframes (V);
    int64_t* pts_lut = preproc_export (V, "pts_lut").ptr_;
    int64_t dest_pts = pts_lut[clamp(framenumber, 0, nframes - 1)];

    if (av_seek_frame (V->container, V->stream_idx,
            0, AVSEEK_FLAG_BACKWARD) < 0)
        return 0;

    avcodec_flush_buffers (V->decoder);

    while (video_seek_next_frame (V) && V->avframe->pts != dest_pts);

    return V->avframe->pts == dest_pts;
}


static int _seek_frame_recover (Video* V, int64_t framenumber)
{
    /* Sometimes av_seek_frame() just doesn't put you at the decode time
     * stamp of the I-Frame tied to your seek time stamp when
     * AVSEEK_FLAG_BACKWARDS is specified.  This tends to happen only
     * occasionally for a given open GOP video -- maybe one frame in
     * several hundred.  For interlaced videos I have tested, it would
     * appear that AVSEEK_FLAG_BACKWARDS is treated as AVSEEK_FLAG_ANY
     * by av_seek_frame(). Not good.
     *
     * Naturally, this is a huge problem because we will not be taken
     * backward to the I-Frame needed to decode the requested frame.
     * Without decoding all frames, we have no way to locate the
     * required I-Frame precisely.  So, here we do a blind search in an
     * attempt to recover in as few decoded frames as possible.
     *
     * This could probably be made better if we attempted to "learn" the
     * GOP structure as we stumble around in the dark. */

    int64_t req;
    int64_t backup = 7;
    int64_t nframes = video_peek_nframes (V);
    int64_t* pts_lut = preproc_export (V, "pts_lut").ptr_;
    int64_t* dts_lut = preproc_export (V, "dts_lut").ptr_;
    int64_t dest_pts = req = pts_lut[clamp(framenumber, 0, nframes - 1)];

    do {
        if (av_seek_frame (V->container, V->stream_idx,
                req, AVSEEK_FLAG_BACKWARD) < 0)
            return 0;

        avcodec_flush_buffers (V->decoder);

        framenumber -= (backup *= 2);
        req = dts_lut[clamp(framenumber, 0, nframes - 1)];
        req = framenumber <= 0 ? 0 : req;
    } while (video_seek_next_frame (V) && V->avframe->pts > dest_pts);

    if (V->avframe->pts == dest_pts)
        return 1;

    while (video_seek_next_frame (V) && V->avframe->pts != dest_pts);

    return V->avframe->pts == dest_pts;
}


int video_seek_frame (Video* V, int64_t framenumber)
{
    int ret;

    ret = _seek_frame_sane (V, framenumber);

    if (!ret)
        ret = _seek_frame_recover (V, framenumber);

    if (!ret)
        ret = _seek_frame_panic (V, framenumber);

    /* user may not check for a failure before doing something that
     * requires V->avframe to be in a sane state.  so, on failure, try
     * to force avframe into a sane state */
    if (!ret) {
        av_seek_frame (V->container, V->stream_idx, 0, AVSEEK_FLAG_BACKWARD);
        avcodec_flush_buffers (V->decoder);
        video_seek_next_frame (V);
    }

    return ret;
}


int64_t video_peek_framenumber (Video* V)
{
    int64_t b = 0;
    int64_t t = video_peek_nframes(V) - 1;
    int64_t i = t / 2;
    int64_t* pts_lut = preproc_export (V, "pts_lut").ptr_;

    while (pts_lut[i] != V->avframe->pts) {
        if (pts_lut[i] < V->avframe->pts)
            b = i + 1;
        else
            t = i - 1;

        i = (t + b) / 2;
    }

    return i;
}


int64_t video_peek_nframes (Video* V)
{
    return preproc_export (V, "nframes").int64_;
}


int video_peek_gop (Video* V, int* m, int* n)
{
    *n = preproc_export (V, "gop_n").int_;
    *m = preproc_export (V, "gop_m").int_;

    if (!*n && !*m)
        return 0;

    return 1;
}


void video_print_format (Video* V)
{
    av_log_set_level (AV_LOG_INFO);
    av_dump_format (V->container, 0, V->filename, 0);
    av_log_set_level (AV_LOG_QUIET);
}


void video_get_frame (Video* V, Frame* F)
{
    F->number = video_peek_framenumber (V);
    F->time_s = video_peek_time (V);
    F->type = _avframetype2internal (V->avframe->pict_type);
    F->is_cabac = video_peek_frame_is_cabac (V);

    frame_set_picture (F, V->avframe);
    frame_set_motion_vecs (F, V->avframe);
}


ErrorCode video_get_frame_perror (Video* V, Frame* F)
{
    if (strcmp(V->decoder->codec->name, "h264"))
        return EATTR;

    F->number = video_peek_framenumber (V);
    F->time_s = video_peek_time (V);
    F->type = _avframetype2internal (V->avframe->pict_type);

    frame_set_picture (F, V->h->perrorf_ptr);

    return SUCCESS;
}


double video_peek_frame_perror (Video* V)
{
    uint32_t i, j;
    uint32_t perror = 0;

    AVFrame* e = V->h->perrorf_ptr;

    if (strcmp(V->decoder->codec->name, "h264"))
        return NAN;

    /* TODO: Support other colorspaces
     * for now we assume YUV420 (vast majority of most video)
     * This will require a patch to h264_mb_template.c as well */

    for (j=0; j<e->height; j++)
        for (i=0; i<e->linesize[0]; i++)
            perror += abs((int16_t)(*(e->data[0] + e->linesize[0]*j + i)) - 128);

    for (j=0; j<e->height/2; j++)
        for (i=0; i<e->linesize[1]; i++)
            perror += abs((int16_t)(*(e->data[1] + e->linesize[1]*j + i)) - 128);

    for (j=0; j<e->height/2; j++)
        for (i=0; i<e->linesize[2]; i++)
            perror += abs((int16_t)(*(e->data[2] + e->linesize[2]*j + i)) - 128);

    return 2.0 * (double)perror / (double)(video_peek_width (V) * video_peek_height (V));
}


FrameType video_peek_frametype (Video* V)
{
    return _avframetype2internal (V->avframe->pict_type);
}


double video_peek_time (Video* V)
{
    return av_q2d (V->stream->time_base) * V->avframe->pts;
}


int video_peek_width (Video* V)
{
    return V->decoder->width;
}


int video_peek_height (Video* V)
{
    return V->decoder->height;
}


int video_peek_profile (Video* V)
{
    return V->decoder->profile;
}


int video_peek_level (Video* V)
{
    return V->decoder->level;
}


int64_t video_peek_bitrate (Video* V)
{
    return V->stream->codecpar->bit_rate;
}


int video_is_avc (Video* V)
{
    if (strcmp(V->decoder->codec->name, "h264"))
        return -1;

    return V->h->is_avc;
}


int video_is_vfr (Video* V)
{
    if (V->stream->r_frame_rate.num != V->stream->avg_frame_rate.num)
        return 1;
    else
        return 0;
}


/* Note: pointer lifetime has that of the Video.
 * If required beyond that, it's the user's
 * responsibility to strdup() */
const char* video_peek_all_frame_types (Video* V)
{
    return preproc_export (V, "all_frame_types").ptr_;
}


int video_peek_frame_is_cabac (Video* V)
{
    if (strcmp(V->decoder->codec->name, "h264"))
        return -1;

    if (V->h->is_avc)
        return V->h->ps.pps->cabac;
    else
        return -1;
}
