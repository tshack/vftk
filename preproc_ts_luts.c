/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include "internal/preproc.h"
#include "internal/video.h"

#include "error.h"
#include "frame.h"
#include "video.h"

typedef struct PreProcData {
    int64_t* pts_lut;
    int64_t* dts_lut;
    int64_t nframes;
} PreProcData;

#define OFFSET(attr) offsetof(PreProcData, attr)

static const PreProcExport exports[] = {
    {.name = "pts_lut", .type = PTR, .data = OFFSET(pts_lut)},
    {.name = "dts_lut", .type = PTR, .data = OFFSET(dts_lut)},
    {.name = "nframes", .type = INT64, .data = OFFSET(nframes)},
    { NULL }
};


static void _sort (PreProcData* D, int64_t n)
{
    int64_t i, j;
    int64_t tmp;

    for (j=0; j<(n-1); j++) {
        for (i=0; i<(n-(1+j)); i++) {
            if (D->pts_lut[i] > D->pts_lut[i+1]) {
                tmp = D->pts_lut[i+1];
                D->pts_lut[i+1] = D->pts_lut[i];
                D->pts_lut[i] = tmp;

                tmp = D->dts_lut[i+1];
                D->dts_lut[i+1] = D->dts_lut[i];
                D->dts_lut[i] = tmp;
            }
        }
    }

}


/* ffmpeg automatically combines interlaced fields into a frame.
 * packets are representative of fields, so for seeking purposes we only
 * need odd fields, which correlate to ffmpegs frame presentation time
 * stamps on produced frames... odd, I know. */
static void _handle_interlaced (Video* V, PreProcData* D)
{
    int64_t s, d;

    for (s=2, d=1; s<V->npackets; s+=2, d++) {
        D->pts_lut[d] = D->pts_lut[s];
        D->dts_lut[d] = D->dts_lut[s];
    }

    D->nframes = V->npackets / 2;
}


static ErrorCode init (Video* V, PreProcData* D)
{
    size_t size = V->npackets*sizeof(*D->pts_lut);

    D->nframes = V->npackets;

    if (size == 0)
        goto out;

    D->pts_lut = malloc (size);
    if (!D->pts_lut)
        goto out;

    D->dts_lut = malloc (size);
    if (!D->dts_lut)
        goto out_dts_lut;

    return SUCCESS;

out_dts_lut:
    free (D->pts_lut);
out:
    return EPREPROC_INIT;
}


static void perpacket (Video* V, PreProcData* D, int64_t packet_number)
{
    D->pts_lut[packet_number] = V->pkt.pts;
    D->dts_lut[packet_number] = V->pkt.dts;

    if (packet_number == V->npackets - 1) {
        _sort (D, V->npackets);

        if (V->is_interlaced)
            _handle_interlaced (V, D);
    }
}


static void fini (Video* V, PreProcData* D)
{
    free (D->pts_lut);
    free (D->dts_lut);

    return;
}


NEW_PREPROC(ts_luts) = {
    .name = "TS Lookup Tables",
    .init = init,
    .perpacket = perpacket,
    .fini = fini,
    .exports = exports,
    .size = sizeof(PreProcData)
};
