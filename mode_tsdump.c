/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "internal/argparse.h"
#include "internal/preproc.h"

#include "frame.h"
#include "video.h"

#define NULL_FRAME -9999999


typedef struct {
    int help;
} Opts;


static Opts opts = {
    .help = 0
};


static void print_usage (ArgParse* P)
{
    printf ("Usage: vftk tsdump [OPTIONS...] INPUT\n"
            "\n"
            "Options:\n");

    argparse_print (P);
}


static ArgParse* _parse (int argc, char* argv[])
{
    int ret;

    ArgParse* P = argparse_new (argc, argv);

    argparse_add (P, ARG_FLAG, "-h", "--help",
            "display this help text and exit", &opts.help);

    if (!argparse (P)) {
        ret = EXIT_FAILURE;
        goto out;
    }

    if (opts.help) {
        print_usage (P);
        ret = EXIT_SUCCESS;
        goto out;
    }

    if (P->argc < 1) {
        fprintf (stderr, "Must specify INPUT\n");
        ret = EXIT_FAILURE;
        goto out;
    }

    return P;

out:
    argparse_destroy (&P);
    exit (ret);
}


static void _sort (int64_t* lut, int64_t n)
{
    int64_t i, j;
    int64_t tmp;

    for (j=0; j<(n-1); j++)
        for (i=0; i<(n-(1+j)); i++)
            if (lut[i] > lut[i+1]) {
                tmp = lut[i+1];
                lut[i+1] = lut[i];
                lut[i] = tmp;
            }
}


static int64_t* _build_ts_lut (Video* V, int64_t* n)
{
    int64_t i, j;

    int64_t npkts = video_peek_nframes (V);
    int64_t ts_lut_n = 2 * npkts;
    int64_t* ts_lut = malloc (sizeof(*ts_lut) * ts_lut_n);
    int64_t* pts_lut = preproc_export (V, "pts_lut").ptr_;
    int64_t* dts_lut = preproc_export (V, "dts_lut").ptr_;

    for (i=0; i < npkts; i++)
        ts_lut[i] = pts_lut[i];

    *n = i;

    for (j=0; j < npkts; j++) {
        for (i=0; i < npkts; i++)
            if (dts_lut[j] == ts_lut[i])
                break;

        if (i == npkts)
            ts_lut[(*n)++] = dts_lut[j];
    }

    _sort (ts_lut, *n);

    return ts_lut;
}


static int64_t _rlookup (int64_t val, int64_t* lut, int64_t n)
{
    int64_t b = 0;
    int64_t t = n-1;
    int64_t i = t / 2;

    while (lut[i] != val && b <= t) {
        if (lut[i] < val)
            b = i + 1;
        else
            t = i - 1;

        i = (t + b) / 2;
    }

    if (b > i || i > t)
        return NULL_FRAME;

    return i;
}


static int64_t _rlookup_unsorted (int64_t val, int64_t* lut, int64_t n)
{
    int64_t i;

    for (i=0; i < n; i++)
        if (lut[i] == val)
            break;

    if (i == n)
        return NULL_FRAME;

    return i;
}


static void _dump_ts (Video* V)
{
    int64_t i;
    int64_t ts_lut_n;
    int64_t df, pf;

    int64_t* ts_lut = _build_ts_lut (V, &ts_lut_n);
    int64_t* dts_lut = preproc_export (V, "dts_lut").ptr_;
    int64_t* pts_lut = preproc_export (V, "pts_lut").ptr_;
    int64_t dts_lut_n = video_peek_nframes (V);

    printf ("Time Stamp, Decode Frame, Present Frame\n");

    for (i=0; i<ts_lut_n; i++) {
        printf ("%*"PRId64", ", 10, ts_lut[i]);

        df = _rlookup_unsorted (ts_lut[i], dts_lut, dts_lut_n);
        if (df != NULL_FRAME)
            printf ("%*"PRId64", ", 12, df);
        else
            printf ("%*c, ", 12, ' ');

        pf = _rlookup (ts_lut[i], pts_lut, dts_lut_n);
        if (pf != NULL_FRAME)
            printf ("%*"PRId64, 13, pf);

        printf ("\n");
    }

    free (ts_lut);
}


int mode_tsdump (int argc, char* argv[])
{
    ErrorCode err;
    ArgParse* P;
    Video* V;

    int ret = EXIT_SUCCESS;;


    P = _parse (argc, argv);

    err = video_open (&V, P->argv[0]);
    if (err) {
        fprintf (stderr, "%s ['%s']\n", err2str (err), P->argv[0]);
        ret = EXIT_FAILURE;
        goto out;
    }

    _dump_ts (V);

    video_destroy (&V);
out:
    argparse_destroy (&P);
    return ret;
}
