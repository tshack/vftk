/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include "libavutil/motion_vector.h"
#include "libavformat/avformat.h"

#include "internal/argparse.h"

#include "error.h"
#include "frame.h"
#include "video.h"


typedef struct {
    int help;
    int backward;
    int forward;
} Opts;


static Opts opts = {
    .help = 0,
    .backward = 0,
    .forward = 0
};


static void print_usage (ArgParse* P)
{
    printf ("Usage: vftk mv [OPTIONS...] INPUT\n"
            "\n"
            "Options:\n");

    argparse_print (P);
}


static ArgParse* _parse (int argc, char* argv[])
{
    int ret;

    ArgParse* P = argparse_new (argc, argv);

    argparse_add (P, ARG_FLAG, "-f", "--forward",
            "write out forward motion vectors",
            &opts.forward);

    argparse_add (P, ARG_FLAG, "-b", "--backward",
            "write out backward motion vectors",
            &opts.backward);

    argparse_add (P, ARG_FLAG, "-h", "--help",
            "display this help text and exit", &opts.help);

    if (!argparse (P)) {
        ret = EXIT_FAILURE;
        goto out;
    }

    if (opts.help) {
        print_usage (P);
        ret = EXIT_SUCCESS;
        goto out;
    }

    if (P->argc < 1) {
        fprintf (stderr, "Must specify INPUT\n");
        ret = EXIT_FAILURE;
        goto out;
    }

    return P;

out:
    argparse_destroy (&P);
    exit (ret);
}


static void _dump_mv (Video* V)
{
    AVMotionVector* mv;
    unsigned int i;

    Frame* F = frame_new ();

    do {
        video_get_frame (V, F);

        if (!F->motion_vecs) continue;

        for (i=0; i<F->num_motion_vecs; i++) {
            mv = &F->motion_vecs[i];

            if ( (opts.forward  && mv->source ==  1) ||
                 (opts.backward && mv->source == -1) ) {
                printf ("%ld [%2d]: %2dx%2d: (%4d, %4d) => (%4d, %4d)\n",
                    F->number, mv->source,
                    mv->w, mv->h,
                    mv->src_x, mv->src_y,
                    mv->dst_x, mv->dst_y);
            }
        }

    } while (video_seek_next_frame (V));

    printf ("done.\n\n");

    frame_destroy (&F);
}


int mode_mv (int argc, char* argv[])
{
    ErrorCode err;
    ArgParse* P;
    Video* V;

    int ret = EXIT_SUCCESS;


    P = _parse (argc, argv);

    err = video_open (&V, P->argv[0]);
    if (err) {
        fprintf (stderr, "%s ['%s']\n", err2str (err), P->argv[0]);
        ret = EXIT_FAILURE;
        goto out;
    }

    _dump_mv (V);

    video_destroy (&V);
out:
    argparse_destroy (&P);
    return ret;
}
