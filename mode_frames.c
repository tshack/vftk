/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include <stdlib.h>

#include "libavformat/avformat.h"

#include "internal/argparse.h"
#include "internal/util.h"

#include "error.h"
#include "frame.h"
#include "tiff.h"
#include "video.h"

#define FN_BUFFER_SIZE 1024


typedef struct {
    int help;
    int i_frames;
    int p_frames;
    int b_frames;
    char* prefix;
} Opts;


static Opts opts = {
    .help = 0,
    .i_frames = 0,
    .p_frames = 0,
    .b_frames = 0,
    .prefix = NULL
};


static void print_usage (ArgParse* P)
{
    printf ("Usage: vftk frames [OPTIONS...] INPUT\n"
            "\n"
            "Options:\n");

    argparse_print (P);

    printf ("\n"
            "Selected frames will be written to disk in Tagged Image "
            "File Format (TIFF)."
            "\n");
}


static ArgParse* _parse (int argc, char* argv[])
{
    int ret;

    ArgParse* P = argparse_new (argc, argv);

    argparse_add (P, ARG_FLAG, "-h", "--help",
            "display this help text and exit",
            &opts.help);

    argparse_add (P, ARG_FLAG, "-i", "--iframes",
            "write inter frames (I-frames) to disk",
            &opts.i_frames);

    argparse_add (P, ARG_FLAG, "-p", "--pframes",
            "write predicted frames (P-frames) to disk",
            &opts.p_frames);

    argparse_add (P, ARG_FLAG, "-b", "--bframes",
            "write bidirectionally predicted frames (B-frames) to disk",
            &opts.b_frames);

    argparse_add (P, ARG_REQUIRED | ARG_STR, "-P", "--prefix=PREFIX",
            "write frames to disk using file names starting with PREFIX",
            &opts.prefix);

    if (!argparse (P)) {
        ret = EXIT_FAILURE;
        goto out;
    }

    if (opts.help) {
        print_usage (P);
        ret = EXIT_SUCCESS;
        goto out;
    }

    if (!argparse_check_required (P)) {
        ret = EXIT_FAILURE;
        goto out;
    }

    if (P->argc < 1) {
        fprintf (stderr, "Must specify INPUT\n");
        ret = EXIT_FAILURE;
        goto out;
    }

    return P;

out:
    argparse_destroy (&P);
    exit (ret);
}


static void _dump_frames (Video* V)
{
    Frame* F;
    FrameType type;

    char fn[FN_BUFFER_SIZE] = {0};

    F = frame_new ();

    printf ("Writing frames to disk in TIFF format with prefix '%s'... ", opts.prefix);
    fflush (stdout);

    do {
        type = video_peek_frametype (V);

        if (type == UNKNOWN_FRAME)
            continue;

        if ( ( (type == I_FRAME && !opts.i_frames)   ||
               (type == P_FRAME && !opts.p_frames)   ||
               (type == B_FRAME && !opts.b_frames) ) &&
             (opts.i_frames || opts.p_frames || opts.b_frames) ) {
            continue;
        }

        video_get_frame (V, F);

        snprintf (fn, FN_BUFFER_SIZE, "%s%0*"PRId64".tiff",
                opts.prefix,
                _num_digits (video_peek_nframes (V)),
                video_peek_framenumber (V));

        tiff_writer (fn, F);
    } while (video_seek_next_frame (V));

    printf ("done.\n\n");

    frame_destroy (&F);
}


int mode_frames (int argc, char* argv[])
{
    ErrorCode err;
    ArgParse* P;
    Video* V;

    int ret = EXIT_SUCCESS;


    P = _parse (argc, argv);

    err = video_open (&V, P->argv[0]);
    if (err) {
        fprintf (stderr, "%s ['%s']\n", err2str (err), P->argv[0]);
        ret = EXIT_FAILURE;
        goto out;
    }

    _dump_frames (V);

    video_destroy (&V);
out:
    argparse_destroy (&P);
    exit (ret);
}
