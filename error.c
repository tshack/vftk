/*********************************************************************
 * Author: James A. Shackleford <shack@linux.com>, <shack@drexel.edu>
 *********************************************************************/
#include "error.h"

const char* const err2str (ErrorCode e)
{
    switch (e) {
        case SUCCESS:
            return "(SUCCESS) Another day in paradise.";
        case EFILE_IO:
            return "(EFILE_IO) Failed to open file!";
        case ESTREAM:
            return "(ESTREAM) Failed to find stream!";
        case ESTREAM_INFO:
            return "(ESTREAM_INFO) Failed to find stream information!";
        case ESTREAM_NOFRAMES:
            return "(ESTREAM_NOFRAMES) Video contains no frames!";
        case ECODEC_ALLOC:
            return "(ECODEC_ALLOC) Failed to allocate codec!";
        case ECODEC_PARAMS:
            return "(ECODEC_PARAMS) Failed to copy codec params to context";
        case ECODEC_IO:
            return "(ECODEC_IO) Failed to open codec!";
        case EFRAME_ALLOC:
            return "(EFRAME_ALLOC) Failed to allocate frame!";
        case EFRAME_MV:
            return "(EFRAME_MV) Failed to get motion vectors!";
        case EFRAME_RGB24:
            return "(EFRAME_RGB24) Failed to convert frame to RGB24!";
        case EPREPROC_ALLOC:
            return "(EPREPROC_ALLOC) Failed to allocate preproc context!";
        case EPREPROC_INIT:
            return "(EPREPROC_INIT) Failed to initialize preproc!";
        case EPREPROC_PKT:
            return "(EPREPROC_PKT) Failed to receive packet!";
        case EATTR:
            return "(EATTR) Failed to determine requested attribute!";
        default:
            return "Encountered invalid error code!";
    }
}
